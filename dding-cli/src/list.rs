/// render interactive lists backed by data stored in redis
/// for domain structs like Wiki/Page/Url/Country/Fingerprint
use anyhow::Result;
use dding::db::ToRedisKey;
// use ratatui::widgets::ListState;
use ratatui::{
    prelude::*,
    widgets::{Block, Borders, List, ListItem, ListState},
};
use redis::cluster::ClusterConnection;
use serde::Deserialize;

/// A ratatui list is updatable and navigatable
pub trait InteractiveList {
    type Value: for<'a> Deserialize<'a> + Clone;
    type Item: MakeListItem;

    /// Given a redis connection,  key and namespace modifier, retrieve a list of values
    /// A modifier allows run time modification of namespace, e.g. when rendering a list
    /// of top pages, the modifier can be used to either query by country or by wiki.
    /// This method can mutate to update state.
    fn poll_namespaced<K: ToRedisKey, F>(
        &mut self,
        con: &mut ClusterConnection,
        key: &K,
        namespace_modifier: F,
    ) -> Result<Vec<Self::Value>>
    where
        F: Fn(&str) -> String;

    /// Create items that can be rendered, using state stored in self.
    fn make_items(&self) -> Vec<Self::Item>;

    fn name(&self) -> &str;
    /// mutable vec of values
    fn values(&mut self) -> &mut Vec<Self::Value>;
    /// mutable state of the list
    fn list_state(&mut self) -> &mut ListState;

    /// Given a redis connection and a key, retrieve a list of values
    fn poll<K: ToRedisKey>(
        &mut self,
        con: &mut ClusterConnection,
        key: &K,
    ) -> Result<Vec<Self::Value>> {
        self.poll_namespaced(con, key, |s| s.to_string())
    }

    /// syncronizes the list with the state stored in redis
    fn update<K: ToRedisKey>(&mut self, con: &mut ClusterConnection, key: &K) -> Result<()> {
        let new_values = self.poll(con, key)?;
        let values = self.values();
        *values = new_values;
        self.attempt_select();
        Ok(())
    }

    /// syncronizes the list with the state stored in redis
    fn update_namespaced<K: ToRedisKey, F>(
        &mut self,
        con: &mut ClusterConnection,
        key: &K,
        namespace_modifier: F,
    ) -> Result<()>
    where
        F: Fn(&str) -> String,
    {
        let new_values = self.poll_namespaced(con, key, namespace_modifier)?;
        let values = self.values();
        *values = new_values;
        self.attempt_select();
        Ok(())
    }

    /// renders the trait in the provided frame and rect
    fn render(&mut self, frame: &mut Frame, rect: Rect, focused: bool, title: Option<String>) {
        let title = title.unwrap_or(self.name().to_string());
        let highlight_bg = if focused {
            Color::Green
        } else {
            Color::LightCyan
        };

        let list_items: Vec<ListItem> = self
            .make_items()
            .into_iter()
            .map(|item| item.list_item())
            .collect();

        let list = List::new(list_items)
            .block(Block::default().borders(Borders::ALL).title(title))
            .highlight_style(
                Style::default()
                    .bg(highlight_bg)
                    .add_modifier(Modifier::BOLD),
            );

        frame.render_stateful_widget(list, rect, self.list_state());
    }

    // list state control helper methods
    fn next(&mut self) {
        if !self.values().is_empty() {
            let i = match self.list_state().selected() {
                Some(i) => {
                    if i >= self.values().len() - 1 {
                        0
                    } else {
                        i + 1
                    }
                }
                None => 0,
            };
            self.list_state().select(Some(i));
        }
    }

    fn previous(&mut self) {
        if !self.values().is_empty() {
            let i = match self.list_state().selected() {
                Some(i) => {
                    if i == 0 {
                        self.values().len() - 1
                    } else {
                        i - 1
                    }
                }
                None => 0,
            };
            self.list_state().select(Some(i));
        }
    }

    fn attempt_select(&mut self) {
        if !self.values().is_empty() {
            match &self.list_state().selected() {
                Some(i) => {
                    let last_i = self.values().len() - 1;
                    if *i > last_i {
                        self.list_state().select(Some(last_i));
                    }
                }
                None => {
                    self.list_state().select(Some(0));
                }
            }
        } else {
            self.list_state().select(None)
        }
    }

    fn selected(&mut self) -> Option<&Self::Value> {
        self.list_state()
            .selected()
            .map(|index| &self.values()[index])
    }
}

// define how an item is rendered
pub trait MakeListItem {
    fn list_item(self) -> ListItem<'static>;
    fn name(&self) -> String;
}
