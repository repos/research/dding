pub mod list;
pub mod topk;
pub mod trend;

use once_cell::sync::Lazy;

pub fn initialize_panic_handler() {
    let original_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        crossterm::execute!(std::io::stderr(), crossterm::terminal::LeaveAlternateScreen).unwrap();
        crossterm::terminal::disable_raw_mode().unwrap();
        original_hook(panic_info);
    }));
}

static MAXMIND_CITY: Lazy<maxminddb::Reader<Vec<u8>>> = Lazy::new(|| {
    maxminddb::Reader::open_readfile("/usr/share/GeoIP/GeoIP2-City.mmdb")
        .expect("Error opening maxmind database")
});
static MAXMIND_ISP: Lazy<maxminddb::Reader<Vec<u8>>> = Lazy::new(|| {
    maxminddb::Reader::open_readfile("/usr/share/GeoIP/GeoIP2-ISP.mmdb")
        .expect("Error opening maxmind database")
});

/// Enrich an IP with ISP/country/city metadata from maxmind
pub fn ip_lookup(ip: &String) -> String {
    ip.clone()
        .parse()
        .ok()
        .and_then(|ip_addr| {
            let result: maxminddb::geoip2::City = MAXMIND_CITY.lookup(ip_addr).ok()?;
            let city = result
                .city
                .and_then(|c| c.names)
                .and_then(|n| n.get("en").map(|n| n.to_string()))
                .unwrap_or("Unknown".to_string());

            let country = result
                .country
                .and_then(|c| c.names)
                .and_then(|n| n.get("en").map(|n| n.to_string()))
                .unwrap_or("Unknown".to_string());

            let isp = MAXMIND_ISP
                .lookup::<maxminddb::geoip2::Isp>(ip_addr)
                .ok()
                .and_then(|i| i.isp)
                .unwrap_or("Unknown");

            Some(format!("{}/{}, ISP: {}", country, city, isp))
        })
        .unwrap_or("Unknown".to_string())
}
