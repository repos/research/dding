use std::{
    error::Error,
    time::{Duration, Instant},
};

use clap::Parser;
use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use dding_cli::{ip_lookup, list::InteractiveList as _, topk::TopKList};
use domain::api::{Country, Fingerprint, Page, PageviewsSeries, TopK, Wiki};
use ratatui::{prelude::*, widgets::*};
use redis::cluster::ClusterClient;
use time::{format_description, OffsetDateTime};

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// country to get top pages
    #[arg(short, long)]
    country: Option<String>,

    /// country to get top pages, e.g. en
    #[arg(short, long)]
    wiki: Option<String>,

    /// list of pages, e.g. -w fr -p Ségolène_Royal
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    page: Vec<String>,

    /// Reload top pages each five seconds
    #[arg(long, default_value_t = false)]
    reload: bool,

    /// host for data api
    #[arg(short, long, default_value = "localhost:3001")]
    api_host: String,

    /// List of redis nodes host:port
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    redis_nodes: Vec<String>,
}

enum Focus {
    TopPages,
    TopIps,
}

#[derive(Debug)]
enum TopPagesType {
    Country(Country),
    Wiki(Wiki),
    Pages(Vec<Page>),
}

struct App {
    topk_pages: TopKList<Page>,
    topk_fingerprints: TopKList<Fingerprint>,
    pageviews: Option<PageviewsSeries>,
    focus: Focus,
    top_pages_type: TopPagesType,
    api_host: String,
    client: ClusterClient,
    refresh_rate: Option<Duration>,
}

impl App {
    fn new(top_pages_type: TopPagesType, api_host: String, client: ClusterClient) -> App {
        let event_time_zero = dding::db::read_time_config(&client)
            .expect("error reading event time config from redis");

        App {
            topk_pages: TopKList::new("Top Pages", "topk", &event_time_zero),
            topk_fingerprints: TopKList::new("Top Fingerprints", "topk", &event_time_zero),
            pageviews: None,
            focus: Focus::TopPages,
            top_pages_type,
            api_host,
            client,
            refresh_rate: None,
        }
    }

    fn update_topk_pages(&mut self) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        let prefixed = |ns: &_| format!("page:{ns}");
        match &self.top_pages_type {
            TopPagesType::Country(country) => self
                .topk_pages
                .update_namespaced(&mut con, country, prefixed)?,
            TopPagesType::Wiki(wiki) => self
                .topk_pages
                .update_namespaced(&mut con, wiki, prefixed)?,
            TopPagesType::Pages(pages) => {
                let con = &mut self.client.get_connection()?;
                // no-op hack to force updating evaluation timestamp
                self.topk_pages.update(con, &())?;
                let time_as_of = self.topk_pages.evaluate_at_timestamp();
                let values = pages
                    .iter()
                    .map(|p| {
                        let mut dv = dding::db::get_decayed_pageviews(con, p)
                            .expect(format!("No value for {p:?}").as_str());
                        // .unwrap_or(DecayedValue::zero().to_domain())
                        dv.update_as_of(time_as_of);
                        (p.clone(), dv)
                    })
                    .collect();
                self.topk_pages.values = values;
            }
        }

        self.update_page()?;
        Ok(())
    }

    fn update_topk_fingerprints(&mut self, page: &Page) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        let prefixed = |ns: &_| format!("fingerprint:{ns}");
        self.topk_fingerprints
            .update_namespaced(&mut con, page, prefixed)?;
        Ok(())
    }

    fn update_pageviews(&mut self, page: &Page) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        match dding::db::get_pageviews_series(&mut con, page) {
            Ok(series) => {
                let ps = PageviewsSeries {
                    page: page.clone(),
                    series,
                };
                self.pageviews = Some(ps)
            }
            Err(_) => (),
        }
        Ok(())
    }

    fn update_page(&mut self) -> Result<(), Box<dyn Error>> {
        self.topk_pages.attempt_select();
        if let Some((page, _)) = self.topk_pages.selected().cloned() {
            self.update_topk_fingerprints(&page)?;
            self.update_pageviews(&page)?;
        }
        Ok(())
    }
}

/// Parse from url, e.g. https://de.wikipedia.org/wiki/Matilda-Effekt
pub fn page_from_url(url: &str) -> Option<Page> {
    let url: &str = url.strip_prefix("https://").unwrap_or(url);
    let (host, uri) = url.split_once('/')?;
    let (project, family) = match host.split('.').collect::<Vec<&str>>()[..] {
        [project, family, _] => Some((project, family)),
        _ => None,
    }?;
    let title = match uri.split('/').collect::<Vec<&str>>()[..] {
        ["wiki", title] => Some(title),
        _ => None,
    }?;
    Some(Page {
        title: title.to_string(),
        project: project.to_string(),
        family: family.to_string(),
    })
}
fn main() -> Result<(), Box<dyn Error>> {
    // create app and run it
    let args = Args::parse();

    let redis_nodes: Vec<String> = args
        .redis_nodes
        .iter()
        .map(|h| format!("redis://{h}"))
        .collect();

    let client = redis::cluster::ClusterClient::new(redis_nodes)?;

    let top_pages_type = match (args.wiki, args.country, args.page.as_slice()) {
        (None, None, &[]) => TopPagesType::Wiki(Wiki {
            project: "en".to_string(),
            family: "wikipedia".to_string(),
        }),
        (None, Some(country), &[]) => TopPagesType::Country(Country(country)),
        (Some(wiki), None, &[]) => TopPagesType::Wiki(Wiki {
            project: wiki,
            family: "wikipedia".to_string(),
        }),
        (Some(wiki), None, pages) => TopPagesType::Pages(
            pages
                .iter()
                .map(|t| Page {
                    title: t.clone(),
                    project: wiki.clone(),
                    family: "wikipedia".to_string(),
                })
                .collect(),
        ),
        (None, None, urls) => {
            TopPagesType::Pages(urls.iter().flat_map(|url| page_from_url(url)).collect())
        }
        _ => panic!("invalid config"),
    };
    let tick_rate = Duration::from_millis(250);
    let mut app = App::new(top_pages_type, args.api_host, client);

    if args.reload {
        app.refresh_rate = Some(Duration::from_secs(5))
    }

    // setup terminal
    dding_cli::initialize_panic_handler();
    enable_raw_mode()?;
    let mut stdout: std::io::Stdout = std::io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let result = run_app(&mut terminal, app, tick_rate);

    // restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen,)?;
    terminal.show_cursor()?;

    result
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> Result<(), Box<dyn Error>> {
    let mut last_tick = Instant::now();
    let mut last_refresh = Instant::now();
    let mut last_page_index: usize = 0;

    app.update_topk_pages()?;
    app.update_page()?;

    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press {
                    match key.code {
                        KeyCode::Char('q') => return Ok(()),
                        KeyCode::Char('r') => {
                            app.update_topk_pages().expect("top pages update failed")
                        }
                        KeyCode::Left => match app.focus {
                            Focus::TopPages => {}
                            Focus::TopIps => app.focus = Focus::TopPages,
                        },
                        KeyCode::Right => match app.focus {
                            Focus::TopPages => app.focus = Focus::TopIps,
                            Focus::TopIps => {}
                        },
                        KeyCode::Down => match app.focus {
                            Focus::TopPages => app.topk_pages.next(),
                            Focus::TopIps => app.topk_fingerprints.next(),
                        },
                        KeyCode::Up => match app.focus {
                            Focus::TopPages => app.topk_pages.previous(),
                            Focus::TopIps => app.topk_fingerprints.previous(),
                        },
                        _ => {}
                    }
                }
            }
        }

        if let Some(selected_index) = app.topk_pages.state.selected() {
            if selected_index != last_page_index {
                app.update_page().expect("error updating page");
                last_page_index = selected_index;
            }
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
        if let Some(refresh_rate) = app.refresh_rate {
            if last_refresh.elapsed() >= refresh_rate {
                app.update_topk_pages().expect("top page update failed");
                last_refresh = Instant::now();
            }
        }
    }
}

fn ui(f: &mut Frame, app: &mut App) {
    let lists_chart = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(70), Constraint::Percentage(30)])
        .split(f.size());

    let top_lists = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
        .split(lists_chart[0]);

    let top_pages_rec = top_lists[0];
    let top_fingerprints_rec = top_lists[1];
    let chart_rec = lists_chart[1];

    let title = match &app.top_pages_type {
        TopPagesType::Country(c) => Some(format!("Top pages visited from {}", c.0)),
        TopPagesType::Wiki(w) => Some(format!("Top pages visited on {}.{}", w.project, w.family)),
        TopPagesType::Pages(_) => None,
    };

    app.topk_pages.render(f, top_pages_rec, false, title);
    app.topk_fingerprints
        .render(f, top_fingerprints_rec, false, None);

    if let Some(pageviews) = &app.pageviews {
        let data: Vec<(f64, f64)> = pageviews
            .series
            .iter()
            .map(|&(x, y)| (x as f64, y))
            .collect();

        let x_range = [
            data.first().unwrap_or(&(0.0, 0.0)).0,
            data.last().unwrap_or(&(1.0, 0.0)).0,
        ];
        let y_range = [
            data.iter().map(|xy| xy.1 as u64).min().unwrap_or(0) as f64,
            data.iter().map(|xy| xy.1 as u64).max().unwrap_or(1) as f64,
        ];

        let datasets = vec![Dataset::default()
            .name(pageviews.page.title.to_string())
            .marker(symbols::Marker::Braille)
            .graph_type(GraphType::Line)
            .style(Style::default().fg(Color::Magenta))
            .data(&data)];

        let date_format = format_description::parse("[hour]:[minute]:[second]").unwrap();
        let chart = Chart::new(datasets)
            .block(Block::default().title("Decaying Pageviews"))
            .x_axis(
                Axis::default()
                    .title(Span::styled("unix epoch", Style::default().fg(Color::Red)))
                    .style(Style::default().fg(Color::White))
                    .bounds(x_range)
                    .labels(
                        x_range
                            .iter()
                            .map(|t| {
                                OffsetDateTime::from_unix_timestamp(*t as i64)
                                    .unwrap()
                                    .format(&date_format)
                                    .unwrap_or("date format error".to_string())
                                    .to_string()
                            })
                            .map(Span::from)
                            .collect::<Vec<Span>>(),
                    ),
            )
            .y_axis(
                Axis::default()
                    .title(Span::styled("value", Style::default().fg(Color::Red)))
                    .style(Style::default().fg(Color::White))
                    .bounds(y_range)
                    .labels(
                        y_range
                            .iter()
                            .map(|f| format!("{f}"))
                            .map(Span::from)
                            .collect::<Vec<Span>>(),
                    ),
            );

        f.render_widget(chart, chart_rec);
    }
}
