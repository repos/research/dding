use std::time::{Duration, Instant};

use anyhow::Result;
use clap::Parser;
use crossterm::{
    event::{Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use dding_cli::{
    list::{InteractiveList, MakeListItem},
    trend::TrendList,
};
use domain::api::{BaseUrl, Country, Page, Trend};
use ratatui::{prelude::*, widgets::*};
use redis::cluster::ClusterClient;
use serde::Deserialize;
use time::OffsetDateTime;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// List of redis nodes host:port
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    redis_nodes: Vec<String>,
    /// Redis namespace of trending elements
    #[arg(short, long)]
    namespace: String,
}

struct App<T>
where
    T: Clone + for<'a> Deserialize<'a>,
    Trend<T>: MakeListItem,
{
    trends: TrendList<T>,
    client: ClusterClient,
}
impl<T> App<T>
where
    T: Clone + for<'a> Deserialize<'a>,
    Trend<T>: MakeListItem,
{
    fn new(client: ClusterClient, namespace: &str) -> App<T> {
        App {
            trends: TrendList::new("Trends", namespace),
            // pre_features: None,
            // post_features: None,
            client,
        }
    }
    fn update(&mut self) -> Result<()> {
        let con = &mut self.client.get_connection()?;
        self.trends.update(con, &())?;
        self.trends.attempt_select();
        Ok(())
    }
}
fn main() -> Result<()> {
    let args = Args::parse();

    let redis_nodes: Vec<String> = args
        .redis_nodes
        .iter()
        .map(|h| format!("redis://{h}"))
        .collect();

    let client = ClusterClient::new(redis_nodes)?;

    let tick_rate = Duration::from_millis(250);

    dding_cli::initialize_panic_handler();
    // setup terminal
    enable_raw_mode()?;
    let mut stdout: std::io::Stdout = std::io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let result = match args.namespace.as_str() {
        "pages" => {
            let app: App<Page> = App::new(client, &args.namespace);
            run_app(&mut terminal, app, tick_rate)
        }
        "cache_misses:host_path" => {
            let app: App<BaseUrl> = App::new(client, &args.namespace);
            run_app(&mut terminal, app, tick_rate)
        }
        "cache_misses:country" => {
            let app: App<Country> = App::new(client, &args.namespace);
            run_app(&mut terminal, app, tick_rate)
        }
        ns => panic!("unknown namespace {ns}"),
    };

    // } else {
    //     let app: App<BaseUrl> = App::new(client, &args.namespace);
    //     run_app(&mut terminal, app, tick_rate)
    // };

    // restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen,)?;
    terminal.show_cursor()?;

    let _ = result.expect("error running app");

    // let con = client.get_connection()?;

    // let date_format = time::format_description::parse("[year]-[month]-[day] [hour]:[minute]")?;

    // let mut active_trends: Vec<ActiveTrend<BaseUrl>> =
    //     dding::db::get_active_trends(&mut con, &args.namespace)?;
    // active_trends.sort_by_key(|t| t.start);
    // for t in active_trends {
    //     let start = t.start.format(&date_format)?;
    //     let url = t.element.url_string();
    //     let line = format!("{start}\t{url}");
    //     println!("{line}")
    // }

    // let mut active_trends: Vec<ActiveTrend<Page>> =
    //     dding::db::get_active_trends(&mut con, &args.namespace)?;
    // active_trends.sort_by_key(|t| (t.element.project.clone(), t.start));
    // for t in active_trends {
    //     let start = t.start.format(&date_format)?;
    //     let url = t.element.base_url().url_string();
    //     let line = format!("{start}\t{url}");
    //     println!("{line}")
    // }

    // let mut past_trends: Vec<PastTrend<Page>> = dding::db::get_past_trends(&mut con, namespace)?;
    // past_trends.sort_by_key(|t| t.start);
    // for t in past_trends {
    //     let start = t.start.format(&date_format)?;
    //     let end = t.end.format(&date_format)?;
    //     let url = t.element.base_url().url_string();
    //     let line = format!("{start}\t{end}\t{url}");
    //     println!("{line}")
    // }

    Ok(())
}
fn run_app<T, B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App<T>,
    tick_rate: Duration,
) -> Result<()>
where
    T: Clone + for<'a> Deserialize<'a>,
    Trend<T>: MakeListItem,
{
    let mut last_tick = Instant::now();

    app.update()?;

    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = crossterm::event::read()? {
                if key.kind == KeyEventKind::Press {
                    match key.code {
                        KeyCode::Char('q') => return Ok(()),
                        KeyCode::Char('r') => app.update()?,
                        KeyCode::Char('t') => {
                            app.trends.toggle_type();
                            app.update()?;
                            app.trends.attempt_select();
                        }
                        KeyCode::Down => app.trends.next(),
                        KeyCode::Up => app.trends.previous(),
                        _ => {}
                    }
                }
            }
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
    }
}

fn ui<T>(frame: &mut Frame, app: &mut App<T>)
where
    T: Clone + for<'a> Deserialize<'a>,
    Trend<T>: MakeListItem,
{
    let lists_chart = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(1),
            Constraint::Percentage(50),
            Constraint::Percentage(50),
        ])
        .split(frame.size());

    let help_rec = lists_chart[0];
    let trends_rec = lists_chart[1];
    let plot_rec = lists_chart[2];

    let help: Line = vec![
        "Keys: ".italic(),
        "arrows".bold(),
        " change focus | ".into(),
        "t".bold(),
        " toggle active/past | ".into(),
        "r".bold(),
        " refresh | ".into(),
        "q".bold(),
        " quit | ".into(),
    ]
    .into();

    frame.render_widget(Paragraph::new(help), help_rec);

    app.trends.render(frame, trends_rec, true, None);

    // if let Some(pageviews) = &app.trends.selected() {
    if let Some(trend) = app.trends.selected() {
        let pre_detection: Vec<(f64, f64)> = trend
            .pre_detection
            .iter()
            .map(|(x, y)| (*x as f64, *y))
            .collect();

        let post_detection: Vec<(f64, f64)> = trend
            .post_detection
            .iter()
            .map(|(x, y)| (*x as f64, *y))
            .collect();

        let last_ts = post_detection
            .last()
            .or(pre_detection.last())
            .expect("values required")
            .0;
        let x_range = [pre_detection.first().expect("values required").0, last_ts];

        let y_range = [
            pre_detection
                .iter()
                .chain(post_detection.iter())
                .map(|xy| xy.1 as u64)
                .min()
                .unwrap_or(0) as f64,
            pre_detection
                .iter()
                .chain(post_detection.iter())
                .map(|xy| xy.1 as u64)
                .max()
                .unwrap_or(1) as f64,
        ];

        let datasets = vec![
            Dataset::default()
                .name("before detection")
                .marker(symbols::Marker::Braille)
                .graph_type(GraphType::Line)
                .style(Style::default().fg(Color::Magenta))
                .data(&pre_detection),
            Dataset::default()
                .name("post detection")
                .marker(symbols::Marker::Braille)
                .graph_type(GraphType::Line)
                .style(Style::default().fg(Color::Blue))
                .data(&post_detection),
        ];

        let date_format = time::format_description::parse("[hour]:[minute]:[second]").unwrap();
        let chart = Chart::new(datasets)
            // .block(Block::default().title(trend.element.base_url().url_string()))
            .block(Block::default().title(trend.name()))
            .x_axis(
                Axis::default()
                    // .title(Span::styled(
                    //     "observations",
                    //     Style::default().fg(Color::Red),
                    // ))
                    .style(Style::default().fg(Color::White))
                    .bounds(x_range)
                    .labels(
                        x_range
                            .iter()
                            .map(|t| {
                                OffsetDateTime::from_unix_timestamp(*t as i64)
                                    .expect("invalid unix timestamp")
                                    .format(&date_format)
                                    .unwrap_or("date format error".to_string())
                                    .to_string()
                            })
                            .map(Span::from)
                            .collect::<Vec<Span>>(),
                    ),
            )
            .y_axis(
                Axis::default()
                    // .title(Span::styled("value", Style::default().fg(Color::Red)))
                    .style(Style::default().fg(Color::White))
                    .bounds(y_range)
                    .labels(
                        y_range
                            .iter()
                            .map(|f| format!("{f}"))
                            .map(Span::from)
                            .collect::<Vec<Span>>(),
                    ),
            );

        frame.render_widget(chart, plot_rec);
    }

    // }
}
