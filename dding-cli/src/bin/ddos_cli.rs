use std::{
    error::Error,
    time::{Duration, Instant},
    vec,
};

use clap::Parser;
use crossterm::{
    event::{self, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};

use dding_cli::topk::TopKList;
use dding_cli::{list::InteractiveList, trend::TrendList};
use domain::api::{BaseUrl, Country, Fingerprint, Isp, Url};
use ratatui::{prelude::*, widgets::*};
use redis::cluster::ClusterClient;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// List of redis nodes host:port
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    redis_nodes: Vec<String>,

    /// Reload counts every 5 seconds
    #[arg(long, default_value_t = false)]
    reload: bool,
}

#[derive(Debug, PartialEq)]
enum Focus {
    Global,
    Fingerprint,
    Uri,
}

#[derive(Debug)]
enum Views {
    Country(Mode),
    Isp(Mode),
    HostPath(Mode),
}
#[derive(Debug, PartialEq, Eq)]
enum Mode {
    TopK,
    Trend,
}
struct App {
    topk_country: TopKList<Country>,
    trend_country: TrendList<Country>,
    topk_isp: TopKList<Isp>,
    topk_host_path: TopKList<BaseUrl>,
    trend_host_path: TrendList<BaseUrl>,
    topk_fingerprint: TopKList<Fingerprint>,
    topk_uri: TopKList<Url>,
    focus: Focus,
    view: Views,
    client: ClusterClient,
    refresh_rate: Option<Duration>,
}

impl App {
    fn new(client: ClusterClient) -> App {
        let event_time_zero = dding::db::read_time_config(&client)
            .expect("error reading event time config from redis");
        App {
            topk_country: TopKList::new("Cache-misses per Country", "topk", &event_time_zero),
            trend_country: TrendList::new(
                "Cache-misses trending per Country",
                "cache_misses:country",
            ),
            topk_isp: TopKList::new("Cache-misses per ISP", "topk", &event_time_zero),
            topk_host_path: TopKList::new("Cache-misses per Host/Path", "topk", &event_time_zero),
            trend_host_path: TrendList::new(
                "Cache-misses trending per Host/Path",
                "cache_misses:host_path",
            ),
            topk_fingerprint: TopKList::new(
                "Top Fingerprints",
                "fingerprint:topk",
                &event_time_zero,
            ),
            topk_uri: TopKList::new("Top URI", "uri:topk", &event_time_zero),
            focus: Focus::Global,
            view: Views::HostPath(Mode::TopK),
            client,
            refresh_rate: None,
        }
    }

    fn update_global(&mut self) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        match self.view {
            Views::Country(Mode::TopK) => {
                self.topk_country.update(&mut con, &"global:country")?;
                self.topk_country.attempt_select();
            }
            Views::Country(Mode::Trend) => {
                self.trend_country.update(&mut con, &())?;
                self.trend_country.attempt_select();
            }
            Views::Isp(Mode::TopK) => {
                self.topk_isp.update(&mut con, &"global:isp")?;
                self.topk_isp.attempt_select();
            }
            Views::HostPath(Mode::TopK) => {
                self.topk_host_path.update(&mut con, &"global:host_path")?;
                self.topk_host_path.attempt_select();
            }
            Views::HostPath(Mode::Trend) => {
                self.trend_host_path.update(&mut con, &())?;
                self.trend_host_path.attempt_select();
            }
            _ => todo!("unimplemented view"),
        };
        self.update_topk_fingerprints()?;
        self.update_topk_uri()?;

        Ok(())
    }

    fn update_topk_fingerprints(&mut self) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        match self.view {
            Views::Country(Mode::TopK) => {
                if let Some((key, _)) = self.topk_country.selected() {
                    self.topk_fingerprint.update(&mut con, key)?
                }
            }
            Views::Country(Mode::Trend) => {
                if let Some(key) = self.trend_country.selected() {
                    self.topk_fingerprint.update(&mut con, &key.element)?
                }
            }
            Views::Isp(Mode::TopK) => {
                if let Some((key, _)) = self.topk_isp.selected() {
                    self.topk_fingerprint.update(&mut con, key)?
                }
            }
            Views::HostPath(Mode::TopK) => {
                if let Some((key, _)) = self.topk_host_path.selected() {
                    let prefixed = |ns: &_| format!("host_path:{ns}");
                    self.topk_fingerprint
                        .update_namespaced(&mut con, key, prefixed)?
                }
            }
            Views::HostPath(Mode::Trend) => {
                if let Some(key) = self.trend_host_path.selected() {
                    // let key = &self.trend_host_path.selected().element;
                    let prefixed = |ns: &_| format!("host_path:{ns}");
                    self.topk_fingerprint
                        .update_namespaced(&mut con, &key.element, prefixed)?
                }
            }
            _ => todo!("unimplemented view"),
        };
        self.topk_fingerprint.attempt_select();
        Ok(())
    }

    fn update_topk_uri(&mut self) -> Result<(), Box<dyn Error>> {
        let mut con = self.client.get_connection()?;
        match self.focus {
            Focus::Global => match self.view {
                Views::Country(Mode::TopK) => {
                    if let Some((key, _)) = self.topk_country.selected() {
                        self.topk_uri.update(&mut con, key)?
                    }
                }
                Views::Country(Mode::Trend) => {
                    if let Some(key) = self.trend_country.selected().cloned() {
                        self.trend_country.update(&mut con, &key.element)?
                    }
                }
                Views::Isp(Mode::TopK) => {
                    if let Some((key, _)) = self.topk_isp.selected() {
                        self.topk_uri.update(&mut con, key)?
                    }
                }
                Views::HostPath(Mode::TopK) => {
                    if let Some((key, _)) = self.topk_host_path.selected() {
                        let prefixed = |ns: &_| format!("host_path:{ns}");
                        self.topk_uri.update_namespaced(&mut con, key, prefixed)?
                    }
                }
                Views::HostPath(Mode::Trend) => {
                    if let Some(key) = self.trend_host_path.selected() {
                        let prefixed = |ns: &_| format!("host_path:{ns}");
                        self.topk_uri
                            .update_namespaced(&mut con, &key.element, prefixed)?
                    }
                }
                _ => todo!("unimplemented view"),
            },
            Focus::Fingerprint => {
                if let Some((key, _)) = self.topk_fingerprint.selected().cloned() {
                    match self.view {
                        Views::Country(_) => {
                            let prefixed = |ns: &_| format!("country:{ns}");
                            self.topk_uri.update_namespaced(&mut con, &key, prefixed)?
                        }
                        Views::Isp(_) => {
                            let prefixed = |ns: &_| format!("isp:{ns}");
                            self.topk_uri.update_namespaced(&mut con, &key, prefixed)?
                        }
                        Views::HostPath(_) => {
                            let prefixed = |ns: &_| format!("host_path:fingerprint:{ns}");
                            self.topk_uri.update_namespaced(&mut con, &key, prefixed)?
                        }
                        _ => todo!("unimplemented view"),
                    }
                }
            }
            Focus::Uri => panic!("nope"),
        };

        Ok(())
    }
    // arrow left
    pub fn left(&mut self) {
        match self.focus {
            Focus::Fingerprint => {
                self.focus = Focus::Global;
                self.update_topk_uri().expect("error updating uri");
            }
            _ => (),
        }
    }
    // arrow right
    pub fn right(&mut self) {
        match self.focus {
            Focus::Global => {
                if !self.topk_fingerprint.values().is_empty() {
                    self.focus = Focus::Fingerprint;
                    self.update_topk_uri().expect("error updating uri");
                }
            }
            _ => (),
        };
    }
    // move selection for relevant focus on arrow down
    pub fn down(&mut self) {
        match self.focus {
            Focus::Global => {
                match self.view {
                    Views::Country(Mode::TopK) => self.topk_country.next(),
                    Views::Country(Mode::Trend) => self.trend_country.next(),
                    Views::Isp(Mode::TopK) => self.topk_isp.next(),
                    Views::HostPath(Mode::TopK) => self.topk_host_path.next(),
                    Views::HostPath(Mode::Trend) => self.trend_host_path.next(),
                    _ => todo!("unimplemented view"),
                };
                self.update_topk_fingerprints()
                    .expect("error updating fingerprint");
                self.update_topk_uri().expect("error updating uri");
            }
            Focus::Fingerprint => {
                self.topk_fingerprint.next();
                self.update_topk_uri().expect("error updating uri");
            }
            Focus::Uri => self.topk_uri.next(),
        };
    }
    // move selection for relevant focus on arrow up
    pub fn up(&mut self) {
        match self.focus {
            Focus::Global => {
                match self.view {
                    Views::Country(Mode::TopK) => self.topk_country.previous(),
                    Views::Country(Mode::Trend) => self.trend_country.previous(),
                    Views::Isp(Mode::TopK) => self.topk_isp.previous(),
                    Views::HostPath(Mode::TopK) => self.topk_host_path.previous(),
                    Views::HostPath(Mode::Trend) => self.trend_host_path.previous(),
                    _ => todo!("unimplemented view"),
                };

                self.update_topk_fingerprints().expect("error updating ip");
                self.update_topk_uri().expect("error updating uri");
            }
            Focus::Fingerprint => {
                self.topk_fingerprint.previous();
                self.update_topk_uri().expect("error updating uri");
            }
            Focus::Uri => self.topk_uri.previous(),
        };
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    // create app and run it
    let args = Args::parse();

    let tick_rate = Duration::from_millis(250);
    let redis_nodes: Vec<String> = args
        .redis_nodes
        .iter()
        .map(|h| format!("redis://{h}"))
        .collect();

    let client = redis::cluster::ClusterClient::new(redis_nodes)?;

    let mut app = App::new(client);

    app.update_global()?;

    if args.reload {
        app.refresh_rate = Some(Duration::from_secs(5))
    }

    dding_cli::initialize_panic_handler();
    // setup terminal
    enable_raw_mode()?;
    let mut stdout: std::io::Stdout = std::io::stdout();
    execute!(stdout, EnterAlternateScreen)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let result = run_app(&mut terminal, app, tick_rate);

    // restore terminal
    disable_raw_mode()?;
    execute!(terminal.backend_mut(), LeaveAlternateScreen,)?;
    terminal.show_cursor()?;

    let _ = result.expect("error running app");
    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> Result<(), Box<dyn Error>> {
    let mut last_tick = Instant::now();
    let mut last_refresh = Instant::now();

    app.update_global()?;

    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press {
                    match key.code {
                        KeyCode::Char('q') => return Ok(()),
                        KeyCode::Char('r') => match app.focus {
                            Focus::Global => app.update_global()?,
                            Focus::Fingerprint => app.update_topk_uri()?,
                            Focus::Uri => (),
                        },
                        KeyCode::Char('h') => {
                            app.view = Views::HostPath(Mode::TopK);
                            app.focus = Focus::Global;
                            app.update_global()?
                        }
                        KeyCode::Char('i') => {
                            app.view = Views::Isp(Mode::TopK);
                            app.focus = Focus::Global;
                            app.update_global()?
                        }
                        KeyCode::Char('c') => {
                            app.view = Views::Country(Mode::TopK);
                            app.focus = Focus::Global;
                            app.update_global()?
                        }
                        KeyCode::Char('t') => {
                            match app.view {
                                Views::Country(Mode::TopK) => {
                                    app.view = Views::Country(Mode::Trend)
                                }
                                Views::Country(Mode::Trend) => {
                                    app.view = Views::Country(Mode::TopK)
                                }
                                Views::Isp(_) => (),
                                Views::HostPath(Mode::TopK) => {
                                    app.view = Views::HostPath(Mode::Trend)
                                }
                                Views::HostPath(Mode::Trend) => {
                                    app.view = Views::HostPath(Mode::TopK)
                                }
                            }
                            // app.view = Views::Country(Mode::TopK);
                            // app.focus = Focus::Global;
                            app.update_global()?
                        }
                        KeyCode::Left => app.left(),
                        KeyCode::Right => app.right(),
                        KeyCode::Down => app.down(),
                        KeyCode::Up => app.up(),
                        _ => {}
                    }
                }
            }
        }

        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
        if let Some(refresh_rate) = app.refresh_rate {
            if last_refresh.elapsed() >= refresh_rate {
                app.update_global().expect("refresh failed");
                last_refresh = Instant::now();
            }
        }
    }
}

fn ui(frame: &mut Frame, app: &mut App) {
    let lists_chart = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Length(1),
            Constraint::Percentage(50),
            Constraint::Percentage(50),
        ])
        .split(frame.size());

    let top_lists = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
        .split(lists_chart[1]);

    let help_rec = lists_chart[0];
    let glob_rec = top_lists[0];
    let fingerprints_rec = top_lists[1];
    let uri_rec = lists_chart[2];

    let help: Line = vec![
        "Keys: ".italic(),
        "arrows".bold(),
        " change focus | ".into(),
        "t".bold(),
        " toogle topk/trend | ".into(),
        "h".bold(),
        " host/path view | ".into(),
        "c".bold(),
        " countries view | ".into(),
        "i".bold(),
        " isp view | ".into(),
        "r".bold(),
        " refresh | ".into(),
        "q".bold(),
        " quit | ".into(),
    ]
    .into();

    frame.render_widget(Paragraph::new(help), help_rec);

    let focused = |focus: Focus| -> bool { focus == app.focus };

    let global_selected: String = match app.view {
        Views::Country(Mode::TopK) => {
            app.topk_country
                .render(frame, glob_rec, focused(Focus::Global), None);
            app.topk_country
                .selected()
                .map(|(key, _)| key.0.to_owned())
                .unwrap_or("N/A".to_owned())
        }
        Views::Country(Mode::Trend) => {
            app.trend_country
                .render(frame, glob_rec, focused(Focus::Global), None);
            app.trend_country
                .selected()
                .map(|t| t.element.0.to_owned())
                .unwrap_or("N/A".to_owned())
        }
        Views::Isp(Mode::TopK) => {
            app.topk_isp
                .render(frame, glob_rec, focused(Focus::Global), None);
            app.topk_isp
                .selected()
                .map(|(key, _)| key.0.to_owned())
                .unwrap_or("N/A".to_owned())
        }
        Views::HostPath(Mode::TopK) => {
            app.topk_host_path
                .render(frame, glob_rec, focused(Focus::Global), None);
            app.topk_host_path
                .selected()
                .map(|(uri, _)| format!("{}{}", uri.host, uri.path))
                .unwrap_or("N/A".to_owned())
        }
        Views::HostPath(Mode::Trend) => {
            app.trend_host_path
                .render(frame, glob_rec, focused(Focus::Global), None);
            app.trend_host_path
                .selected()
                .map(|t| t.element.url_string())
                .unwrap_or("N/A".to_owned())
        }
        _ => todo!("unimplemented view"),
    };

    app.topk_fingerprint
        .render(frame, fingerprints_rec, focused(Focus::Fingerprint), None);

    let fingerprint_name = match app.focus {
        Focus::Global => format!("Top URI for {global_selected}"),
        Focus::Fingerprint => app
            .topk_fingerprint
            .selected()
            .map(|(fingerprint, _)| {
                format!(
                    "Top URI for selected fingerprint (ip:{}, ua:{}, accept: {})",
                    fingerprint.ip, fingerprint.user_agent, fingerprint.accept_language
                )
            })
            .unwrap_or("N/A".to_owned()),
        Focus::Uri => panic!("nope"),
    };

    app.topk_uri
        .render(frame, uri_rec, focused(Focus::Uri), Some(fingerprint_name));
}
