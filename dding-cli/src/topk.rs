/// Render interactive lists for topk data
use crate::ip_lookup;
use crate::list::{InteractiveList, MakeListItem};
use domain::api::{BaseUrl, Country, Fingerprint, Isp, Page, Url};
use domain::counting::DecayedValue;
use ratatui::prelude::*;
use ratatui::widgets::{ListItem, ListState};
use serde::Deserialize;
use time::OffsetDateTime;
pub struct TopKList<V: Clone> {
    pub name: String,
    pub state: ListState,
    pub values: Vec<(V, DecayedValue)>,
    namespace: String,
    event_time_zero: OffsetDateTime,
    evaluate_time: OffsetDateTime,
}

impl<V> TopKList<V>
where
    V: for<'a> Deserialize<'a> + Clone,
{
    pub fn new(name: &str, namespace: &str, event_time_zero: &OffsetDateTime) -> Self {
        TopKList {
            name: name.to_string(),
            state: ListState::default(),
            values: vec![],
            namespace: namespace.to_string(),
            event_time_zero: *event_time_zero,
            evaluate_time: *event_time_zero,
        }
    }
    pub fn evaluate_at_timestamp(&self) -> f64 {
        (self.evaluate_time - self.event_time_zero).whole_milliseconds() as f64
    }
}
impl<V: Clone + for<'a> Deserialize<'a>> InteractiveList for TopKList<V>
where
    TopKItem<V>: MakeListItem,
{
    type Value = (V, DecayedValue);

    type Item = TopKItem<V>;
    fn poll_namespaced<K: dding::db::ToRedisKey, F>(
        &mut self,
        con: &mut redis::cluster::ClusterConnection,
        key: &K,
        namespace_modifier: F,
    ) -> anyhow::Result<Vec<Self::Value>>
    where
        F: Fn(&str) -> String,
    {
        self.evaluate_time = OffsetDateTime::now_utc().replace_nanosecond(0)?;
        let namespace = namespace_modifier(&self.namespace);
        let values = match dding::db::get_decayed_topk(con, key, namespace.as_str()) {
            Ok(res) => res.topk,
            Err(e) => {
                // println!(
                //     "error updating for {}. error {e:?}",
                //     key.namespaced(&namespace)
                // );
                vec![]
            }
        };
        Ok(values)
    }
    // fn poll_namespaced<K: dding::db::ToRedisKey>(
    //     &mut self,
    //     con: &mut redis::cluster::ClusterConnection,
    //     key: &K,
    //     namespace_modifier: impl Fn(&str) -> String,
    // ) -> anyhow::Result<Vec<Self::Value>> {
    // }
    fn make_items(&self) -> Vec<Self::Item> {
        self.values
            .iter()
            .enumerate()
            .map(|(rank, (v, dv))| TopKItem {
                rank,
                element: v.clone(),
                value: dv.value_as_of(self.evaluate_at_timestamp()),
            })
            .collect()
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn values(&mut self) -> &mut Vec<Self::Value> {
        &mut self.values
    }

    fn list_state(&mut self) -> &mut ListState {
        &mut self.state
    }
}

#[derive(Clone)]
pub struct TopKItem<V: Clone> {
    pub rank: usize,
    pub element: V,
    pub value: f64,
}

impl MakeListItem for TopKItem<String> {
    fn list_item(self) -> ListItem<'static> {
        let rank = Span::raw(format!("{}. ", &self.rank));
        let element = Span::raw(self.element);
        let value = Span::raw(format!("{:.2}", &self.value));
        let line: Line<'_> = vec![rank, element, " | value: ".into(), value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.clone()
    }
}

impl MakeListItem for TopKItem<Country> {
    fn list_item(self) -> ListItem<'static> {
        let rank = Span::raw(format!("{}. ", &self.rank));
        let element = Span::raw(self.element.0);
        let value = Span::raw(format!("{:.2}", &self.value));
        let line: Line<'_> = vec![rank, element, " | value: ".into(), value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.0.clone()
    }
}

impl MakeListItem for TopKItem<Isp> {
    fn list_item(self) -> ListItem<'static> {
        let rank = Span::raw(format!("{}. ", &self.rank));
        let element = Span::raw(self.element.0);
        let value = Span::raw(format!("{:.2}", &self.value));
        let line: Line<'_> = vec![rank, element, " | value: ".into(), value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.0.clone()
    }
}

impl MakeListItem for TopKItem<Page> {
    fn list_item(self) -> ListItem<'static> {
        let rank = Span::raw(format!("{}. ", &self.rank));
        let element = Span::raw(self.name());
        let value = Span::raw(format!("{:.2}", &self.value));
        let line: Line<'_> = vec![rank, element, " | value: ".into(), value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "https://{}.{}.com/wiki/{}",
            self.element.project, self.element.family, self.element.title
        )
    }
}
impl MakeListItem for TopKItem<Fingerprint> {
    fn list_item(self) -> ListItem<'static> {
        let i = Span::raw(format!("{}. ", &self.rank));
        let ip = Span::raw(self.element.ip.clone()).bold();
        let value = Span::raw(format!("{:.2}", &self.value));
        let ip_line: Line<'_> = vec![i, ip, " | value: ".into(), value].into();

        let geo = Span::raw(ip_lookup(&self.element.ip));
        let geo_line: Line<'_> = geo.into();

        let ua = Span::raw(format!(
            "{} / {}",
            &self.element.user_agent, &self.element.accept_language
        ))
        .italic();
        let ua_line: Line<'_> = ua.into();
        ListItem::new(vec![ip_line, ua_line, geo_line])
            .style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "ip:{}, ua:{}, accept: {}",
            &self.element.ip, &self.element.user_agent, &self.element.accept_language
        )
    }
}

impl MakeListItem for TopKItem<Url> {
    fn list_item(self) -> ListItem<'static> {
        let i = Span::raw(format!("{}. ", &self.rank));
        let host = Span::raw(self.element.host).bold();
        let path = Span::raw(self.element.path);
        let query = Span::raw(self.element.query);
        let value = Span::raw(format!("{:.2}", &self.value));
        let host_path = vec![i, host, path, " | value: ".into(), value].into();
        let query_line = vec!["query: ".into(), query].into();

        ListItem::new(vec![host_path, query_line])
            .style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "{}{}{}",
            &self.element.host, &self.element.path, &self.element.query
        )
    }
}
impl MakeListItem for TopKItem<BaseUrl> {
    fn list_item(self) -> ListItem<'static> {
        let i = Span::raw(format!("{}. ", &self.rank));
        let host = Span::raw(self.element.host).bold();
        let path = Span::raw(self.element.path);
        let value = Span::raw(format!("{:.2}", &self.value));
        let host_path = vec![i, host, path, " | value: ".into(), value].into();

        ListItem::new(vec![host_path]).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!("{}{}", &self.element.host, &self.element.path)
    }
}
