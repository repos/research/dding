/// Render interactive lists for topk data
use crate::ip_lookup;
use crate::list::{InteractiveList, MakeListItem};
use domain::api::{BaseUrl, Country, Fingerprint, Isp, Page, Trend, Url};
use once_cell::sync::Lazy;
use ratatui::prelude::*;
use ratatui::widgets::{ListItem, ListState};
use serde::Deserialize;
use time::format_description::FormatItem;

enum TrendType {
    Active,
    Past,
}

pub struct TrendList<V: Clone> {
    pub name: String,
    pub state: ListState,
    pub values: Vec<Trend<V>>,
    namespace: String,
    trend_type: TrendType,
}

impl<V> TrendList<V>
where
    V: for<'a> Deserialize<'a> + Clone,
{
    pub fn new(name: &str, namespace: &str) -> Self {
        TrendList {
            name: name.to_string(),
            state: ListState::default(),
            values: vec![],
            namespace: namespace.to_string(),
            trend_type: TrendType::Active,
        }
    }

    pub fn toggle_type(&mut self) {
        match self.trend_type {
            TrendType::Active => self.trend_type = TrendType::Past,
            TrendType::Past => self.trend_type = TrendType::Active,
        }
    }
}
impl<V: Clone + for<'a> Deserialize<'a>> InteractiveList for TrendList<V>
where
    Trend<V>: MakeListItem,
{
    type Value = Trend<V>;
    type Item = Trend<V>;

    fn poll_namespaced<K: dding::db::ToRedisKey, F>(
        &mut self,
        con: &mut redis::cluster::ClusterConnection,
        //TODO make use of key instead of modifier
        _key: &K,
        namespace_modifier: F,
    ) -> anyhow::Result<Vec<Self::Value>>
    where
        F: Fn(&str) -> String,
    {
        let namespace = namespace_modifier(&self.namespace);
        let mut trends = match self.trend_type {
            TrendType::Active => dding::db::get_active_trends(con, namespace.as_str()),
            TrendType::Past => dding::db::get_past_trends(con, namespace.as_str()),
        }?;
        // .unwrap_or(vec![]);
        trends.sort_by_key(|t| std::cmp::Reverse(t.start));
        Ok(trends)
    }

    fn make_items(&self) -> Vec<Self::Item> {
        self.values.clone()
    }

    fn name(&self) -> &str {
        &self.name
    }

    fn values(&mut self) -> &mut Vec<Self::Value> {
        &mut self.values
    }

    fn list_state(&mut self) -> &mut ListState {
        &mut self.state
    }
}

// #[derive(Clone)]
// pub struct TopKItem<V: Clone> {
//     pub rank: usize,
//     pub element: V,
//     pub value: f64,
// }

static FORMAT: Lazy<Vec<FormatItem>> = Lazy::new(|| {
    time::format_description::parse("[year]-[month]-[day] [hour]:[minute]").expect("invalid format")
});
impl MakeListItem for Trend<String> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));

        let element = Span::raw(self.element);
        let value = Span::raw(format!(
            " | start value: {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let line: Line<'_> = vec![time, element, value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.clone()
    }
}

impl MakeListItem for Trend<Country> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let element = Span::raw(self.element.0);
        let value = Span::raw(format!(
            "| start value {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let line: Line<'_> = vec![time, element, value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.0.clone()
    }
}

impl MakeListItem for Trend<Isp> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let element = Span::raw(self.element.0);
        let value = Span::raw(format!(
            "| start value {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let line: Line<'_> = vec![time, element, value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        self.element.0.clone()
    }
}

impl MakeListItem for Trend<Page> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let element = Span::raw(self.name());
        let value = Span::raw(format!(
            "| start value {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let line: Line<'_> = vec![time, element, value].into();
        ListItem::new(line).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "https://{}.{}.com/wiki/{}",
            self.element.project, self.element.family, self.element.title
        )
    }
}
impl MakeListItem for Trend<Fingerprint> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let ip = Span::raw(self.element.ip.clone()).bold();
        let value = Span::raw(format!(
            "| start value {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let ip_line: Line<'_> = vec![time, ip, value].into();

        let geo = Span::raw(ip_lookup(&self.element.ip));
        let geo_line: Line<'_> = geo.into();

        let ua = Span::raw(format!(
            "{} / {}",
            &self.element.user_agent, &self.element.accept_language
        ))
        .italic();
        let ua_line: Line<'_> = ua.into();
        ListItem::new(vec![ip_line, ua_line, geo_line])
            .style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "ip:{}, ua:{}, accept: {}",
            &self.element.ip, &self.element.user_agent, &self.element.accept_language
        )
    }
}

impl MakeListItem for Trend<Url> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let host = Span::raw(self.element.host).bold();
        let path = Span::raw(self.element.path);
        let query = Span::raw(self.element.query);
        let value = Span::raw(format!(
            " | value: {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let host_path = vec![time, host, path, " | value: ".into(), value].into();
        let query_line = vec!["\tquery: ".into(), query].into();

        ListItem::new(vec![host_path, query_line])
            .style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!(
            "{}{}{}",
            &self.element.host, &self.element.path, &self.element.query
        )
    }
}
impl MakeListItem for Trend<BaseUrl> {
    fn list_item(self) -> ListItem<'static> {
        let time = Span::raw(format!(
            "{} - {} : ",
            &self.start.format(&FORMAT).expect("format error"),
            &self.end.map_or("now".to_string(), |dt| dt
                .format(&FORMAT)
                .expect("format error"))
        ));
        let host = Span::raw(self.element.host).bold();
        let path = Span::raw(self.element.path);
        let value = Span::raw(format!(
            " | value: {:.2}",
            &self.pre_detection.last().unwrap().1
        ));
        let host_path = vec![time, host, path, value].into();

        ListItem::new(vec![host_path]).style(Style::default().fg(Color::Black).bg(Color::White))
    }

    fn name(&self) -> String {
        format!("{}{}", &self.element.host, &self.element.path)
    }
}
