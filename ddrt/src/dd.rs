use std::{
    cell::RefCell,
    collections::{HashMap, HashSet, VecDeque},
    rc::Rc,
    time::Duration,
};

use domain::api::{Page, TopK, Wiki};
use gloo_timers::callback::Interval;
use poll_promise::Promise;

pub struct TrendingPages {
    period: i32,
    growth: f64,
    pub candidates: HashMap<String, (i32, f64)>,
    pub trending: HashSet<String>,
}

impl TrendingPages {
    pub fn new(period: i32, growth: f64) -> Self {
        TrendingPages {
            period,
            growth,
            candidates: HashMap::new(),
            trending: HashSet::new(),
        }
    }

    pub fn insert(&mut self, topk: &HashMap<String, f64>) {
        for (page, value) in topk {
            let (chain, _) = self
                .candidates
                .entry(page.to_string())
                .and_modify(|(chain, prev)| {
                    if value > &(*prev * self.growth) {
                        *chain += 1;
                    } else {
                        *chain = 0;
                    }
                    *prev = *value;
                })
                .or_insert((0, *value));
            if *chain >= self.period {
                tracing::warn!("TRENDING {page} chain {chain} ");
                self.trending.insert(page.to_string());
            }
        }
    }

    pub fn is_trending(&self, page: &str) -> bool {
        self.trending.contains(page)
    }
}

pub type History = VecDeque<(Secs, HashMap<String, f64>)>;
pub type Secs = i32;
pub struct DDState {
    pub seconds: Secs,
    promise: Option<Promise<Result<TopK<Page>, String>>>,
    pub interval: Option<Interval>,
    pub history: History,
    pub trending: TrendingPages,
}

// if schedule is 5s, 1440 is 2 hours of data
const HISTORY_LENGTH: usize = 1440;

impl DDState {
    pub fn new() -> Self {
        DDState {
            seconds: 0,
            promise: None,
            interval: None,
            history: VecDeque::new(),
            trending: TrendingPages::new(8, 1.01f64),
        }
    }
    pub fn insert(&mut self, secs: Secs, topk: HashMap<String, f64>) {
        self.history.push_front((secs, topk));
        while self.history.len() > HISTORY_LENGTH {
            self.history.pop_back();
        }
    }

    pub fn reset(&mut self) {
        self.interval = None;
        self.history = VecDeque::new();
    }
}

pub struct DdBackend {
    url: String,
    pub schedule: Duration,
    pub wiki: Wiki,
    pub topk_wiki: Rc<RefCell<DDState>>,
}

impl DdBackend {
    pub fn new(url: &str, wiki: Wiki, schedule: Duration) -> Self {
        Self {
            url: url.to_owned(),
            wiki,
            schedule,
            topk_wiki: Rc::new(RefCell::new(DDState::new())),
        }
    }

    pub fn topK_wiki(&mut self, ctx: &egui::Context) -> Interval {
        let ctx = ctx.clone();
        let rc_dd_state = self.topk_wiki.clone();
        let millis = self.schedule.as_millis() as u32;
        let secs: Secs = self.schedule.as_secs() as i32;
        let url = format!(
            "{}wiki?project={}&family={}",
            self.url, self.wiki.project, self.wiki.family
        );
        // let url = format!("{}/top?project=en&family=wikipedia",&self.url);
        // let url = &url[..];
        // let url = "http://localhost:3000/top?project=en&family=wikipedia";

        Interval::new(millis, move || {
            let mut dd_state = rc_dd_state.borrow_mut();
            let topk: Option<HashMap<String, f64>> = match &dd_state.promise {
                Some(promise) => {
                    match promise.ready() {
                        Some(res) => match res {
                            Ok(topk) => {
                                let topk: HashMap<String, f64> = topk
                                    .topk
                                    .clone()
                                    .into_iter()
                                    .map(|(p, v)| (p.title, v))
                                    .collect();
                                Some(topk)
                            }
                            Err(e) => {
                                tracing::warn!("ddrt backend error: {}", e);
                                None
                            }
                        },
                        None => {
                            // not ready not good
                            tracing::warn!("ddrt backend not ready");
                            None
                        }
                    }
                }
                None => {
                    // first time
                    None
                }
            };
            // insert new topk in history
            dd_state.seconds += secs;
            if let Some(topk) = topk {
                let current = dd_state.seconds;
                dd_state.trending.insert(&topk);
                dd_state.insert(current, topk);
                ctx.request_repaint();
            }

            let url = url.clone();
            dd_state.promise = None;
            dd_state.promise.get_or_insert_with(move || {
                let (sender, promise) = Promise::new();
                let request = ehttp::Request::get(url);
                ehttp::fetch(request, move |response| {
                    let topk = response.and_then(|res| {
                        let json = res.text().ok_or("no response text")?;
                        serde_json::from_str::<TopK<Page>>(json)
                            .map_err(|err| format!("json error: {:?}", err))
                    });
                    sender.send(topk);
                });
                promise
            });
        })
    }
}
