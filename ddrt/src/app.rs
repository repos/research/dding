use std::{collections::HashSet, time::Duration};

use domain::api::Wiki;
use egui::plot::{Line, Plot, PlotPoints};

use crate::dd::DdBackend;

#[derive(PartialEq)]
enum PlotMode {
    Only,
    Hidden,
    Trending,
}

struct PlottedPages {
    pages: HashSet<String>,
    mode: PlotMode,
}

impl PlottedPages {
    pub fn only() -> Self {
        PlottedPages {
            pages: HashSet::new(),
            mode: PlotMode::Only,
        }
    }
    pub fn hidden() -> Self {
        PlottedPages {
            pages: HashSet::new(),
            mode: PlotMode::Hidden,
        }
    }

    pub fn trending() -> Self {
        PlottedPages {
            pages: HashSet::new(),
            mode: PlotMode::Trending,
        }
    }

    pub fn is_trending(&self) -> bool {
        self.mode == PlotMode::Trending
    }

    pub fn add(&mut self, page: &str) {
        self.pages.insert(page.to_string());
    }

    fn filter(&self, page: &str) -> bool {
        match self.mode {
            PlotMode::Only => self.pages.contains(page),
            PlotMode::Hidden => !self.pages.contains(page),
            PlotMode::Trending => self.pages.contains(page),
        }
    }
}

#[derive(serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct DdrtApp {
    // skipping for data privacy reasons
    #[serde(skip)]
    plotted: Option<PlottedPages>,
    #[serde(skip)]
    pages: HashSet<String>,
    #[serde(skip)]
    backend: DdBackend,
}

impl Default for DdrtApp {
    fn default() -> Self {
        Self {
            plotted: None,
            pages: HashSet::new(),
            backend: DdBackend::new(
                "", // "http://localhost:3000/",
                Wiki {
                    project: "en".to_owned(),
                    family: "wikipedia".to_owned(),
                },
                Duration::from_secs(5),
            ),
        }
    }
}

impl DdrtApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        if let Some(storage) = cc.storage {
            return eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default();
        }

        Default::default()
    }
}

impl eframe::App for DdrtApp {
    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }

    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let dd_state = self.backend.topk_wiki.clone();
        let mut dd_state = dd_state.borrow_mut();
        if dd_state.interval.is_none() {
            let interval = self.backend.topK_wiki(ctx);
            dd_state.interval = Some(interval);
        };

        #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        _frame.close();
                    }
                });
            });
        });

        egui::SidePanel::left("side_panel").show(ctx, |ui| {
            ui.heading("Top pages");
            ui.horizontal(|ui| {
                ui.label("Language: ");

                let y = ui.available_size()[1];
                let project_changed = ui
                    .add_sized(
                        [100f32, y],
                        egui::TextEdit::singleline(&mut self.backend.wiki.project),
                    )
                    .lost_focus();
                ui.label("Family: ");
                let family_changed = ui
                    .text_edit_singleline(&mut self.backend.wiki.family)
                    .lost_focus();

                let enter = ui.input(|i| i.key_pressed(egui::Key::Enter));

                if (project_changed || family_changed) && enter {
                    dd_state.reset();
                    self.pages.clear();
                    self.plotted = None;
                    ctx.request_repaint();
                }
            });

            if let Some((_, topk)) = dd_state.history.front() {
                let topk = topk.clone();
                let mut topk: Vec<(&String, &f64)> = topk.iter().collect::<Vec<(&String, &f64)>>();
                topk.sort_by(|a, b| a.1.total_cmp(b.1).reverse());
                let topk: Vec<(usize, (&String, &f64))> = topk.into_iter().enumerate().collect();

                for (_, (page, _)) in &topk {
                    if !self.pages.contains(*page) {
                        let new_page = page.clone();
                        self.pages.insert(new_page.to_owned());
                    }
                }

                ui.horizontal(|ui| {
                    if ui.add(egui::Button::new("reset")).clicked() {
                        self.plotted = None;
                    }

                    let trending_button = egui::Button::new("trending");
                    let trending_button = if self
                        .plotted
                        .as_ref()
                        .filter(|pp| pp.is_trending())
                        .is_some()
                    {
                        trending_button.fill(egui::Color32::DARK_GREEN)
                    } else {
                        trending_button
                    };

                    if ui.add(trending_button).clicked() {
                        if let Some(pp) = &mut self.plotted {
                            if pp.is_trending() {
                                self.plotted = None;
                            } else {
                                self.plotted = Some(PlottedPages::trending());
                            }
                        } else {
                            self.plotted = Some(PlottedPages::trending());
                        };
                        let candidates: Vec<(&String, &(i32, f64))> = dd_state
                            .trending
                            .candidates
                            .iter()
                            .filter(|(_, (c, _))| c > &0i32)
                            .collect();
                        tracing::warn!("trending candidates {:?}", candidates);
                        tracing::warn!("trending pages {:?}", dd_state.trending.trending);
                    };
                });

                let topk = if self
                    .plotted
                    .as_ref()
                    .filter(|pp| pp.is_trending())
                    .is_some()
                {
                    topk.into_iter()
                        .filter(|(_, (page, _))| dd_state.trending.is_trending(page))
                        .collect()
                } else {
                    topk
                };

                // update trending pages
                if let Some(pp) = &mut self.plotted {
                    if pp.is_trending() && dd_state.trending.trending != pp.pages {
                        pp.pages = dd_state.trending.trending.clone();
                    }
                }

                let text_style = egui::TextStyle::Body;
                let row_height = ui.text_style_height(&text_style);
                // let row_height = ui.spacing().interact_size.y; // if you are adding buttons instead of labels.
                let total_rows = topk.len();
                egui::ScrollArea::vertical()
                    .auto_shrink([false; 2])
                    .show_rows(ui, row_height, total_rows, |ui, row_range| {
                        for row in row_range {
                            let (index, (page, val)) = topk[row];

                            ui.horizontal(|ui| {
                                ui.label(format!("{}: ", index));
                                ui.hyperlink_to(
                                    page,
                                    format!(
                                        "https://{}.{}.org/wiki/{}",
                                        self.backend.wiki.project, self.backend.wiki.family, page
                                    ),
                                );
                                ui.label(format!(" ({:.0}) ", val));

                                if dd_state.trending.trending.contains(page) {
                                    ui.label(
                                        egui::RichText::new("trending")
                                            .background_color(egui::Color32::DARK_GREEN),
                                    );
                                }

                                match &mut self.plotted {
                                    Some(pp) => {
                                        match pp.mode {
                                            PlotMode::Hidden => {
                                                if pp.pages.contains(page) {
                                                    if ui
                                                        .add(
                                                            egui::Button::new("show")
                                                                .fill(egui::Color32::DARK_BLUE),
                                                        )
                                                        .clicked()
                                                    {
                                                        pp.pages.remove(page);
                                                    }
                                                } else {
                                                    if ui.add(egui::Button::new("hide")).clicked() {
                                                        pp.add(page);
                                                    }
                                                    if ui.add(egui::Button::new("only")).clicked() {
                                                        *pp = PlottedPages::only();
                                                        pp.add(page);
                                                    }
                                                }
                                            }
                                            PlotMode::Only => {
                                                if !pp.pages.contains(page) {
                                                    if ui.add(egui::Button::new("show")).clicked() {
                                                        pp.add(page);
                                                    }
                                                } else if ui
                                                    .add(
                                                        egui::Button::new("hide")
                                                            .fill(egui::Color32::DARK_RED),
                                                    )
                                                    .clicked()
                                                {
                                                    pp.pages.remove(page);
                                                }
                                            }
                                            PlotMode::Trending => {
                                                //
                                            }
                                        }
                                    }
                                    None => {
                                        if ui.add(egui::Button::new("hide")).clicked() {
                                            let mut pp = PlottedPages::hidden();
                                            pp.add(page);
                                            self.plotted = Some(pp);
                                        }
                                        if ui.add(egui::Button::new("only")).clicked() {
                                            let mut pp = PlottedPages::only();
                                            pp.add(page);
                                            self.plotted = Some(pp);
                                        }
                                    }
                                }
                            });
                        }
                    });
            } else {
                ui.label("No data");
            };
        });

        egui::CentralPanel::default().show(ctx, |ui| {
            ui.vertical(|ui| {

                let text = format!("Exponentially decaying counts for the top pageviews for the {} language {} project, polled at a {:?} interval.", self.backend.wiki.project, self.backend.wiki.family, self.backend.schedule);
                ui.label(text);
                ui.label("Plot controls: mouse primary to drag, mouse secondary to draw a rectangle, double click to reset");

            });


            let lines: Vec<Line> = self
                .pages
                .iter()
                .filter(|page| match &self.plotted {
                    Some(pp) => pp.filter(page),
                    None => true,
                })
                .map(|page| {
                    let current = dd_state.seconds;
                    let points: PlotPoints = dd_state
                        .history
                        .iter()
                        .flat_map(|(secs, topk)| topk.get(page).map(|val| {
                            let offset = *secs - current;
                            [offset as f64, *val]
                        }))
                        .collect();
                    Line::new(points).name(page)
                })
                .collect();

            let plot = Plot::new("top_pages").view_aspect(2.0);
            let plot = if lines.len() <= 10 {
                plot.legend(egui::plot::Legend::default())
            } else {
                plot
            };

            plot.show(ui, |plot_ui| {
                for line in lines {
                    plot_ui.line(line)
                }
            });

            egui::warn_if_debug_build(ui);
        });
    }
}
