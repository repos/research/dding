//! Data structures that are used to across
//! system boundaries.
//!  - used in the streaming pipelines
//!  - used to serialize in embedded databases
//!  - used in the api

use serde::{Deserialize, Serialize};
use std::fmt::Debug;

/// A domain representation of a decaying value.
/// Note that there is DecayedValue in the dding
/// crate that uses a const generic generic to
/// represent the halflife.
/// In this domain represenation, the halflife is a
/// normal field.
/// Do not use this struct to manipulate
/// decaying counts, use the [dding::decay::DecayedValue]
/// instead.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DecayedValue {
    pub value: f64,
    pub scaled_time: f64,
    pub half_life: f64,
}

impl DecayedValue {
    /// Compute the value as of `time`
    pub fn value_as_of(&self, time: f64) -> f64 {
        let scaled_as_of_time = time * 2f64.ln() / self.half_life;
        (self.scaled_time - scaled_as_of_time).exp() * self.value
    }
    pub fn update_as_of(&mut self, time: f64) {
        let scaled_as_of_time = time * 2f64.ln() / self.half_life;
        self.value = (self.scaled_time - scaled_as_of_time).exp() * self.value;
        self.scaled_time = scaled_as_of_time;
    }
}

/// Domain representation of [dding::decay::DecayedTopK] for serialization
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DecayedTopK<T> {
    pub topk: Vec<(T, DecayedValue)>,
}
