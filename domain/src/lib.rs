//! Domain data structures are pure data, there
//! are no other dependency besides serde.

pub mod api;
pub mod counting;
