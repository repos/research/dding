use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Wiki {
    pub project: String,
    pub family: String,
}

// A unique identifier for a wikipedia page
#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Page {
    pub title: String,
    pub project: String,
    pub family: String,
}
impl Page {
    pub fn wiki(&self) -> Wiki {
        Wiki {
            project: self.project.clone(),
            family: self.family.clone(),
        }
    }
    pub fn base_url(&self) -> BaseUrl {
        BaseUrl {
            host: format!("{}.{}.org", self.project, self.family),
            path: format!("/wiki/{}", self.title),
        }
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Url {
    pub host: String,
    pub path: String,
    pub query: String,
}

impl Url {
    pub fn base(&self) -> BaseUrl {
        BaseUrl {
            host: self.host.clone(),
            path: self.path.clone(),
        }
    }
}
#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct BaseUrl {
    pub host: String,
    pub path: String,
}
impl BaseUrl {
    pub fn url_string(&self) -> String {
        format!("{}{}", self.host, self.path)
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Fingerprint {
    pub ip: String,
    pub user_agent: String,
    pub accept_language: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Geography {
    pub country: String,
    pub wmf_region: String,
    pub isp: String,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct UserAgent(pub String);

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Country(pub String);

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Isp(pub String);

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Ip(pub String);

#[derive(Debug, Deserialize, Serialize)]
pub struct TopK<T> {
    pub topk: Vec<(T, f64)>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Pageviews {
    pub page: Page,
    pub count: f64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PageviewsSeries {
    pub page: Page,
    pub series: Vec<(u64, f64)>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Trend<T> {
    pub element: T,
    pub start: time::OffsetDateTime,
    pub end: Option<time::OffsetDateTime>,
    pub pre_detection: Vec<(u64, f64)>,
    pub post_detection: Vec<(u64, f64)>,
}
