//! Timely dataflow to compute exponentially decaying counts of pageviews
//! Consumes the webrequest_text kafka event streams
//! Stores output data in key/value database
//! Starts a http server to serve data
//! See README for deploy instructions
use anyhow::Result;
use clap::{Args, Parser as ClapParser};

use dding::cumulative::Decayed;
use dding::operators::{processing_datetime, MaxByKey, TopK, WorkerSelection, WriteToRedis};
use dding::pageview::ExtractPageview;
use dding::trending::{TrendDetection, TrendingConfig};
use dding::wmf_kafka::KafkaStream;
use domain::api::Country;
use std::sync::Arc;
use std::time::{Duration, Instant};
use time::OffsetDateTime;
use timely::dataflow::{channels::pact::Pipeline, operators::*};
use timely::worker::ProgressMode;
use timely::{CommunicationConfig, Config, WorkerConfig};

#[derive(Args, Debug)]
struct TopKArgs {
    #[arg(long, default_value_t = 100)]
    pages_per_wiki: usize,
    #[arg(long, default_value_t = 20)]
    fingerprints_per_page: usize,
    #[arg(long, default_value_t = 100)]
    pages_per_country: usize,
}

fn parse_duration(secs: &str) -> Result<Duration> {
    Ok(secs.parse().map(Duration::from_secs)?)
}

#[derive(Args, Debug)]
struct EmitIntervals {
    #[clap(value_parser = parse_duration, default_value="1")]
    decayed_pageviews: Duration,
    #[clap(value_parser = parse_duration, default_value="5")]
    decayed_pageviews1: Duration,
    #[clap(value_parser = parse_duration, default_value="60")]
    pageviews_timeseries: Duration,
    #[clap(value_parser = parse_duration, default_value="10")]
    top_wikis: Duration,
    #[clap(value_parser = parse_duration, default_value="10")]
    top_countries: Duration,
}

#[derive(ClapParser, Debug)]
#[command(version, about, long_about = None)]
struct TopPagesArgs {
    /// Number of threads to run timely dataflow wokers
    #[arg(short, long)]
    workers: usize,

    /// Kafka consumer group to use
    #[arg(short, long)]
    group_id: String,

    /// List of redis nodes host:port
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    redis_nodes: Vec<String>,

    /// Config of top entries to keep
    #[command(flatten)]
    top_k: TopKArgs,

    /// Emit intervals (seconds) for dataflow operators
    #[command(flatten)]
    emit_intervals: EmitIntervals,

    /// Port for api
    #[arg(short, long, default_value_t = 3000u16)]
    port: u16,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = TopPagesArgs::parse();

    tracing_subscriber::fmt::init();

    let tokio_executor = tokio::runtime::Handle::current();

    // the half-life of the exponential decay
    const HALF_LIFE: usize = dding::decay::HALF_LIFE_30_MIN;

    // the starting timestamp for the processing time clock
    // used by timely dataflow to determine a nodes capability to produce data
    // for a given timestamp
    let time_zero = Instant::now();

    // the UTC datetime at the starting time used for event time
    // e.g. webrequests have a one second granularity
    let event_time_zero = OffsetDateTime::now_utc();

    let redis_nodes: Vec<String> = args
        .redis_nodes
        .iter()
        .map(|h| format!("redis://{h}"))
        .collect();
    let client = redis::cluster::ClusterClient::new(redis_nodes)?;

    // updating event time state
    dding::db::write_time_config(event_time_zero, &client)?;

    let db_outer = Arc::new(client);
    // TODO, with an external storage - there is no need for the data api to be embedded
    // This could/should be it's own binary, e.g. it could serve data from multiple dataflow programs
    // spawn web server to serve data from the kv db
    let db_async = db_outer.clone();
    tokio::spawn(dding::api::start(
        db_async,
        time_zero,
        event_time_zero,
        args.port,
    ));

    let timely_config = Config {
        // TODO investigate using TIMELY_COMM_LOG_ADD to collect distributed logs
        // TODO ivestigate using CommunicationConfig::ProcessBinary
        communication: CommunicationConfig::ProcessBinary(args.workers),
        // TODO investigate eager vs demand mode. Demand seems to delay writing to the db
        worker: WorkerConfig::default().progress_mode(ProgressMode::Eager),
    };
    timely::execute(timely_config, move |worker| {
        let _tokio_guard = tokio_executor.enter();

        let group_id = &args.group_id;
        let kafka_webrequest = KafkaStream::new(
            "webrequest_text",
            group_id,
            time_zero,
            Duration::from_millis(200),
        );

        // split up the worker into compute & io workers
        let worker_selection = WorkerSelection::new(worker, 0);

        let db_inner = db_outer.clone();

        // a map of page collect
        // let pages_to_observe: RwLock<HashSet<Page>>

        worker.dataflow::<u64, _, _>(move |scope| {
            // webrequests kafka stream, only consumed on worker processes
            let reqs = if worker_selection.is_compute() {
                kafka_webrequest.to_stream(scope)
            } else {
                vec![].to_stream(scope)
            };

            // extract features from webrequest relevant for counting
            let page_views = reqs.extract_pageview(event_time_zero);

            // let decayed_pageviews = page_views.decayed::<{ HALF_LIFE }>(
            let decayed_pageviews = page_views.decayed::<{ HALF_LIFE }>(
                args.emit_intervals.decayed_pageviews,
                worker_selection,
                Some(Duration::from_secs(60)),
            );

            let decayed_pageviews_windowed = decayed_pageviews.max_by_key(
                args.emit_intervals.decayed_pageviews1,
                |cum| cum.features.page.to_owned(),
                |cum| cum.key,
                worker_selection,
            );

            decayed_pageviews_windowed
                .map(|cum| (cum.features.page.to_owned(), cum.key))
                .write_to_redis("pv:dv".to_string(), db_inner.clone(), worker_selection)
                .expect("pageviews decayed error");

            decayed_pageviews_windowed
                .max_by_key(
                    args.emit_intervals.pageviews_timeseries,
                    |cum| cum.features.page.to_owned(),
                    |cum| cum.key,
                    worker_selection,
                )
                .unary_frontier(Pipeline, "pageview timeseries", |_default_cap, _info| {
                    // vec used to drain input batch
                    let mut batch_vec = Vec::new();
                    move |input, output| {
                        while let Some((time, data)) = input.next() {
                            //calculate the unix timestap of the batch
                            let evaluate_timestamp = *time.time();
                            let unix_timestamp =
                                processing_datetime(evaluate_timestamp, &event_time_zero)
                                    .unix_timestamp() as u64;

                            // consume new batch of features
                            data.swap(&mut batch_vec);
                            for cum in batch_vec.drain(..) {
                                let page = cum.features.page.to_owned();
                                let value = cum.key.value_as_of(evaluate_timestamp as f64);
                                output.session(&time).give((page, (unix_timestamp, value)));
                            }
                        }
                    }
                })
                .write_to_redis("pv:series".to_string(), db_inner.clone(), worker_selection)
                .expect("pageviews timeseries error");

            // top pages per wiki
            let top_wikis = decayed_pageviews.top_k_nested(
                args.top_k.pages_per_wiki,
                args.top_k.fingerprints_per_page,
                |kc| kc.features.page.wiki().to_owned(),
                |kc| kc.features.page.to_owned(),
                |kc| {
                    if kc.features.page.family == "wikipedia" {
                        Some(kc.features.fingerprint.to_owned())
                    } else {
                        None
                    }
                },
                |cum| cum.key,
                |cum| cum.key_fingerprint,
                args.emit_intervals.top_wikis,
                worker_selection,
            );

            // write top pages per wiki
            top_wikis
                .map(|(wiki, (top_k1, _))| (wiki, top_k1))
                .write_to_redis("page:topk".to_string(), db_inner.clone(), worker_selection)
                .expect("top_countries error");

            // write top fingerprint by page
            top_wikis
                .flat_map(|(_, (_, top_k2s))| {
                    top_k2s.into_iter().map(|(page, top_k2)| (page, top_k2))
                })
                .write_to_redis(
                    "fingerprint:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("top_countries error");

            // top pages per country
            decayed_pageviews
                .top_k(
                    args.top_k.pages_per_country,
                    |kc| Country(kc.features.geography.country.to_owned()),
                    |kc| kc.features.page.to_owned(),
                    |kc| kc.key_country,
                    args.emit_intervals.top_countries,
                    worker_selection,
                )
                .write_to_redis("page:topk".to_string(), db_inner.clone(), worker_selection)
                .expect("top_countries error");

            let trending_pages = decayed_pageviews
                .map(|cum| (cum.features.page, cum.key))
                .trend_detection(
                    TrendingConfig::default(),
                    event_time_zero,
                    args.emit_intervals.decayed_pageviews,
                    // Duration::ZERO,
                    Duration::from_millis(HALF_LIFE as u64),
                    worker_selection,
                );
            trending_pages
                .write_to_redis("pages".to_string(), db_inner.clone(), worker_selection)
                .expect("trending pages error");
        });

        loop {
            worker.step_or_park(Some(Duration::from_millis(100)));
        }
    })
    .expect("Timely Dataflow computation failed.");

    Ok(())
}
