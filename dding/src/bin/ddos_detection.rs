use anyhow::Result;
use clap::{Args, Parser as ClapParser};
use dding::{
    cache_status::ExtractCacheInfo,
    cumulative::{Decayed, KeyedFields as _},
    operators::{TopK, WorkerSelection, WriteToRedis},
    trending::{TrendDetection as _, TrendingConfig},
    wmf_kafka::KafkaStream,
};
use domain::api::{BaseUrl, Country, Isp};
use std::{
    sync::Arc,
    time::{Duration, Instant},
};
use time::OffsetDateTime;
use timely::{
    dataflow::operators::{Broadcast, Filter, Map, Operator, ToStream},
    worker::ProgressMode,
    CommunicationConfig, Config, WorkerConfig,
};
use tracing_subscriber::filter::EnvFilter;
use tracing_subscriber::fmt::format::FmtSpan;

#[derive(Args, Debug)]
struct TopKArgs {
    #[arg(long, default_value_t = 20)]
    top_global: usize,
    #[arg(long, default_value_t = 20)]
    fingerprint_per_global: usize,
    #[arg(long, default_value_t = 20)]
    uri_per_global: usize,
    #[arg(long, default_value_t = 10)]
    uri_per_fingerprint: usize,
}

fn parse_duration(secs: &str) -> Result<Duration> {
    Ok(secs.parse().map(Duration::from_secs)?)
}

#[derive(Args, Debug)]
struct EmitIntervals {
    #[clap(value_parser = parse_duration, default_value="1")]
    decayed_cache_misses: Duration,
    #[clap(value_parser = parse_duration, default_value="5")]
    global_country: Duration,
    #[clap(value_parser = parse_duration, default_value="5")]
    global_isp: Duration,
    #[clap(value_parser = parse_duration, default_value="30")]
    global_ip: Duration,
    #[clap(value_parser = parse_duration, default_value="10")]
    fingerprint: Duration,
    #[clap(value_parser = parse_duration, default_value="30")]
    uri: Duration,
}

#[derive(ClapParser, Debug)]
#[command(version, about, long_about = None)]
struct DdosArgs {
    /// Number of threads to run timely dataflow wokers
    #[arg(short, long)]
    workers: usize,

    /// Kafka consumer group to use
    #[arg(short, long)]
    group_id: String,

    /// List of redis nodes host:port
    #[arg(short, long, value_parser, num_args = 1.., value_delimiter = ' ')]
    redis_nodes: Vec<String>,

    /// Config of top entries to keep
    #[command(flatten)]
    top_k: TopKArgs,

    /// Emit intervals (seconds) for dataflow operators
    #[command(flatten)]
    emit_intervals: EmitIntervals,

    /// Port for api
    #[arg(short, long, default_value_t = 3000u16)]
    port: u16,
}

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt()
        .with_env_filter(EnvFilter::from_default_env())
        .with_span_events(FmtSpan::CLOSE)
        .init();

    let args = DdosArgs::parse();
    let tokio_executor = tokio::runtime::Handle::current();

    // the half-life of the exponential decay
    const HALF_LIFE: usize = dding::decay::HALF_LIFE_10_MIN;

    // the starting timestamp for the processing time clock
    // used by timely dataflow to determine a nodes capability to produce data
    // for a given timestamp
    let time_zero = Instant::now();

    // the UTC datetime at the starting time used for event time
    // e.g. webrequests have a one second granularity
    let event_time_zero = OffsetDateTime::now_utc().replace_nanosecond(0)?;

    let redis_nodes: Vec<String> = args
        .redis_nodes
        .iter()
        .map(|h| format!("redis://{h}"))
        .collect();
    let client = redis::cluster::ClusterClient::new(redis_nodes)?;
    dding::db::write_time_config(event_time_zero, &client)?;
    let db_outer = Arc::new(client);

    let timely_config = Config {
        // TODO investigate using TIMELY_COMM_LOG_ADD to collect distributed logs
        // TODO ivestigate using CommunicationConfig::ProcessBinary
        communication: CommunicationConfig::ProcessBinary(args.workers),
        // TODO investigate eager vs demand mode. Demand seems to delay writing to the db
        worker: WorkerConfig::default().progress_mode(ProgressMode::Eager),
    };

    timely::execute(timely_config, move |worker| {
        let worker_index = worker.index() as u64;
        let worker_selection = WorkerSelection::new(worker, 0);

        let _tokio_guard = tokio_executor.enter();

        let db_inner = db_outer.clone();

        let group_id: &String = &args.group_id;
        let kafka_webrequest = KafkaStream::new(
            "webrequest_text",
            group_id,
            time_zero,
            Duration::from_millis(200),
        );

        worker.dataflow(move |scope| {
            // webrequests kafka stream
            let webrequests = if worker_selection.is_compute() {
                kafka_webrequest.to_stream(scope)
            } else {
                vec![].to_stream(scope)
            };

            // extract cache misses
            let cache_misses = webrequests
                .filter(move |req: &dding::webrequest::WebRequest| {
                    let not_cached = req.is_cache_miss_or_pass();
                    // let not_cached = req.cache_status == "miss";
                    let valid_timestamp = req
                        .datetime()
                        .ok()
                        .map(|dt| dt >= (event_time_zero - Duration::from_secs(60)))
                        .unwrap_or(false);
                    if !valid_timestamp {
                        tracing::debug!(
                            "invalid timestamp: {} < {}",
                            req.datetime()
                                .map(|dt| dt.to_string())
                                .unwrap_or("no datetime".to_string()),
                            event_time_zero - Duration::from_secs(60)
                        )
                    }

                    let filtered_hosts = vec![
                        "intake-analytics.wikimedia.org",
                        "intake-logging.wikimedia.org",
                    ];
                    let filtered_path = vec![
                        "/w/api.php",
                        "/w/index.php",
                        "/api/rest_v1/media/math/check/tex",
                        "/api/rest_v1/page/random/summary",
                        "/wiki/Special:Random",
                        "/w//api.php",
                        // "/wiki/Special:Search",
                        // "/wiki/Special:Watchlist",
                    ];

                    not_cached
                        && valid_timestamp
                        && !filtered_hosts.contains(&req.uri_host.as_str())
                        && !filtered_path.contains(&req.uri_path.as_str())
                })
                .extract_cache_info(event_time_zero);

            let decayed_cache_misses = cache_misses.decayed::<{ HALF_LIFE }>(
                args.emit_intervals.decayed_cache_misses,
                worker_selection,
                Some(Duration::from_secs(60)),
            );

            // decayed_cache_misses
            //     .map(|cum| {
            //         let country = cum.features.geography.country;
            //         let host = cum.features.url.host;
            //         let path = cum.features.url.path;
            //         (
            //             format!("{host}:{path}:{country}"),
            //             cum.keyed_cumulative.key_country,
            //         )
            //     })
            //     .write_to_redis(
            //         "topk".to_string(),
            //         db_inner.clone(),
            //         worker_selection,
            //     )
            //     .expect("topK global country error");

            // decayed_cache_misses.inspect_batch(move |time, cis| {
            //     let min_time = cis.iter().map(|ci| ci.features.relative_event_time as u64).min().unwrap();
            //     let max_time = cis.iter().map(|ci| ci.features.relative_event_time as u64).max().unwrap();
            //     tracing::debug!(
            //         "@time {time}. worker {worker_index}. decayed cachemisses: {}. processing - event timestamp (min: {}, max: {}) ({min_time} / {max_time})",
            //         cis.len(),
            //         (*time- min_time),
            //         (*time- max_time),
            //     );
            // });
            let trending_config = TrendingConfig {
                min_value: 1000.0,
                min_growth_mean: 1.02,
                max_growth_std: 10.0,
                min_detection_count: 10,
                disregard_growth: 0.9,
                disregard_count: 30,
                trend_end_theshold: 0.9,
                trend_update_interval: Duration::from_secs(30),
                min_trend_duration: Duration::from_secs(60 * 5),
                max_trend_duration: Duration::from_secs(1 * 3600),
            };
            // trending countries
            decayed_cache_misses
                // .map(|cum| (cum.features.base_url(), cum.base_url))
                .map(|cum| (cum.features.geography.country, cum.country))
                .trend_detection(
                    trending_config,
                    event_time_zero,
                    args.emit_intervals.decayed_cache_misses,
                    Duration::from_millis(HALF_LIFE as u64),
                    // Duration::ZERO,
                    worker_selection,
                )
                .write_to_redis(
                    "cache_misses:country".to_string(),
                    // "cache_misses:base_url".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("trending country error");

            // trending base url
            decayed_cache_misses
                .map(|cum| (cum.features.base_url(), cum.base_url.to_owned()))
                .trend_detection(
                    trending_config,
                    event_time_zero,
                    args.emit_intervals.decayed_cache_misses,
                    Duration::from_millis(HALF_LIFE as u64),
                    // Duration::ZERO,
                    worker_selection,
                )
                .write_to_redis(
                    "cache_misses:host_path".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("trending host_path error");

            // global top countries
            decayed_cache_misses
                .top_k(
                    args.top_k.top_global,
                    |_| "global:country".to_string(), // exchange to same worker
                    |cum| Country(cum.features.geography.country.clone()),
                    |cum| cum.country,
                    args.emit_intervals.global_country,
                    worker_selection,
                )
                .write_to_redis("topk".to_string(), db_inner.clone(), worker_selection)
                .expect("topK global country error");

            // country - top fingerprints
            let country_fingerprint_uri = decayed_cache_misses.top_k_nested(
                args.top_k.fingerprint_per_global,
                args.top_k.uri_per_fingerprint,
                |cum| Country(cum.features.geography.country.clone()),
                |cum| cum.features.fingerprint.clone(),
                |cum| Some(cum.features.url.clone()),
                |cum| cum.fingerprint,
                |cum| cum.key_fingerprint,
                args.emit_intervals.fingerprint,
                worker_selection,
            );
            // write top fingerprints per country
            country_fingerprint_uri
                .map(|(country, (top_k1, _))| (country, top_k1))
                .write_to_redis(
                    "fingerprint:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK fingerprints per country error");

            // country: write top uri path per fingerprint
            country_fingerprint_uri
                .flat_map(|(_, (_, top_k2s))| {
                    top_k2s
                        .into_iter()
                        .map(|(fingerprint, top_k2)| (fingerprint, top_k2))
                })
                .write_to_redis(
                    "country:uri:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK uri per fingerprints error");

            // country - top uri
            decayed_cache_misses
                .top_k(
                    args.top_k.uri_per_global,
                    |cm| Country(cm.features.geography.country.clone()),
                    |cm| cm.features.url.clone(),
                    |cum| cum.key_country,
                    args.emit_intervals.uri,
                    worker_selection,
                )
                .write_to_redis("uri:topk".to_string(), db_inner.clone(), worker_selection)
                .expect("topK uri per country error");

            // global top isp
            decayed_cache_misses
                .top_k(
                    args.top_k.top_global,
                    |_| "global:isp".to_string(),
                    |cum| Isp(cum.features.geography.isp.clone()),
                    |cum| cum.isp,
                    args.emit_intervals.global_isp,
                    worker_selection,
                )
                .write_to_redis("topk".to_string(), db_inner.clone(), worker_selection)
                .expect("topK isp global error");

            // isp - top fingerprints
            let isp_fingerprint_uri = decayed_cache_misses.top_k_nested(
                args.top_k.fingerprint_per_global,
                args.top_k.uri_per_fingerprint,
                |cum| Isp(cum.features.geography.isp.clone()),
                |cum| cum.features.fingerprint.clone(),
                |cum| Some(cum.features.url.clone()),
                |cum| cum.fingerprint,
                |cum| cum.key_fingerprint,
                args.emit_intervals.fingerprint,
                worker_selection,
            );

            // write top fingerprints per isp
            isp_fingerprint_uri
                .map(|(isp, (top_k1, _))| (isp, top_k1))
                .write_to_redis(
                    "fingerprint:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK fingerprints per isp error");

            // isp - write top uri path per fingerprint
            isp_fingerprint_uri
                .flat_map(|(_, (_, top_k2s))| {
                    top_k2s
                        .into_iter()
                        .map(|(fingerprint, top_k2)| (fingerprint, top_k2))
                })
                .write_to_redis(
                    "isp:uri:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK uri per fingerprints error");

            // isp - top uri
            decayed_cache_misses
                .top_k(
                    args.top_k.uri_per_global,
                    |cum| Isp(cum.features.geography.isp.clone()),
                    |cum| cum.features.url.clone(),
                    |cum| cum.key_isp,
                    args.emit_intervals.uri,
                    worker_selection,
                )
                .write_to_redis("uri:topk".to_string(), db_inner.clone(), worker_selection)
                .expect("topK uri per isp error");

            // global top host/path
            let global_base_url = decayed_cache_misses.top_k(
                args.top_k.top_global,
                |_cum| "global:host_path".to_string(),
                |cum| cum.features.url.base(),
                |cum| cum.base_url,
                args.emit_intervals.uri,
                worker_selection,
            );
            global_base_url
                .write_to_redis("topk".to_string(), db_inner.clone(), worker_selection)
                .expect("topK uri per host/path error");

            // filter the cumulative stream to only include the top k base urls
            // a nested topk on all possible base urls is memory intensive
            let global_cumulative = decayed_cache_misses.binary(
                &global_base_url
                    .map(|(_, topk)| topk.elements().cloned().collect::<Vec<BaseUrl>>())
                    .broadcast(),
                timely::dataflow::channels::pact::Pipeline,
                timely::dataflow::channels::pact::Pipeline,
                "filter global top",
                |_, _info| {
                    let mut cumulative = Vec::new();
                    let mut top_global = Vec::new();
                    move |input1, input2, output| {
                        while let Some((time, data)) = input1.next() {
                            data.swap(&mut cumulative);
                            cumulative.retain(|cum| top_global.contains(&cum.features.url.base()));
                            output.session(&time).give_container(&mut cumulative);
                        }
                        while let Some((_, data)) = input2.next() {
                            match data.last().take() {
                                Some(v) => {
                                    // replace the filtered base url
                                    top_global = v.to_vec();
                                }
                                None => (),
                            };
                        }
                    }
                },
            );

            // host_path - top fingerprints
            let host_path_fingerprint_uri = global_cumulative.top_k_nested(
                args.top_k.fingerprint_per_global,
                args.top_k.uri_per_fingerprint,
                |cum| cum.features.url.base(),
                |cum| cum.features.fingerprint.clone(),
                |cum| {
                    Some(cum.features.url.clone())
                    // only consider URI that have a query, since host/path is already the key
                    // if cum.features.url.query.is_empty() {
                    //     None
                    // } else {
                    //     Some(cum.features.url.clone())
                    // }
                },
                |cum| cum.fingerprint,
                |cum| cum.key_fingerprint,
                args.emit_intervals.fingerprint,
                worker_selection,
            );

            // write top fingerprints per host_path
            // use "host_path" namespace prefix
            host_path_fingerprint_uri
                .map(|(host_path, (top_k1, _))| (host_path, top_k1))
                .write_to_redis(
                    "host_path:fingerprint:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK fingerprints per host_path error");

            // host_path - write top uri path per fingerprint
            host_path_fingerprint_uri
                .flat_map(|(_, (_, top_k2s))| {
                    top_k2s
                        .into_iter()
                        .map(|(fingerprint, top_k2)| (fingerprint, top_k2))
                })
                .write_to_redis(
                    "host_path:fingerprint:uri:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK uri per fingerprints error");

            // host_path - top uri
            global_cumulative
                .top_k(
                    args.top_k.uri_per_global,
                    |cum| cum.features.url.base(),
                    |cum| cum.features.url.clone(),
                    |cum| cum.key,
                    args.emit_intervals.uri,
                    worker_selection,
                )
                .write_to_redis(
                    "host_path:uri:topk".to_string(),
                    db_inner.clone(),
                    worker_selection,
                )
                .expect("topK uri per host_path error");
        });

        loop {
            worker.step_or_park(Some(Duration::from_millis(100)));
        }
    })
    .expect("Timely Dataflow computation failed.");

    Ok(())
}
