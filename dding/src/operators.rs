use crate::db::IoData;
use crate::decay::DecayedTopK;
use crate::decay::DecayedValue;
use anyhow::Result;
use rand::Rng;
use redis::cluster::ClusterClient;
use std::collections::hash_map::Entry;
use std::fmt::Debug;
use std::sync::Arc;
use std::time::Duration;
use std::{
    collections::{hash_map::DefaultHasher, HashMap, HashSet},
    hash::{Hash, Hasher},
};
use timely::communication::allocator::Generic;
use timely::worker::Worker;
use timely::{
    dataflow::{
        channels::pact::{Exchange as ExchangePact, Pipeline},
        operators::{Capability, Exchange, FrontierNotificator, InputCapability, Operator},
        Scope, ScopeParent, Stream,
    },
    ExchangeData,
};

/// Some dataflow workers can be reserved for i/o operations.
/// Each dataflow worker is associated with a worker_index.
/// compute workers have index (0 .. compute_workers-1)
/// io workers have index (compute_workers .. compute_workers+io_workers-1)
#[derive(Debug, Clone, Copy)]
pub struct WorkerSelection {
    pub worker_index: usize,
    pub compute_workers: usize,
    pub io_workers: usize,
}

impl WorkerSelection {
    pub fn new(worker: &Worker<Generic>, io_workers: usize) -> Self {
        assert!(worker.peers() - io_workers > 0);
        WorkerSelection {
            worker_index: worker.index(),
            compute_workers: worker.peers() - io_workers,
            io_workers: io_workers,
        }
    }

    pub fn num_workers(&self) -> usize {
        self.compute_workers + self.io_workers
    }

    pub fn is_compute(&self) -> bool {
        self.worker_index < self.compute_workers
    }

    /// returns a u64 that can be used for a modulo operation on
    /// a hashed key (u64), in order to shuffle stream elements
    /// with the same hash to the same worker.
    pub fn compute_worker_modulo(&self) -> u64 {
        self.compute_workers as u64
    }

    /// a random i/o worker index, useful for to shuffle i/o data evenly
    pub fn random_io_worker_index(&self) -> u64 {
        let index = rand::thread_rng()
            .gen_range(self.compute_workers..(self.compute_workers + self.io_workers));
        index as u64
    }

    /// Return a capability that corresponds to the next batch for a given input capability.
    /// The capability is shifted depending on the index of the worker, so that the notifications
    /// are not happing at the same time for all workers, while using the same batch_size.
    /// This is useful if e.g. the output is used in an i/o operation such as writing to a database.
    pub fn shifted_batch(&self, time: &InputCapability<u64>, batch_size: u64) -> Capability<u64> {
        let this_batch_index = time.time() / batch_size;
        let next_batch = (this_batch_index + 1) * batch_size;
        let shifted = (batch_size / self.compute_workers as u64) * self.worker_index as u64;
        let next_batch = next_batch + shifted;
        time.delayed(&next_batch)
    }
}

/// Return the processing datetime
pub fn processing_datetime(
    evaluate_timestamp: u64,
    event_time_zero: &time::OffsetDateTime,
) -> time::OffsetDateTime {
    let since_time_zero = Duration::from_millis(evaluate_timestamp);
    *event_time_zero + since_time_zero
}

/// A dataflow operator to generate the top elements of
/// an already aggregated stream
/// grouping_fn: specify a grouping, e.g. country
/// key_fn: specify the key for the topk, e.g. page
/// value_fn: specify which count to use for topk, e.g. by_ip
/// The example generates: topK pages visited by country, measured by requests to distinct ip addresses
pub trait TopK<S: Scope, const HL: usize, T> {
    fn top_k<G: Eq + Hash + Clone + 'static, K: Debug + Eq + Hash + Clone + 'static>(
        &self,
        top_k_entries: usize,
        grouping_fn: impl FnMut(&T) -> G + 'static + Clone,
        key_fn: impl FnMut(&T) -> K + 'static + Clone,
        value_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, (G, DecayedTopK<K, HL>)>;

    /// Nested topK. TopK for K1 as top_k generates, and for each K1 in the topK the topK of K2 is generated
    /// Returns a tuple (G, (TopK1, Vec<(K1, TopK2)>))
    fn top_k_nested<
        G: Eq + Hash + Clone + 'static,
        K1: Debug + Eq + Hash + Clone + 'static,
        K2: Debug + Eq + Hash + Clone + 'static,
    >(
        &self,
        top_k1_entries: usize,
        top_k2_entries: usize,
        grouping_fn: impl FnMut(&T) -> G + 'static + Clone,
        key1_fn: impl FnMut(&T) -> K1 + 'static + Clone,
        key2_fn: impl FnMut(&T) -> Option<K2> + 'static + Clone,
        value1_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        value2_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, (G, (DecayedTopK<K1, HL>, Vec<(K1, DecayedTopK<K2, HL>)>))>;
}

impl<S: Scope, const HL: usize, T: ExchangeData> TopK<S, HL, T> for Stream<S, T>
where
    S: ScopeParent<Timestamp = u64>,
{
    fn top_k<G: Eq + Hash + Clone + 'static, K: Debug + Eq + Hash + Clone + 'static>(
        &self,
        top_k_entries: usize,
        mut grouping_fn: impl FnMut(&T) -> G + 'static + Clone,
        mut key_fn: impl FnMut(&T) -> K + 'static + Clone,
        mut value_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, (G, DecayedTopK<K, HL>)> {
        let span = tracing::span!(tracing::Level::DEBUG, "topk");
        let _enter = span.enter();

        let emit_iterval_ms = emit_iterval.as_millis() as u64;
        // route the same key to the same worker
        let mut exchange_fn = grouping_fn.clone();
        self.exchange(move |t| -> u64 {
            let mut hasher = DefaultHasher::new();
            exchange_fn(&t).hash(&mut hasher);
            hasher
                .finish()
                .rem_euclid(worker_selection.compute_worker_modulo())
        })
        .unary_frontier(Pipeline, "top_k", |_default_cap, _info| {
            // vec used to drain input batch
            let mut batch_vec = Vec::new();

            let mut by_group = HashMap::new();
            let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();
            let mut to_emit = HashMap::new();

            move |input, output| {
                while let Some((time, data)) = input.next() {
                    // compute batch and add notification for end of batch
                    let batch_cap = worker_selection.shifted_batch(&time, emit_iterval_ms);
                    let batch_time = *batch_cap.time();
                    notificator.notify_at(batch_cap);
                    // consume new batch
                    data.swap(&mut batch_vec);

                    for t in batch_vec.drain(..) {
                        let group = grouping_fn(&t);

                        let key = key_fn(&t);
                        let value = value_fn(&t);
                        let top_k = by_group
                            .entry(group.clone())
                            .or_insert(DecayedTopK::new(top_k_entries));
                        if let (true, _) = top_k.insert(key, value) {
                            let to_emit_at_t = to_emit.entry(batch_time).or_insert(HashSet::new());
                            to_emit_at_t.insert(group);
                        };
                    }
                }

                notificator.for_each(&[input.frontier()], |time, _notificator| {
                    if let Some(mut groups_to_emit) = to_emit.remove(time.time()) {
                        let mut out_vec = groups_to_emit
                            .drain()
                            .flat_map(|group| by_group.get_key_value(&group))
                            .map(|(group, topk)| (group.to_owned(), topk.clone()))
                            .collect();
                        output.session(&time).give_container(&mut out_vec);
                    };

                    // cleanup remove counts that have decayed to ~0
                    // let since_event_time_zero = req.datetime().ok()? - event_time_zero;
                    // let relative_event_time = since_event_time_zero.whole_milliseconds();
                    let as_of_time = *time.time() as f64;
                    let before = by_group.len();
                    by_group.retain(|group, top_k| {
                        top_k.sanitize(as_of_time);
                        let empty_top_k = top_k.is_empty();
                        // if empty_top_k {
                        //     tracing::error!("removing group!");
                        // }
                        !empty_top_k
                    });
                    let after = by_group.len();
                    tracing::debug!(
                        target: "topk",
                        "@{} size of topk by_group[{}]  hashmap (before {before}, after {after})",
                        time.time(),
                        std::any::type_name::<G>()
                    );
                    tracing::debug!(
                        target: "topk",
                        "@{} size of topk to_emit hashmap = {}",
                        time.time(),
                        to_emit.len()
                    );
                });
            }
        })
    }

    fn top_k_nested<
        G: Eq + Hash + Clone + 'static,
        K1: Debug + Eq + Hash + Clone + 'static,
        K2: Debug + Eq + Hash + Clone + 'static,
    >(
        &self,
        top_k1_entries: usize,
        top_k2_entries: usize,
        mut grouping_fn: impl FnMut(&T) -> G + 'static + Clone,
        mut key1_fn: impl FnMut(&T) -> K1 + 'static + Clone,
        mut key2_fn: impl FnMut(&T) -> Option<K2> + 'static + Clone,
        mut value1_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        mut value2_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, (G, (DecayedTopK<K1, HL>, Vec<(K1, DecayedTopK<K2, HL>)>))> {
        let span = tracing::span!(tracing::Level::DEBUG, "topk_nested");
        let _enter = span.enter();

        let emit_iterval_ms = emit_iterval.as_millis() as u64;
        // route the same key to the same worker
        let mut exchange_fn = grouping_fn.clone();
        let compute_workers = worker_selection.compute_worker_modulo();
        self.exchange(move |t| -> u64 {
            let mut hasher = DefaultHasher::new();
            exchange_fn(&t).hash(&mut hasher);
            hasher.finish().rem_euclid(compute_workers)
        })
        .unary_frontier(Pipeline, "top_k", |_default_cap, _info| {
            // vec used to drain input batch
            let mut batch_vec = Vec::new();

            let mut by_group: HashMap<G, DecayedTopK<K1, HL>> = HashMap::new();
            let mut by_key1: HashMap<K1, DecayedTopK<K2, HL>> = HashMap::new();
            let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();
            let mut to_emit: HashMap<u64, HashSet<G>> = HashMap::new();

            move |input, output| {
                while let Some((time, data)) = input.next() {
                    // compute batch and add notification for end of batch
                    let batch_cap = worker_selection.shifted_batch(&time, emit_iterval_ms);
                    let batch_time = *batch_cap.time();
                    notificator.notify_at(batch_cap);

                    // consume new batch
                    data.swap(&mut batch_vec);

                    for t in batch_vec.drain(..) {
                        let group = grouping_fn(&t);
                        let key1 = key1_fn(&t);
                        let value1 = value1_fn(&t);
                        let top_k1 = by_group
                            .entry(group.clone())
                            .or_insert(DecayedTopK::new(top_k1_entries));
                        let (updated, ejected) = top_k1.insert(key1.clone(), value1);
                        if updated {
                            let to_emit_at_t = to_emit.entry(batch_time).or_insert(HashSet::new());
                            to_emit_at_t.insert(group);
                        }

                        // if k1 is in topK, update topK for k2
                        if top_k1.contains(&key1) {
                            let top_k2 = by_key1
                                .entry(key1)
                                .or_insert(DecayedTopK::new(top_k2_entries));

                            if let Some(key2) = key2_fn(&t) {
                                let value2 = value2_fn(&t);
                                top_k2.insert(key2, value2);
                            };

                            // handle ejected K1 that is not in the topk1  anymore
                            if let Some((ejected_key, _)) = ejected {
                                // only remove the ejected K1 if it is also not present in any other group
                                if by_group
                                    .values()
                                    .into_iter()
                                    .all(|top_k| !top_k.contains(&ejected_key))
                                {
                                    by_key1.remove(&ejected_key);
                                }
                            }
                        }
                    }
                }

                notificator.for_each(&[input.frontier()], |time, _notificator| {
                    if let Some(mut groups_to_emit) = to_emit.remove(time.time()) {
                        let mut out_vec = groups_to_emit
                            .drain()
                            .flat_map(|group| by_group.get_key_value(&group))
                            .map(|(group, topk)| {
                                let top_k1 = topk.to_owned();
                                let top_k2_vec = top_k1
                                    .elements()
                                    .flat_map(|k1| {
                                        by_key1
                                            .get(k1)
                                            .map(|top_k2| (k1.to_owned(), top_k2.to_owned()))
                                    })
                                    .collect();
                                (group.to_owned(), (top_k1, top_k2_vec))
                            })
                            .collect();
                        output.session(&time).give_container(&mut out_vec);
                    };
                    let before = by_group.len();
                    let as_of_time = *time.time() as f64;
                    by_group.retain(|_, top_k| {
                        top_k.sanitize(as_of_time);
                        !top_k.is_empty()
                    });
                    let after = by_group.len();
                    tracing::debug!(
                        target: "topk",
                        "@{} size of topk nested by_group hashmap[{}] (before {before}, after {after})",
                        time.time(),
                        std::any::type_name::<G>()
                    );

                    let before = by_key1.len();
                    by_key1.retain(|k1, top_k| {
                        top_k.sanitize(as_of_time);
                        let empty_top_k = top_k.is_empty();
                        // only remove the K1 list if not present in any group
                        // let k1_unused = by_group
                        //     .values()
                        //     .into_iter()
                        //     .all(|g_top_k| !g_top_k.contains(k1));
                        // if !empty_top_k && k1_unused {
                        //     tracing::error!("removing k1!");
                        // }
                        // !(empty_top_k && k1_unused)
                        !empty_top_k
                    });
                    let after = by_key1.len();
                    tracing::debug!(
                        target: "topk",
                        "@{} size of topk nested by_key1[{}] hashmap (before {before}, after {after})",
                        time.time(),
                        std::any::type_name::<K1>()
                    );

                    tracing::debug!(
                        target: "topk",
                        "@{} size of topk nested to_emit hashmap = {}",
                        time.time(),
                        to_emit.len()
                    );
                });
            }
        })
    }
}

/// A dataflow operator to write a stream of data to redis
/// TODO, re-evaluate if WorkerTypes is needed
pub trait WriteToRedis<S: Scope> {
    fn write_to_redis(
        &self,
        namespace: String,
        client: Arc<ClusterClient>,
        worker_types: WorkerSelection,
    ) -> Result<()>;
}

impl<S: Scope, R: Clone + 'static> WriteToRedis<S> for Stream<S, R>
where
    Vec<R>: IoData,
    S: ScopeParent<Timestamp = u64>,
{
    fn write_to_redis(
        &self,
        namespace: String,
        client: Arc<ClusterClient>,
        worker_types: WorkerSelection,
    ) -> Result<()> {
        let mut con = client.get_connection()?;

        self.sink(Pipeline, "write to redis", move |input| {
            let mut write_vec = Vec::new();
            while let Some((time, data)) = input.next() {
                data.swap(&mut write_vec);
                let num_keys = write_vec.len();
                match write_vec.insert_logged(
                    &namespace,
                    &mut con,
                    time.time(),
                    num_keys,
                    worker_types.worker_index,
                ) {
                    Ok(_) => (),
                    Err(_) => {
                        if !con.check_connection() {
                            tracing::error!(
                                "reconnecting to redis for writing {}",
                                std::any::type_name::<R>()
                            );
                            con = client.get_connection().expect("connection broken");
                        }
                    }
                };
            }
        });
        Ok(())
    }
}

/// A dataflow operator that takes a stream of `T` and emits a stream of T. A key
/// is used to determine how to group the elements of T, and a value function is used to determine
/// how to compare elements of T to determine the largest T among those mapping to the same key.
/// At each interval, the T with the largest value is emitted for each observed key.
/// If needed, this could be generalized with a `reduce_fn` to support other operators than max.
/// Example: produce a stream of `
/// mulative` which maximizes the overall decaying count (value fn)
/// for each observed page (key fn) every 5 seconds
pub trait MaxByKey<S: Scope, T> {
    fn max_by_key<const HL: usize, K: Eq + Hash + 'static>(
        &self,
        emit_iterval: Duration,
        key_fn: impl FnMut(&T) -> K + 'static + Clone,
        value_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        worker_selection: WorkerSelection,
    ) -> Stream<S, T>;
}

impl<S: Scope, T: ExchangeData> MaxByKey<S, T> for Stream<S, T>
where
    S: ScopeParent<Timestamp = u64>,
{
    fn max_by_key<const HL: usize, K: Eq + Hash + 'static>(
        &self,
        emit_iterval: Duration,
        mut key_fn: impl FnMut(&T) -> K + 'static + Clone,
        mut value_fn: impl FnMut(&T) -> DecayedValue<HL> + 'static + Clone,
        worker_selection: WorkerSelection,
    ) -> Stream<S, T> {
        let emit_iterval_ms = emit_iterval.as_millis() as u64;
        let mut exchange_fn = key_fn.clone();
        let pact = ExchangePact::new(move |t: &T| {
            let mut hasher = DefaultHasher::new();
            exchange_fn(t).hash(&mut hasher);
            hasher
                .finish()
                .rem_euclid(worker_selection.compute_worker_modulo())
        });
        self.unary_frontier(pact, "max_by_key", |_default_cap, _info| {
            // vec used to drain input batch
            let mut batch_vec = Vec::new();

            let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();

            let mut to_emit = HashMap::new();

            move |input, output| {
                while let Some((time, data)) = input.next() {
                    // consume new batch of features
                    data.swap(&mut batch_vec);

                    // compute batch and add notification for end of batch
                    let batch_cap = worker_selection.shifted_batch(&time, emit_iterval_ms);
                    notificator.notify_at(batch_cap);

                    // insert new requests into cms
                    for t in batch_vec.drain(..) {
                        let key = key_fn(&t);
                        let dv = value_fn(&t);

                        match to_emit.entry(key) {
                            Entry::Occupied(mut entry) => {
                                let existing_t = entry.get_mut();
                                let existing_dv = value_fn(existing_t);
                                if existing_dv < dv {
                                    *existing_t = t;
                                }
                            }
                            Entry::Vacant(entry) => {
                                entry.insert(t);
                            }
                        }
                    }
                }
                notificator.for_each(&[input.frontier()], |time, _notificator| {
                    let values = to_emit.drain().map(|(_k, v)| v);
                    output.session(&time).give_iterator(values);
                });
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use domain::api::{BaseUrl, Fingerprint, Geography};
    use serde::{Deserialize, Serialize};
    use timely::{
        dataflow::{
            operators::{Capture, Map as _, Probe as _, UnorderedInput as _},
            ProbeHandle,
        },
        Config,
    };

    pub const HALF_LIFE: usize = 100;

    #[test]
    fn worker_types() {
        for i in 0..6 {
            let worker_types = WorkerSelection {
                worker_index: i,
                compute_workers: 6,
                io_workers: 3,
            };
            assert_eq!(worker_types.is_compute(), true);
        }

        let worker_types = WorkerSelection {
            worker_index: 0,
            compute_workers: 6,
            io_workers: 3,
        };
        for _ in 0..100 {
            let io_worker = worker_types.random_io_worker_index();
            assert!(io_worker >= 6 && io_worker < 9);
        }
    }

    type G<'a> = timely::dataflow::scopes::Child<'a, Worker<Generic>, u64>;
    /// Helper function to test operators
    pub fn test_op<I, O, F>(
        data: Vec<(u64, Vec<I>)>,
        expected: Vec<(u64, Vec<O>)>,
        num_workers: usize,
        logic: F,
    ) where
        I: Clone + Sync + Send + 'static,
        O: Clone
            + Debug
            + Sync
            + Send
            + Serialize
            + for<'a> Deserialize<'a>
            + Ord
            + PartialEq
            + PartialOrd
            + Eq
            + 'static,
        F: Fn(WorkerSelection, Stream<G, I>) -> Stream<G, O> + Send + Sync + 'static,
    {
        // timely::execute(Config::process(num_workers), move |worker| {
        timely::execute(Config::process(num_workers), move |worker| {
            let mut probe = ProbeHandle::new();
            let worker_selection = WorkerSelection::new(worker, 0);

            // create a new input, exchange data, and inspect its output
            let ((mut input, mut cap), receiver) = worker.dataflow::<u64, _, _>(|scope| {
                let (input, stream) = scope.new_unordered_input();
                let receiver = logic(worker_selection, stream)
                    .probe_with(&mut probe)
                    .capture();

                (input, receiver)
            });
            if worker_selection.worker_index == 0 {
                // start at 0
                cap = cap.delayed(&0);
                // insert data
                for (time, events) in data.clone() {
                    let mut session = input.session(cap.delayed(&time));
                    for e in events {
                        session.give(e);
                    }
                    cap = cap.delayed(&time);

                    while probe.less_than(&time) {
                        worker.step();
                    }
                }
            }
            cap = cap.delayed(&u64::MAX);
            while probe.less_than(&u64::MAX) {
                worker.step();
            }

            //TODO running .extract() on the .capture()'ed stream does not complete, presumably because the
            //input doesn't close/get dropped. Instead, poor man's approach to collecting into vec
            let mut result = vec![];
            while let Ok(recv) = receiver.recv_timeout(Duration::from_millis(100)) {
                if let timely::dataflow::operators::capture::Event::Messages(t, mut d) = recv {
                    d.sort();
                    result.push((t, d));
                }
            }
            assert_eq!(result, expected);
        })
        .expect("dataflow test failed");
    }

    #[test]
    fn topk() {
        #[derive(Clone, Debug, Serialize, Deserialize, Ord, PartialEq, PartialOrd, Eq)]
        struct Data {
            group: String,
            key: String,
            value: usize,
        }
        impl Data {
            pub fn new(encoded: &str) -> Self {
                match encoded.split(',').collect::<Vec<_>>()[..] {
                    [group, key, value_str] => Data {
                        group: group.to_owned(),
                        key: key.to_owned(),
                        value: value_str.parse().expect("invalid value"),
                    },
                    _ => panic!("entry data {encoded}"),
                }
            }
        }

        // for simplicity, the decaying values are all using the same time offset
        // a key appearing in a previous interval will thus have the same value
        let data = vec![
            (
                59u64,
                vec![
                    "g1,k1,1", "g1,k2,2", "g1,k3,4", "g2,k1,4", "g2,k2,1", "g2,k3,3",
                ],
            ),
            (123u64, vec!["g1,k1,5", "g1,k2,3", "g2,k2,6", "g2,k3,5"]),
        ]
        .into_iter()
        .map(|(t, vs)| (t, vs.into_iter().map(Data::new).collect()))
        .collect();

        let expected = vec![
            (
                100,
                vec![("g1", vec!["k3", "k2"]), ("g2", vec!["k1", "k3"])],
            ),
            (
                200,
                vec![("g1", vec!["k1", "k3"]), ("g2", vec!["k2", "k3"])],
            ),
        ]
        .into_iter()
        .map(|(t, topk)| {
            let topk = topk
                .into_iter()
                .map(|(g, ks)| (g.to_owned(), ks.into_iter().map(|k| k.to_owned()).collect()))
                .collect();
            (t as u64, topk)
        })
        .collect();

        test_op(data, expected, 1, |worker_selection, stream| {
            stream
                .top_k(
                    2,
                    |data: &Data| data.group.to_owned(),
                    |data: &Data| data.key.to_owned(),
                    |data| DecayedValue::<{ HALF_LIFE }>::new(data.value as f64, 0.0),
                    Duration::from_millis(100),
                    worker_selection,
                )
                .map(|(group, topk)| {
                    let keys: Vec<String> =
                        topk.to_domain().topk.into_iter().map(|(k, _)| k).collect();
                    (group, keys)
                })
        })
    }
    #[test]
    fn topk_nested() {
        #[derive(Clone, Debug, Serialize, Deserialize, Ord, PartialEq, PartialOrd, Eq)]
        struct Data {
            group: String,
            key: String,
            nested: String,
            key_value: usize,
            nested_value: usize,
        }
        impl Data {
            pub fn new(encoded: &str) -> Self {
                match encoded.split(',').collect::<Vec<_>>()[..] {
                    [group, key, nested, key_value_str, nested_value_str] => Data {
                        group: group.to_owned(),
                        key: key.to_owned(),
                        nested: nested.to_owned(),
                        key_value: key_value_str.parse().expect("invalid value"),
                        nested_value: nested_value_str.parse().expect("invalid value"),
                    },
                    _ => panic!("entry data {encoded}"),
                }
            }
        }

        // for simplicity, the decaying values are all using the same time offset
        // a key appearing in a previous interval will thus have the same value
        let data = vec![
            (
                59u64,
                vec![
                    "g1,k1,n1,1,3",
                    "g1,k1,n2,1,4",
                    "g1,k1,n3,1,5",
                    "g1,k2,n1,2,7",
                    "g1,k2,n2,2,4",
                    "g1,k2,n3,2,3",
                    "g1,k3,n1,0,6",
                ],
            ),
            (
                123u64,
                vec![
                    "g1,k1,n3,1,8",
                    "g1,k2,n3,2,5",
                    "g1,k2,n4,2,6",
                    "g1,k3,n1,3,6",
                ],
            ),
        ]
        .into_iter()
        .map(|(t, vs)| (t, vs.into_iter().map(Data::new).collect()))
        .collect();

        let expected = vec![
            (
                100,
                vec![(
                    "g1".to_string(),
                    vec![
                        ("k2".to_string(), vec!["n1".to_string(), "n2".to_string()]),
                        ("k1".to_string(), vec!["n3".to_string(), "n2".to_string()]),
                    ],
                )],
            ),
            (
                200,
                vec![(
                    "g1".to_string(),
                    vec![
                        ("k3".to_string(), vec!["n1".to_string()]),
                        ("k2".to_string(), vec!["n1".to_string(), "n4".to_string()]),
                    ],
                )],
            ),
        ];

        test_op(data, expected, 1, |worker_selection, stream| {
            stream
                .top_k_nested(
                    2,
                    2,
                    |data: &Data| data.group.to_owned(),
                    |data: &Data| data.key.to_owned(),
                    |data: &Data| Some(data.nested.to_owned()),
                    |data| DecayedValue::<{ HALF_LIFE }>::new(data.key_value as f64, 0.0),
                    |data| DecayedValue::<{ HALF_LIFE }>::new(data.nested_value as f64, 0.0),
                    Duration::from_millis(100),
                    worker_selection,
                )
                .map(|(group, (topk, nested))| {
                    let mut nested: HashMap<String, Vec<String>> = nested
                        .into_iter()
                        .map(|(k, topk)| {
                            let nested =
                                topk.to_domain().topk.into_iter().map(|(k, _)| k).collect();
                            (k, nested)
                        })
                        .collect();
                    let keys: Vec<(String, Vec<String>)> = topk
                        .to_domain()
                        .topk
                        .into_iter()
                        .map(|(k, _)| {
                            let n = nested.remove(&k).unwrap();
                            (k, n)
                        })
                        .collect();
                    (group, keys)
                })
        })
    }

    #[test]
    fn max_by_key() {
        #[derive(Clone, Debug, Serialize, Deserialize, Ord, PartialEq, PartialOrd, Eq)]
        struct Data {
            key: String,
            value: usize,
        }
        impl Data {
            pub fn new(k: &str, v: usize) -> Self {
                Data {
                    key: k.to_owned(),
                    value: v,
                }
            }
        }
        let data = vec![
            (
                59u64,
                vec![Data::new("k1", 1), Data::new("k2", 1), Data::new("k1", 2)],
            ),
            (
                123u64,
                vec![Data::new("k1", 3), Data::new("k2", 1), Data::new("k1", 2)],
            ),
            (
                166u64,
                vec![Data::new("k1", 6), Data::new("k2", 1), Data::new("k2", 4)],
            ),
            (
                356u64,
                vec![Data::new("k1", 1), Data::new("k2", 1), Data::new("k2", 4)],
            ),
        ];

        let expected = vec![
            (100, vec![Data::new("k1", 2), Data::new("k2", 1)]),
            (200, vec![Data::new("k1", 6), Data::new("k2", 4)]),
            (400, vec![Data::new("k1", 1), Data::new("k2", 4)]),
        ];

        test_op(data, expected, 1, |worker_selection, stream| {
            stream.max_by_key(
                Duration::from_millis(100),
                |data: &Data| data.key.to_owned(),
                |data| DecayedValue::<{ HALF_LIFE }>::new(data.value as f64, 0.0),
                worker_selection,
            )
        })
    }
}
