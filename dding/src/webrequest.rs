//! Defines the WebRequest stuct and implementation for extracting information.
use anyhow::{Context, Result};
use domain::api::{Fingerprint, Geography};
use once_cell::sync::Lazy;
use serde::Deserialize;
use time::{format_description::well_known::Iso8601, OffsetDateTime, PrimitiveDateTime};

/// Json struct for webrequest_text kafka stream
#[derive(Clone, Debug, Default, Deserialize)]
pub struct WebRequest {
    hostname: String,            // "cp4036.ulsfo.wmnet"
    sequence: u64,               // 9716003321
    pub dt: String,              // "2022-09-07T02:29:58Z"
    time_firstbyte: f32,         // 0.000211
    pub ip: String,              // "49.227.71.45"
    pub cache_status: String,    // "hit-front"
    http_status: String,         // "200"
    response_size: u32,          // 1848
    http_method: String,         // "GET"
    pub uri_host: String,        // "en.m.wikipedia.org"
    pub uri_path: String,        // "/w/load.php"
    pub uri_query: String, // "?lang=en&modules=ext.relatedArticles.readMore&skin=minerva&version=1csry"
    content_type: String,  // "text/javascript; charset=utf-8"
    referer: String,       // "https: String//en.m.wikipedia.org/wiki/Magic_Mike_XXL"
    pub user_agent: String, // "Mozilla/5.0 (iPhone; CPU iPhone OS 15_6_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.6.1 Mobile/15E148 Safari/604.1"
    pub accept_language: String, // "en-au,en;q=0.9"
    x_analytics: String, // "WMF-Last-Access=07-Sep-2022;WMF-Last-Access-Global=07-Sep-2022;https=1;client_port=49275"
    range: String,       // "-"
    x_cache: String,     // "cp4036 hit, cp4036 hit/112112"
    accept: String,      // "*/*"
    backend: String,     // "ATS/8.0.8"
    tls: String, // "vers=TLSv1.3;keyx=UNKNOWN;auth=ECDSA;ciph=AES-256-GCM-SHA384;prot=h2;sess=new"
}

static URI_PATH_API: &str = "api.php";

static URI_QUALIFIERS: [&str; 6] = ["m", "mobile", "wap", "zero", "www", "download"];
static WMF_DOMAINS: [&str; 17] = [
    "wikimedia.org",
    "wikibooks.org",
    "wikinews.org",
    "wikipedia.org",
    "wikiquote.org",
    "wikisource.org",
    "wiktionary.org",
    "wikiversity.org",
    "wikivoyage.org",
    "wikidata.org",
    "mediawiki.org",
    "wikimediafoundation.org",
    "wikiworkshop.org",
    "wmfusercontent.org",
    "wmflabs.org",
    "wmcloud.org",
    "toolforge.org",
];

static SUCCESS_HTTP_STATUSES: [&str; 2] = ["200", "304"];
static REDIRECT_HTTP_STATUSES: [&str; 3] = ["301", "302", "307"];

static TEXT_HTML_CONTENT_TYPES: [&str; 5] = [
    "text/html",
    "text/html; charset=iso-8859-1",
    "text/html; charset=ISO-8859-1",
    "text/html; charset=utf-8",
    "text/html; charset=UTF-8",
];

static PAGE_TITLE_PATHS: [&str; 5] = [
    "/wiki/",
    "/w/index.php/",
    "/api/rest_v1/page/mobile-sections-lead/",
    "/api/rest_v1/page/mobile-sections/",
    "/api/rest_v1/page/mobile-html/",
];

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct NormalizedHost {
    pub project: Option<String>,
    pub qualifiers: Vec<String>,
    pub project_family: String,
    pub tld: String,
}

static MAXMIND_CITY: Lazy<maxminddb::Reader<Vec<u8>>> = Lazy::new(|| {
    maxminddb::Reader::open_readfile("/usr/share/GeoIP/GeoIP2-City.mmdb")
        .expect("Error opening maxmind database")
});
static MAXMIND_ISP: Lazy<maxminddb::Reader<Vec<u8>>> = Lazy::new(|| {
    maxminddb::Reader::open_readfile("/usr/share/GeoIP/GeoIP2-ISP.mmdb")
        .expect("Error opening maxmind database")
});

/// extractor functions based on
/// https://github.com/wikimedia/analytics-refinery-source/blob/8154872ac55a7059dacb39ba14566ea388b3720b/refinery-core/src/main/java/org/wikimedia/analytics/refinery/core/Webrequest.java#L32
///
/// X-Analytics: https://wikitech.wikimedia.org/wiki/X-Analytics
impl WebRequest {
    /// Parse WebRequest from a json string (e.g. from kafka)
    pub fn from_json(json: &str) -> Result<WebRequest> {
        serde_json::from_str::<WebRequest>(json).context("Failed to deserialize WebRequest")
    }

    /// parse uri_host
    /// "en.m.wikipedia.org"
    /// TODO edge cases schmedge cases
    pub fn normalized_host(&self) -> Option<NormalizedHost> {
        let parts: Vec<&str> = self.uri_host.split('.').collect();
        match parts[..] {
            [project_family, tld] => Some(NormalizedHost {
                project: None,
                qualifiers: vec![],
                project_family: project_family.to_string(),
                tld: tld.to_string(),
            }),
            [project, project_family, tld] => Some(NormalizedHost {
                project: Some(project.to_string()),
                qualifiers: vec![],
                project_family: project_family.to_string(),
                tld: tld.to_string(),
            }),
            [project, qualifier, project_family, tld] => {
                if !URI_QUALIFIERS.contains(&project) && URI_QUALIFIERS.contains(&qualifier) {
                    Some(NormalizedHost {
                        project: Some(project.to_string()),
                        qualifiers: vec![qualifier.to_string()],
                        project_family: project_family.to_string(),
                        tld: tld.to_string(),
                    })
                } else {
                    //TODO
                    None
                }
            }
            _ => None,
        }
    }

    /// poor man's pageview classifier
    /// https://github.com/wikimedia/analytics-refinery-source/blob/8154872ac55a7059dacb39ba14566ea388b3720b/refinery-core/src/main/java/org/wikimedia/analytics/refinery/core/PageviewDefinition.java#L267
    /// TODO edge cases schmedge cases
    pub fn is_page_view(&self) -> bool {
        let wmf_host = WMF_DOMAINS
            .iter()
            .any(|domain| self.uri_host.contains(domain));
        let success = SUCCESS_HTTP_STATUSES.contains(&self.http_status.as_str());

        let app_agent = self.user_agent.contains("WikipediaApp");

        let web_pageview = {
            let html_content_type = TEXT_HTML_CONTENT_TYPES.contains(&self.content_type.as_str());
            let api = self.uri_path.contains(URI_PATH_API);
            let edit = self.uri_query.contains("action=edit");
            !app_agent && html_content_type && !api && !edit
        };

        let app_page_view = {
            let tagged = self.x_analytics.contains("pageview=1");
            app_agent && tagged
        };

        wmf_host && success && (web_pageview || app_page_view)
    }

    /// extract the page title from uri_path / uri_query
    /// order of preference:
    ///   1. "title" query param from uri_query
    ///   2. extracted from uri_path
    ///   3. "page" query param from uri_query
    pub fn page_title(&self) -> Option<String> {
        if !self.is_page_view() {
            return None;
        }

        let mut page_query_param: Option<&str> = None;
        for pair in self.uri_query.split('&') {
            match pair.split('=').collect::<Vec<_>>()[..] {
                [key, value] => {
                    // if there is a title param, use it
                    if key == "title" {
                        // if there is a title param, return since #1 choice
                        return Some(value.to_string());
                    } else if key == "page" {
                        page_query_param = Some(value)
                    }
                }
                _ => (),
            }
        }
        // General case of page title in path
        let path_page_title = PAGE_TITLE_PATHS
            .iter()
            .map(|path| {
                if self.uri_path.contains(path) {
                    Some(&self.uri_path[path.len()..])
                } else {
                    None
                }
            })
            .find(|o| o.is_some())
            .flatten();

        path_page_title
            .or(page_query_param)
            .and_then(|s| {
                //TODO wiki page name encoding is a science, https://en.wikipedia.org/wiki/Wikipedia:Page_name
                //simply url decoding is too simple to be correct...
                //TODO add error handling
                urlencoding::decode(s).ok()
            })
            .map(|s| s.to_string())
    }

    /// Returns the string value the field `name` form the x_analytics header, if it exists
    pub fn x_analytics(&self, name: &str) -> Option<&str> {
        let found = self
            .x_analytics
            .split(';')
            .flat_map(|pair| pair.split_once('='))
            .find(|(field, _)| *field == name);
        found.map(|(_, value)| value)
    }

    /// Extract the page_id from the x_analytics header

    pub fn page_id(&self) -> Option<i32> {
        self.x_analytics("page_id").and_then(|v| v.parse().ok())
    }

    /// Parses the timestamp as UTC
    pub fn datetime(&self) -> Result<OffsetDateTime> {
        PrimitiveDateTime::parse(&self.dt, &Iso8601::DEFAULT)
            .map(|date| date.assume_utc())
            .context("Failed to parse datetime")
    }

    /// Did the request go past the caching layer?
    pub fn is_cache_miss_or_pass(&self) -> bool {
        self.cache_status == "miss" || self.cache_status == "pass"
    }

    pub fn fingerprint(&self) -> Fingerprint {
        Fingerprint {
            ip: self.ip.clone(),
            user_agent: self.user_agent.clone(),
            accept_language: self.accept_language.clone(),
        }
    }
    pub fn geography(&self) -> Geography {
        let ip_addr = self.ip.parse().ok();
        let country: String = ip_addr
            .and_then(|ip_addr| {
                let country: maxminddb::geoip2::Country = MAXMIND_CITY.lookup(ip_addr).ok()?;
                let names = country.country?.names?;
                let name = names.get("en").map(|n| n.to_string());
                name
            })
            .unwrap_or("Unknown".to_string());

        let isp: String = ip_addr
            .and_then(|ip_addr| {
                let isp = MAXMIND_ISP.lookup::<maxminddb::geoip2::Isp>(ip_addr).ok()?;
                isp.isp.map(|isp| isp.to_string())
            })
            .unwrap_or("Unknown".to_string());

        Geography {
            country,
            wmf_region: "TODO".to_string(),
            isp,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_x_analytics() {
        let mut req = WebRequest::default();
        req.x_analytics = "ns=5;page_id=18951222;https=1;client_port=59600;nocookies=1".to_owned();
        assert_eq!(req.x_analytics("ns"), Some("5"));
        assert_eq!(req.x_analytics("nocookies"), Some("1"));

        assert_eq!(req.x_analytics("page_id"), Some("18951222"));
        assert_eq!(req.page_id(), Some(18951222));
    }

    #[test]
    fn cache_miss_or_pass() {
        let mut req = WebRequest::default();
        req.cache_status = "pass".to_owned();
        assert!(req.is_cache_miss_or_pass());
        req.cache_status = "miss".to_owned();
        assert!(req.is_cache_miss_or_pass());
        req.cache_status = "hit-front".to_owned();
        assert!(!req.is_cache_miss_or_pass());
    }
}
