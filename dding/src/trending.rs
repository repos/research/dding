//! Data structures and implementations to help detect trending content
//! in a streaming pipeline.
use std::collections::hash_map::DefaultHasher;
use std::fmt::Debug;
use std::hash::Hasher as _;
use std::{
    collections::{hash_map::Entry, HashMap},
    hash::Hash,
    time::Duration,
};

use time::OffsetDateTime;
use timely::dataflow::operators::Exchange;
use timely::{
    dataflow::{channels::pact::Pipeline, operators::Operator as _, Scope, ScopeParent, Stream},
    ExchangeData,
};
use tracing::Level;

use crate::operators::{MaxByKey as _, WorkerSelection};
use crate::{decay::DecayedValue, operators::processing_datetime};

// trending config
#[derive(Clone, Copy)]
pub struct TrendingConfig {
    // min value to be a candidate
    pub min_value: f64,
    // minimum average % growth
    pub min_growth_mean: f64,
    // maximum std of growth
    pub max_growth_std: f64,
    // number of observations required to detect
    pub min_detection_count: i32,
    // avg % growth to discard candidate
    pub disregard_growth: f64,
    // number of observations after which to discard candidate
    pub disregard_count: i32,
    // % of the original trending value at which a trend ends
    pub trend_end_theshold: f64,
    // update interval of trend after detection
    pub trend_update_interval: Duration,
    // minimum duration before trend can end
    pub min_trend_duration: Duration,
    // maximum duration of a trend
    pub max_trend_duration: Duration,
}
impl Default for TrendingConfig {
    fn default() -> Self {
        TrendingConfig {
            min_value: 500.0,
            min_growth_mean: 1.02,
            max_growth_std: 0.2,
            min_detection_count: 24,
            disregard_growth: 0.9,
            disregard_count: 100,
            trend_end_theshold: 0.9,
            trend_update_interval: Duration::from_secs(60),
            min_trend_duration: Duration::from_secs(2 * 60),
            max_trend_duration: Duration::from_secs(2 * 3600),
        }
    }
}
// struct to determine which elements might be trending
#[derive(Debug, Clone)]
pub struct TrendingFeatures {
    /// tuple of unix timestamp and observed value
    pub values: Vec<(u64, f64)>,
}
impl TrendingFeatures {
    pub fn insert(&mut self, ts: u64, val: f64) {
        self.values.push((ts, val));
        // self.unique_fingerprints.push(fingerprint);
    }

    pub fn len(&self) -> usize {
        self.values.len()
    }

    fn mean_growth(&self) -> f64 {
        self.values
            .iter()
            .fold((0f64, None), |(count, prev_val), (_ts, cval)| {
                // TODO ts is not taking into account atm
                let ratio: f64 = prev_val.map(|pval| cval / pval).unwrap_or(1.0);
                (count + ratio, Some(cval))
            })
            .0
            / self.values.len() as f64
    }

    fn std_growth(&self) -> f64 {
        let mean = self.mean_growth();
        let var = self
            .values
            .iter()
            .fold((0f64, None), |(count, prev_val), (_ts, cval)| {
                let ratio: f64 = prev_val.map(|pval| cval / pval).unwrap_or(1.0);
                let diff = ratio - mean;
                let diff = diff * diff;
                (count + diff, Some(cval))
            })
            .0
            / self.values.len() as f64;
        var.sqrt()
    }

    pub fn is_trending(&self, config: &TrendingConfig) -> bool {
        self.values.len() as i32 > config.min_detection_count
            && self.mean_growth() > config.min_growth_mean
            && self.std_growth() < config.max_growth_std
    }

    pub fn disregard(&self, config: &TrendingConfig) -> bool {
        self.mean_growth() < config.disregard_growth
            || self.values.len() as i32 > config.disregard_count
    }
}

impl Default for TrendingFeatures {
    fn default() -> Self {
        TrendingFeatures { values: Vec::new() }
    }
}

#[derive(Clone, Debug)]
pub enum Event {
    Start {
        start: time::OffsetDateTime,
        features: TrendingFeatures,
    },
    Ongoing {
        start: time::OffsetDateTime,
        pre: TrendingFeatures,
        post: TrendingFeatures,
    },
    End {
        start: time::OffsetDateTime,
        end: time::OffsetDateTime,
        pre: TrendingFeatures,
        post: TrendingFeatures,
    },
}

#[derive(Clone, Debug)]
pub struct TrendEvent<T> {
    pub element: T,
    pub event: Event,
}

impl<T> TrendEvent<T> {
    pub fn update(&self, val: f64, dt: OffsetDateTime, config: TrendingConfig) -> Option<Event> {
        match &self.event {
            Event::Start { start, features } => {
                let mut post = TrendingFeatures::default();
                post.insert(dt.unix_timestamp() as u64, val);
                Some(Event::Ongoing {
                    start: *start,
                    pre: features.clone(),
                    post,
                })
            }
            Event::Ongoing { start, pre, post } => {
                let (last_ts, _) = post.values.last().expect("required for detection");
                let new_ts = dt.unix_timestamp() as u64;
                // an ongoing event is only updateded at intervals
                if (&new_ts - last_ts) > config.trend_update_interval.as_secs() as u64 {
                    let mut post = post.clone();
                    post.insert(new_ts, val);
                    let mean = post.values.iter().map(|(_, v)| v).sum::<f64>() / post.len() as f64;
                    let (_, start_value) = pre.values.last().expect("required for detection");
                    // trend ends when values are below the starting point
                    // or longer than the max trend duration
                    if (dt - *start > config.min_trend_duration
                        && mean < start_value * config.trend_end_theshold)
                        || dt - *start > config.max_trend_duration
                    {
                        Some(Event::End {
                            start: *start,
                            end: dt,
                            pre: pre.clone(),
                            post,
                        })
                    } else {
                        Some(Event::Ongoing {
                            start: *start,
                            pre: pre.clone(),
                            post,
                        })
                    }
                } else {
                    None
                }
            }
            Event::End { .. } => None,
        }
    }
}

/// A dataflow operator to do a trend/peak detection using a simple heuristic
pub trait TrendDetection<S: Scope, T> {
    fn trend_detection(
        &self,
        trending_config: TrendingConfig,
        event_time_zero: time::OffsetDateTime,
        emit_interval: Duration,
        warmup: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, TrendEvent<T>>;
}

impl<S: Scope, T: Debug + ExchangeData + Hash + Eq + PartialEq, const HL: usize>
    TrendDetection<S, T> for Stream<S, (T, DecayedValue<HL>)>
where
    S: ScopeParent<Timestamp = u64>,
{
    fn trend_detection(
        &self,
        trending_config: TrendingConfig,
        event_time_zero: time::OffsetDateTime,
        emit_interval: Duration,
        warmup: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<S, TrendEvent<T>> {
        let span = tracing::span!(Level::DEBUG, "trending_detection");
        let _enter = span.enter();

        let trends = self
            .exchange(move |(t, _)| -> u64 {
                let mut hasher = DefaultHasher::new();
                t.to_owned().hash(&mut hasher);
                hasher
                    .finish()
                    .rem_euclid(worker_selection.compute_worker_modulo())
            })
            .max_by_key(
                emit_interval,
                |(t, _)| t.to_owned(),
                |(_, dv)| dv.to_owned(),
                worker_selection,
            )
            .unary_frontier(Pipeline, "trending_detection", |_default_cap, _info| {
                // vec used to drain input batch
                let mut batch_vec = Vec::new();

                let mut trending_candidates: HashMap<T, TrendingFeatures> = HashMap::new();
                let mut active_trends: HashMap<T, TrendEvent<T>> = HashMap::new();
                move |input, output| {
                    while let Some((time, data)) = input.next() {
                        // consume new batch of features
                        let timestamp = *time.time();
                        let datetime = processing_datetime(timestamp, &event_time_zero);

                        if timestamp > warmup.as_millis() as u64 {
                            data.swap(&mut batch_vec);

                            for (t, dv) in batch_vec.drain(..) {
                                let val: f64 = dv.value_as_of(timestamp as f64);
                                match active_trends.entry(t.clone()) {
                                    Entry::Occupied(mut trend_entry) => {
                                        let trend = trend_entry.get_mut();
                                        if let Some(event) =
                                            trend.update(val, datetime, trending_config)
                                        {
                                            trend.event = event;
                                            output.session(&time).give(trend.clone());
                                            if let Event::End { .. } = trend.event {
                                                tracing::debug!(target: "trending_detection", "END TREND {trend:?}");
                                                trend_entry.remove();
                                            }
                                        };
                                    }
                                    Entry::Vacant(trend_entry) => {
                                        let ts = datetime.unix_timestamp() as u64;
                                        let ncandidates = trending_candidates.len();
                                        match trending_candidates.entry(t.clone()) {
                                            Entry::Occupied(mut candidate_entry) => {
                                                if val > trending_config.min_value {
                                                    let trending_features =
                                                        candidate_entry.get_mut();
                                                    trending_features.insert(ts, val);

                                                    if trending_features
                                                        .is_trending(&trending_config)
                                                    {
                                                        tracing::debug!(target: "trending_detection", "START TREND element {t:?}");
                                                        tracing::debug!(target: "trending_detection", "{trending_features:?} avg: {}, std {}",
                                                            trending_features.mean_growth(),
                                                            trending_features.std_growth()
                                                        );
                                                        let features = candidate_entry.remove();
                                                        let trend = TrendEvent {
                                                            element: t,
                                                            event: Event::Start {
                                                                start: datetime,
                                                                features: features,
                                                            },
                                                        };
                                                        trend_entry.insert(trend.clone());
                                                        output.session(&time).give(trend);
                                                    } else if trending_features
                                                        .disregard(&trending_config)
                                                    {
                                                        tracing::debug!(target: "trending_detection", "removed candidate trend {t:?}. size of candidates: {ncandidates}");
                                                        candidate_entry.remove();
                                                    }
                                                } else {
                                                    tracing::debug!(target: "trending_detection", "removed candidate trend {t:?}. size of candidates: {ncandidates}");
                                                    candidate_entry.remove();
                                                }
                                            }
                                            Entry::Vacant(candidate_entry) => {
                                                if val > trending_config.min_value {
                                                    tracing::debug!(target: "trending_detection", "new candidate trend {t:?}. size of candidates: {ncandidates}");
                                                    let mut trending_features =
                                                        TrendingFeatures::default();
                                                    trending_features.insert(ts, val);
                                                    candidate_entry.insert(trending_features);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        trends
    }
}
