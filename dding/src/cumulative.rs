//! Data structures and implementations to maintain cumulative decaying counts
//! in a streaming pipeline.

use crate::{decay::DecayedValue, operators::WorkerSelection};
use domain::api::{BaseUrl, Country, Fingerprint, Geography, Ip, Isp, UserAgent};
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::{
    collections::{
        hash_map::{DefaultHasher, Entry},
        HashMap, HashSet,
    },
    hash::{Hash, Hasher},
    time::Duration,
};
use streaming_algorithms::CountMinSketch;
use timely::{
    dataflow::{
        channels::pact::Exchange as ExchangePact,
        operators::{
            generic::{builder_rc::OperatorBuilder, FrontieredInputHandleCore},
            Capability, FrontierNotificator, Map as _, Operator as _,
        },
        Scope, ScopeParent, Stream,
    },
    ExchangeData,
};

/// A count-min-sketch to aggregate decaying counts for elements of K
struct DecayedCMS<K: Hash + Sized, const HL: usize>(CountMinSketch<K, DecayedValue<HL>>);
impl<const HL: usize, K: Hash + Sized> DecayedCMS<K, HL> {
    pub fn new(probability: f64, tolerance: f64) -> Self {
        DecayedCMS(CountMinSketch::new(probability, tolerance, ()))
    }
    pub fn push_at(&mut self, key: &K, time: f64) {
        let dv: DecayedValue<HL> = DecayedValue::new(1f64, time);
        self.0.push(key, &dv);
    }
    pub fn get_at(&self, key: &K, time: f64) -> DecayedValue<HL> {
        let mut dv = self.0.get(key);
        dv.update_as_of(time);
        dv
    }
}

/// Extractor trait required to calculate
/// decaying counts using the `Visits` struct
pub trait KeyedFields {
    fn fingerprint(&self) -> &Fingerprint;
    fn geography(&self) -> &Geography;
    fn base_url(&self) -> BaseUrl;
    fn relative_event_time(&self) -> f64;
}

/// The key associated to group a feature struct by
/// Example, a `Pageview` contains relevant features
/// about a pageview, the key to group by is a `Page`
pub trait Keyed {
    type Key: Hash;
    fn key(&self) -> &Self::Key;
}

/// Cumulative decaying that are associated
/// with a key value that is derived from the Keyed implementation
/// for T
#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct KeyedCumulative<const HL: usize> {
    pub key: DecayedValue<HL>,
    pub key_country: DecayedValue<HL>,
    pub key_isp: DecayedValue<HL>,
    pub key_ip: DecayedValue<HL>,
    pub key_user_agent: DecayedValue<HL>,
    pub key_fingerprint: DecayedValue<HL>,
}

/// Cumulative decaying values grouped by country
/// An IP is assumed to always be in the same country.
/// TODO: assumption are not 100% correct but good enough?
#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct CountryCumulative<const HL: usize> {
    pub country: DecayedValue<HL>,
    pub ip: DecayedValue<HL>,
    pub fingerprint: DecayedValue<HL>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct IspCumulative<const HL: usize> {
    pub isp: DecayedValue<HL>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct BaseUrlCumulative<const HL: usize> {
    pub base_url: DecayedValue<HL>,
}

trait UpdatePartial<const HL: usize>: Sized {
    /// How to ingest a partial cumulative
    fn into_partial(self, partial_cumulative: &mut PartialCumulative<HL>);
    /// method to update a hashmap of partial cumulatives. Consumes self.
    fn update_partial_state<T: Clone + Debug + PartialEq + Eq + Hash>(
        self,
        t: T,
        partial_cumulatives: &mut HashMap<T, PartialCumulative<HL>>,
    ) -> Option<Cumulative<HL, T>> {
        match partial_cumulatives.entry(t.clone()) {
            Entry::Occupied(mut entry) => {
                let partial_cumulative = entry.get_mut();
                self.into_partial(partial_cumulative);
                // if complete, remove from hashmap, build cumulative, and emit output
                if partial_cumulative.completed() {
                    let owned = entry.remove();
                    Some(owned.build(t))
                } else {
                    None
                }
            }
            Entry::Vacant(entry) => {
                let mut partial_cumulative = PartialCumulative::default();
                self.into_partial(&mut partial_cumulative);
                entry.insert(partial_cumulative);
                None
            }
        }
    }
}
impl<const HL: usize> UpdatePartial<HL> for KeyedCumulative<HL> {
    fn into_partial(self, partial_cumulative: &mut PartialCumulative<HL>) {
        partial_cumulative.keyed = Some(self);
    }
}
impl<const HL: usize> UpdatePartial<HL> for CountryCumulative<HL> {
    fn into_partial(self, partial_cumulative: &mut PartialCumulative<HL>) {
        partial_cumulative.country = Some(self);
    }
}
impl<const HL: usize> UpdatePartial<HL> for IspCumulative<HL> {
    fn into_partial(self, partial_cumulative: &mut PartialCumulative<HL>) {
        partial_cumulative.isp = Some(self);
    }
}
impl<const HL: usize> UpdatePartial<HL> for BaseUrlCumulative<HL> {
    fn into_partial(self, partial_cumulative: &mut PartialCumulative<HL>) {
        partial_cumulative.base_url = Some(self);
    }
}

#[derive(Debug, Default)]
struct PartialCumulative<const HL: usize> {
    keyed: Option<KeyedCumulative<HL>>,
    country: Option<CountryCumulative<HL>>,
    isp: Option<IspCumulative<HL>>,
    base_url: Option<BaseUrlCumulative<HL>>,
}

impl<const HL: usize> PartialCumulative<HL> {
    pub fn completed(&self) -> bool {
        match self {
            PartialCumulative {
                keyed: Some(_),
                country: Some(_),
                isp: Some(_),
                base_url: Some(_),
            } => true,
            _ => false,
        }
    }

    pub fn build<T>(self, t: T) -> Cumulative<HL, T> {
        let keyed_cum = self.keyed.expect("verified completed");
        let country_cum = self.country.expect("verified completed");
        let isp_cum = self.isp.expect("verified completed");
        let base_url_cum = self.base_url.expect("verified completed");
        Cumulative {
            features: t,
            country: country_cum.country,
            ip: country_cum.ip,
            fingerprint: country_cum.fingerprint,
            isp: isp_cum.isp,
            base_url: base_url_cum.base_url,
            key: keyed_cum.key,
            key_country: keyed_cum.key_country,
            key_isp: keyed_cum.key_isp,
            key_ip: keyed_cum.key_ip,
            key_user_agent: keyed_cum.key_user_agent,
            key_fingerprint: keyed_cum.key_fingerprint,
        }
    }
}

/// Cumulative decaying values
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Cumulative<const HL: usize, T> {
    pub features: T,
    pub country: DecayedValue<HL>,
    pub ip: DecayedValue<HL>,
    pub fingerprint: DecayedValue<HL>,
    pub isp: DecayedValue<HL>,
    pub base_url: DecayedValue<HL>,
    pub key: DecayedValue<HL>,
    pub key_country: DecayedValue<HL>,
    pub key_isp: DecayedValue<HL>,
    pub key_ip: DecayedValue<HL>,
    pub key_user_agent: DecayedValue<HL>,
    pub key_fingerprint: DecayedValue<HL>,
}

trait CumulativeCounting<const HL: usize, C> {
    type Key: Hash + Sized;

    /// Exchange hashing function to ensure that the relevant observations
    /// are processed by the same workers
    fn exchange_hash(&self) -> u64;

    // Back of the envelop calculations:
    // def cms_width(tolerance):
    //     x = int(2/tolerance)
    //     return 1<<(x-1).bit_length()
    // cms_width(0.01),cms_width(0.001), cms_width(0.0001)
    // (256, 2048, 32768)
    // def cms_depth(p):
    //     return np.log(1.0 - p) / np.log(0.5)
    // cms_depth(0.9),cms_depth(0.99),cms_depth(0.999)
    // (3.3219280948873626, 6.643856189774723, 9.965784284662085)
    /// Create Count-Min-Sketch datastructure, configured for Self
    fn create_cms() -> DecayedCMS<Self::Key, HL>;

    /// Count min sketch configuration as tuple (probability, tolerance)
    // fn cms_config() -> (f64, f64);

    /// Update the CMS with observation &self
    fn push(&self, cms: &mut DecayedCMS<Self::Key, HL>);

    /// Get the decaying values for a given time
    fn get(&self, as_of_time: f64, cms: &DecayedCMS<Self::Key, HL>) -> C;
}

impl<const HL: usize, K: Keyed + KeyedFields> CumulativeCounting<HL, KeyedCumulative<HL>> for K {
    // type Key = (<K as Keyed>::Key, String);
    type Key = u64;

    fn exchange_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.key().hash(&mut hasher);
        hasher.finish()
    }

    fn create_cms() -> DecayedCMS<Self::Key, HL> {
        let probability = 0.99;
        let tolerance = 0.000001;
        DecayedCMS(CountMinSketch::new(probability, tolerance, ()))
    }

    fn push(&self, cms: &mut DecayedCMS<Self::Key, HL>) {
        let mut key_hasher = DefaultHasher::new();
        self.key().hash(&mut key_hasher);
        // the key is the hash state now
        let geography = self.geography();
        let fingerprint = self.fingerprint();
        let time = self.relative_event_time();
        // push key
        cms.push_at(&key_hasher.clone().finish(), time);
        // push (key,country)
        let mut hasher = key_hasher.clone();
        geography.country.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push (key,isp)
        let mut hasher = key_hasher.clone();
        geography.isp.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push (key,ip)
        let mut hasher = key_hasher.clone();
        fingerprint.ip.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push (key,ua)
        let mut hasher = key_hasher.clone();
        fingerprint.user_agent.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push (key,fingerprint)
        let mut hasher = key_hasher.clone();
        fingerprint.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
    }

    fn get(&self, as_of_time: f64, cms: &DecayedCMS<Self::Key, HL>) -> KeyedCumulative<HL> {
        let mut key_hasher = DefaultHasher::new();
        self.key().hash(&mut key_hasher);
        // the key is the hash state now
        let geography = self.geography();
        let fingerprint = self.fingerprint();
        // key
        let key = key_hasher.clone().finish();
        // (key,country)
        let mut hasher = key_hasher.clone();
        geography.country.hash(&mut hasher);
        let key_country = hasher.finish();
        // (key,isp)
        let mut hasher = key_hasher.clone();
        geography.isp.hash(&mut hasher);
        let key_isp = hasher.finish();
        // (key,ip)
        let mut hasher = key_hasher.clone();
        fingerprint.ip.hash(&mut hasher);
        let key_ip = hasher.finish();
        // (key,ua)
        let mut hasher = key_hasher.clone();
        fingerprint.user_agent.hash(&mut hasher);
        let key_user_agent = hasher.finish();
        // (key,fingerprint)
        let mut hasher = key_hasher.clone();
        fingerprint.hash(&mut hasher);
        let key_fingerprint = hasher.finish();

        KeyedCumulative {
            key: cms.get_at(&key, as_of_time),
            key_country: cms.get_at(&key_country, as_of_time),
            key_isp: cms.get_at(&key_isp, as_of_time),
            key_ip: cms.get_at(&key_ip, as_of_time),
            key_user_agent: cms.get_at(&key_user_agent, as_of_time),
            key_fingerprint: cms.get_at(&key_fingerprint, as_of_time),
        }
    }
}

impl<const HL: usize, K: KeyedFields> CumulativeCounting<HL, CountryCumulative<HL>> for K {
    type Key = u64;

    fn exchange_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.geography().country.hash(&mut hasher);
        hasher.finish()
    }

    fn create_cms() -> DecayedCMS<Self::Key, HL> {
        let probability = 0.99;
        let tolerance = 0.00001;
        DecayedCMS(CountMinSketch::new(probability, tolerance, ()))
    }

    fn push(&self, cms: &mut DecayedCMS<Self::Key, HL>) {
        let geography = self.geography();
        let fingerprint = self.fingerprint();
        let time = self.relative_event_time();
        // push country
        let mut hasher = DefaultHasher::new();
        geography.country.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push ip
        let mut hasher = DefaultHasher::new();
        fingerprint.ip.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
        // push fingerprint
        let mut hasher = DefaultHasher::new();
        fingerprint.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
    }

    fn get(&self, as_of_time: f64, cms: &DecayedCMS<Self::Key, HL>) -> CountryCumulative<HL> {
        let geography = self.geography();
        let fingerprint = self.fingerprint();
        // push country
        let mut hasher = DefaultHasher::new();
        geography.country.hash(&mut hasher);
        let country = hasher.finish();
        // push ip
        let mut hasher = DefaultHasher::new();
        fingerprint.ip.hash(&mut hasher);
        let ip = hasher.finish();
        // push fingerprint
        let mut hasher = DefaultHasher::new();
        fingerprint.hash(&mut hasher);
        let fingerprint = hasher.finish();
        CountryCumulative {
            country: cms.get_at(&country, as_of_time),
            ip: cms.get_at(&ip, as_of_time),
            fingerprint: cms.get_at(&fingerprint, as_of_time),
        }
    }
}
impl<const HL: usize, K: KeyedFields> CumulativeCounting<HL, IspCumulative<HL>> for K {
    type Key = u64;

    fn exchange_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.geography().isp.hash(&mut hasher);
        hasher.finish()
    }
    // todo, small data, adjust config
    fn create_cms() -> DecayedCMS<Self::Key, HL> {
        let probability = 0.99;
        let tolerance = 0.00001;
        DecayedCMS(CountMinSketch::new(probability, tolerance, ()))
    }

    fn push(&self, cms: &mut DecayedCMS<Self::Key, HL>) {
        let geography = self.geography();
        let time = self.relative_event_time();
        // push isp
        let mut hasher = DefaultHasher::new();
        geography.isp.hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
    }

    fn get(&self, as_of_time: f64, cms: &DecayedCMS<Self::Key, HL>) -> IspCumulative<HL> {
        let geography = self.geography();
        // push country
        let mut hasher = DefaultHasher::new();
        geography.isp.hash(&mut hasher);
        let isp = hasher.finish();
        IspCumulative {
            isp: cms.get_at(&isp, as_of_time),
        }
    }
}
impl<const HL: usize, K: KeyedFields> CumulativeCounting<HL, BaseUrlCumulative<HL>> for K {
    type Key = u64;

    fn exchange_hash(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.base_url().hash(&mut hasher);
        hasher.finish()
    }

    // todo, small data, adjust config
    fn create_cms() -> DecayedCMS<Self::Key, HL> {
        let probability = 0.99;
        let tolerance = 0.00001;
        DecayedCMS(CountMinSketch::new(probability, tolerance, ()))
    }

    fn push(&self, cms: &mut DecayedCMS<Self::Key, HL>) {
        let time = self.relative_event_time();
        // push base url
        let mut hasher = DefaultHasher::new();
        self.base_url().hash(&mut hasher);
        cms.push_at(&hasher.clone().finish(), time);
    }

    fn get(&self, as_of_time: f64, cms: &DecayedCMS<Self::Key, HL>) -> BaseUrlCumulative<HL> {
        let mut hasher = DefaultHasher::new();
        self.base_url().hash(&mut hasher);
        let base_url = hasher.finish();
        BaseUrlCumulative {
            base_url: cms.get_at(&base_url, as_of_time),
        }
    }
}

pub trait PartialDecayed<const HL: usize, G: Scope, T, C> {
    // let dv = DecayedValue::new(value, relative_event_time);
    // cms.push(key, &dv);

    fn partial_decayed(
        &self,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<G, (T, C)>;
}

impl<const HL: usize, G, T: CumulativeCounting<HL, C>, C> PartialDecayed<HL, G, T, C>
    for Stream<G, T>
where
    G: Scope + ScopeParent<Timestamp = u64>,
    T: Debug + Eq + ExchangeData + Hash,
    C: Debug + Clone + 'static,
    // T::Key: Clone + Hash + 'static,
{
    fn partial_decayed(
        &self,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
    ) -> Stream<G, (T, C)> {
        let span = tracing::span!(tracing::Level::DEBUG, "partial_decayed");
        let _enter = span.enter();

        let emit_iterval_ms = emit_iterval.as_millis() as u64;

        let compute_workers = worker_selection.compute_worker_modulo();
        let pact = ExchangePact::new(move |t: &T| t.exchange_hash().rem_euclid(compute_workers));

        self.unary_frontier(pact, "partial decaying values", |_default_cap, _info| {
            // vec used to drain input batch
            let mut batch_vec = Vec::new();

            // count min sketch instances to count all the things
            let mut cms: DecayedCMS<T::Key, HL> = T::create_cms();

            let mut notificator: FrontierNotificator<u64> = FrontierNotificator::new();
            let mut to_emit = HashMap::new();
            // let mut to_emit = HashSet::new();

            move |input, output| {
                while let Some((time, data)) = input.next() {
                    // consume new batch of features
                    data.swap(&mut batch_vec);

                    // compute batch and add notification for end of batch
                    let batch_cap = worker_selection.shifted_batch(&time, emit_iterval_ms);
                    let batch_time = *batch_cap.time();

                    notificator.notify_at(batch_cap);

                    // insert new requests into cms
                    for t in batch_vec.drain(..) {
                        t.push(&mut cms);
                        // to_emit.insert(t);
                        to_emit
                            .entry(batch_time)
                            .or_insert(HashSet::new())
                            .insert(t);
                    }
                }
                // emit cumulative counts
                notificator.for_each(&[input.frontier()], |time, _notificator| {
                    if let Some(mut cumulative) = to_emit.remove(time.time()).map(|ts| {
                        ts.into_iter()
                            .map(|t| {
                                let as_of_time = *time.time() as f64;
                                let c = t.get(as_of_time, &cms);
                                (t, c)
                            })
                            .collect()
                    }) {
                        output.session(&time).give_container(&mut cumulative);
                    }
                    tracing::debug!(
                        target: "partial_decayed",
                        "@{} size of partial decayed hashmap for {} = {}",
                        time.time(),
                        std::any::type_name::<C>(),
                        to_emit.len()
                    );
                });
            }
        })
    }
}

pub trait Decayed<G: Scope, T: Keyed> {
    fn decayed<const HL: usize>(
        &self,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
        expire_interval: Option<Duration>,
    ) -> Stream<G, Cumulative<HL, T>>;
}

/// A dataflow operator to generate decaying counts for a stream of T.
/// Each incoming T is also emmited by this operator, along with a set of exponentially decaying
/// values (e.g. by country/ip/etc) configured by a half-life (e.g. with a
/// half-life of 10 minutes, 100 observations at time t are evalated as 50 at time t+10min).
/// The decaying counts are emitted at a configurable interval (e.g. 1 second)
impl<G, T> Decayed<G, T> for Stream<G, T>
where
    G: Scope + ScopeParent<Timestamp = u64>,
    T: Debug + Eq + ExchangeData + Hash + KeyedFields + Keyed,
    T::Key: Clone + Hash + 'static,
{
    fn decayed<const HL: usize>(
        &self,
        emit_iterval: Duration,
        worker_selection: WorkerSelection,
        expire_interval: Option<Duration>,
    ) -> Stream<G, Cumulative<HL, T>> {
        let span = tracing::span!(tracing::Level::DEBUG, "decayed");
        let _enter = span.enter();

        // TODO decide if it is worth hashing T only once and carry through tuples (u64,T)
        // Currently the hash is recomputed where needed
        // Another consideration: T is likely not unique (e.g. if it depends on the timestamp
        // of a webrequest, which is in seconds)- hence the expiry mechanism
        // alternatively, use a UUID for each request as identifier
        let hash_t = |t: &T| {
            let mut hasher = DefaultHasher::new();
            t.hash(&mut hasher);
            hasher.finish()
        };

        let keyed_cumulative: Stream<G, (T, KeyedCumulative<HL>)> =
            self.partial_decayed(emit_iterval, worker_selection);
        let country_cumulative: Stream<G, (T, CountryCumulative<HL>)> =
            self.partial_decayed(emit_iterval, worker_selection);
        let isp_cumulative: Stream<G, (T, IspCumulative<HL>)> =
            self.partial_decayed(emit_iterval, worker_selection);
        let base_url_cumulative: Stream<G, (T, BaseUrlCumulative<HL>)> =
            self.partial_decayed(emit_iterval, worker_selection);

        // constructing a custom operator to join all sub streams
        let mut builder = OperatorBuilder::new("join decaying values".to_owned(), self.scope());

        // registering the input sources. the order matters
        // frontier 0
        let mut keyed_input = builder.new_input(
            &keyed_cumulative,
            ExchangePact::new(move |(t, _): &(T, KeyedCumulative<HL>)| {
                hash_t(t).rem_euclid(worker_selection.compute_worker_modulo())
            }),
        );
        // frontier 1
        let mut country_input = builder.new_input(
            &country_cumulative,
            ExchangePact::new(move |(t, _): &(T, CountryCumulative<HL>)| {
                hash_t(t).rem_euclid(worker_selection.compute_worker_modulo())
            }),
        );
        // frontier 2
        let mut isp_input = builder.new_input(
            &isp_cumulative,
            ExchangePact::new(move |(t, _): &(T, IspCumulative<HL>)| {
                hash_t(t).rem_euclid(worker_selection.compute_worker_modulo())
            }),
        );
        // frontier 3
        let mut base_url_input = builder.new_input(
            &base_url_cumulative,
            ExchangePact::new(move |(t, _): &(T, BaseUrlCumulative<HL>)| {
                hash_t(t).rem_euclid(worker_selection.compute_worker_modulo())
            }),
        );
        // adding the output of cumulative, which creates the stream returned by the operator
        let (mut output, output_stream) = builder.new_output();

        builder.build(move |mut capabilities| {
            // notifier that cleans up the partials state, removing
            // capacity for the expriy notifier
            // let mut expire_cap: Option<Capability<u64>> = None;
            // let mut expire_cap = Some(capabilities.pop().unwrap());
            let expiry_time = 3 * { HL } as u64;
            let mut expire_partials: FrontierNotificator<u64> = FrontierNotificator::new();
            match expire_interval {
                Some(_) => {
                    let cap = capabilities.pop().unwrap();
                    expire_partials.notify_at(cap.delayed(&cap.time()));
                }
                None => (),
            }

            // keep track of partial cumulative counts that have been received
            let mut partial_cumulatives: HashMap<T, PartialCumulative<HL>> = HashMap::new();

            // vectors to drain input batches
            let mut keyed_vec = Vec::new();
            let mut country_vec = Vec::new();
            let mut isp_vec = Vec::new();
            let mut base_url_vec = Vec::new();

            move |frontiers| {
                let mut keyed_handle =
                    FrontieredInputHandleCore::new(&mut keyed_input, &frontiers[0]);
                let mut country_handle =
                    FrontieredInputHandleCore::new(&mut country_input, &frontiers[1]);
                let mut isp_handle = FrontieredInputHandleCore::new(&mut isp_input, &frontiers[2]);
                let mut base_url_handle =
                    FrontieredInputHandleCore::new(&mut base_url_input, &frontiers[3]);

                let mut output_handle = output.activate();

                // expire old unjoined values from state
                expire_partials.for_each(
                    &[&frontiers[0], &frontiers[1], &frontiers[2], &frontiers[3]],
                    |time, notificator| {
                        let before = partial_cumulatives.len();
                        partial_cumulatives.retain(|t, _partial| {
                            let age = time.time() - t.relative_event_time() as u64;
                            age < expiry_time
                        });
                        let after = partial_cumulatives.len();
                        if before > after {
                            tracing::debug!(target: "decayed", "{time:?} expire partials (from {before} to {after})",);
                        }
                        let expire_iterval_ms = expire_interval
                            .expect("only notified if defined")
                            .as_millis() as u64;

                        let new_time = time.time() + expire_iterval_ms;
                        notificator.notify_at(time.delayed(&new_time))
                    },
                );

                while let Some((time, data)) = keyed_handle.next() {
                    // consume new batch of features
                    data.swap(&mut keyed_vec);

                    // insert partial counts and emit when all partials are available
                    for (t, keyed_cumulative) in keyed_vec.drain(..) {
                        if let Some(cumulative) =
                            keyed_cumulative.update_partial_state(t, &mut partial_cumulatives)
                        {
                            output_handle.session(&time).give(cumulative)
                        }
                    }
                }

                while let Some((time, data)) = country_handle.next() {
                    // consume new batch of features
                    data.swap(&mut country_vec);

                    // insert partial counts into state hashmap
                    for (t, country_cumulative) in country_vec.drain(..) {
                        if let Some(cumulative) =
                            country_cumulative.update_partial_state(t, &mut partial_cumulatives)
                        {
                            output_handle.session(&time).give(cumulative)
                        }
                    }
                }
                while let Some((time, data)) = isp_handle.next() {
                    // consume new batch of features
                    data.swap(&mut isp_vec);

                    // insert partial counts into state hashmap
                    for (t, isp_cumulative) in isp_vec.drain(..) {
                        if let Some(cumulative) =
                            isp_cumulative.update_partial_state(t, &mut partial_cumulatives)
                        {
                            output_handle.session(&time).give(cumulative)
                        }
                    }
                }
                while let Some((time, data)) = base_url_handle.next() {
                    // consume new batch of features
                    data.swap(&mut base_url_vec);

                    // insert partial counts into state hashmap
                    for (t, base_url_cumulative) in base_url_vec.drain(..) {
                        if let Some(cumulative) =
                            base_url_cumulative.update_partial_state(t, &mut partial_cumulatives)
                        {
                            output_handle.session(&time).give(cumulative)
                        }
                    }
                }
            }
        });
        output_stream
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use domain::api::{BaseUrl, Fingerprint, Geography};
    use serde::{Deserialize, Serialize};
    use timely::{
        dataflow::{
            operators::{Capture, Map as _, Probe as _, UnorderedInput as _},
            ProbeHandle,
        },
        Config,
    };

    pub const HALF_LIFE: usize = 100;

    #[test]
    fn decayed() {
        #[derive(Clone, Debug, Hash, Serialize, Deserialize, Ord, PartialEq, PartialOrd, Eq)]
        struct Data {
            id: String,
            fingerprint: Fingerprint,
            geography: Geography,
            time: u64,
        }
        impl Data {
            pub fn new(encoded: &str) -> Self {
                match encoded.split(',').collect::<Vec<_>>()[..] {
                    [id, time_str] => Data {
                        id: id.to_owned(),
                        fingerprint: Fingerprint {
                            ip: format!("ip_{}", id),
                            user_agent: format!("ua_{}", id),
                            accept_language: format!("al_{}", id),
                        },
                        geography: Geography {
                            country: format!("country_{}", id),
                            wmf_region: format!("region_{}", id),
                            isp: format!("isp_{}", id),
                        },
                        time: time_str.parse().expect("invalid value"),
                    },
                    _ => panic!("entry data {encoded}"),
                }
            }
        }
        impl Keyed for Data {
            type Key = String;

            fn key(&self) -> &Self::Key {
                &self.id
            }
        }
        impl KeyedFields for Data {
            fn fingerprint(&self) -> &Fingerprint {
                &self.fingerprint
            }

            fn geography(&self) -> &Geography {
                &self.geography
            }

            fn base_url(&self) -> BaseUrl {
                BaseUrl {
                    host: format!("host_{}", self.key()),
                    path: format!("path_{}", self.key()),
                }
            }

            fn relative_event_time(&self) -> f64 {
                self.time as f64
            }
        }

        let data: Vec<Data> = vec![
            "a,0", "a,50", "b,50", "c,99", "b,150", "a,150", "a,180", "a,220",
        ]
        .into_iter()
        .map(Data::new)
        .collect();
        // let data: Vec<Data> = vec!["a,0"].into_iter().map(Data::new).collect();

        // let expected = vec![
        // (
        //     100,
        //     vec![("g1", vec!["k3", "k2"]), ("g2", vec!["k1", "k3"])],
        // ),
        // (
        //     200,
        //     vec![("g1", vec!["k1", "k3"]), ("g2", vec!["k2", "k3"])],
        // ),
        // ];
        timely::execute(Config::process(1), move |worker| {
            let mut probe = ProbeHandle::new();
            let worker_selection = WorkerSelection::new(worker, 0);

            // create a new input, exchange data, and inspect its output
            let ((mut input, mut cap), receiver) = worker.dataflow::<u64, _, _>(|scope| {
                let (input, stream) = scope.new_unordered_input();
                let receiver = stream
                    .decayed::<{ HALF_LIFE }>(Duration::from_millis(100), worker_selection, None)
                    .probe_with(&mut probe)
                    .capture();

                (input, receiver)
            });
            if worker_selection.worker_index == 0 {
                // start at 0
                cap.downgrade(&0);
                // insert data
                for event in data.clone() {
                    let time = event.time;
                    input.session(cap.delayed(&time)).give(event);
                    cap.downgrade(&time);

                    while probe.less_than(&time) {
                        worker.step();
                    }
                }
            }
            cap.downgrade(&u64::MAX);
            while probe.less_than(&u64::MAX) {
                worker.step();
            }

            //TODO running .extract() on the .capture()'ed stream does not complete, presumably because the
            //input doesn't close/get dropped. Instead, poor man's approach to collecting into vec
            let mut result = vec![];
            while let Ok(recv) = receiver.recv_timeout(Duration::from_millis(500)) {
                if let timely::dataflow::operators::capture::Event::Messages(t, mut d) = recv {
                    result.push((t, d));
                }
            }
            // assert_eq!(result, expected);
            println!("{result:?}");
        })
        .expect("dataflow test failed");
    }
}
