use domain::api::{BaseUrl, Fingerprint, Geography, Page};
use serde::{Deserialize, Serialize};
use std::hash::Hash;
use time::OffsetDateTime;
use timely::dataflow::{operators::Map, Scope, Stream};

use crate::{
    cumulative::{Keyed, KeyedFields},
    webrequest::WebRequest,
};

/// Information about a pageview.
/// This is not a domain struct
#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Pageview {
    pub page: Page,
    pub fingerprint: Fingerprint,
    pub geography: Geography,
    // TODO make a struct Timestamp and make this a domain struct?
    pub relative_event_time: i128,
}

impl KeyedFields for Pageview {
    fn fingerprint(&self) -> &Fingerprint {
        &self.fingerprint
    }

    fn geography(&self) -> &Geography {
        &self.geography
    }

    fn relative_event_time(&self) -> f64 {
        self.relative_event_time as f64
    }

    fn base_url(&self) -> BaseUrl {
        self.page.base_url()
    }
}

impl Keyed for Pageview {
    type Key = Page;
    fn key(&self) -> &Page {
        &self.page
    }
}

pub trait ExtractPageview<S: Scope> {
    /// Extract
    fn extract_pageview(&self, event_time_zero: OffsetDateTime) -> Stream<S, Pageview>;
}

impl<S: Scope> ExtractPageview<S> for Stream<S, WebRequest> {
    fn extract_pageview(&self, event_time_zero: OffsetDateTime) -> Stream<S, Pageview> {
        // let ua_parser = UserAgentParser::from_yaml("resources/regexes.yaml").unwrap();
        // TODO add cache to avoid regex parsing, maybe LRU
        // let ua_cache: HashMap<&str, bool> = HashMap::new();

        self.flat_map(move |req| {
            // key
            let normalized_host = req.normalized_host()?;
            let project = normalized_host.project?;
            let family = normalized_host.project_family;

            let title = req.page_title()?;
            let _page_id = req.page_id().unwrap_or({
                // The page_id is not present e.g. the WikipediaApp, for File:xyz pages, for Special:xyz
                // tracing::error!("{page_title}: no page_id for request {req:?}",);
                0
            });
            let page = Page {
                title,
                project,
                family,
            };

            // filter the ua string
            // TODO instead of filter, add agent_type
            // let ua = Some(req.user_agent.clone())
            //     .filter(|ua| ua != "-")
            //     // TODO, this is expensive to do for all webrequests
            //     .filter(|ua| {
            //         let device = ua_parser.parse_device(ua);
            //         // let ua = ua_parser.parse_user_agent(ua);
            //         // ua.
            //         device.family != "Spider"
            //     })?;
            let _ua = Some(req.user_agent.clone()).filter(|ua| ua != "-")?;

            // construct the time of the webreqest
            let since_event_time_zero = req.datetime().ok()? - event_time_zero;
            let relative_event_time = since_event_time_zero.whole_milliseconds();

            Some(Pageview {
                page,
                fingerprint: req.fingerprint().clone(),
                geography: req.geography().clone(),
                relative_event_time,
            })
        })
    }
}
