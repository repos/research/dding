//! Data structures for using exponentially decaying values

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::Debug;
use std::hash::Hash;
use std::ops::AddAssign;

/// espsilon pull small values to 0
const EPSILON: f64 = 0.1f64;

/// A 10 min half-life in miliseconds.
/// This is used as a const generic, ie. the
/// program is compiled with a specific value
pub const HALF_LIFE_10_MIN: usize = 1000 * 60 * 10;
pub const HALF_LIFE_30_MIN: usize = 1000 * 60 * 30;
pub const HALF_LIFE_60_MIN: usize = 1000 * 60 * 60;

pub type DecayedValueDefault = DecayedValue<HALF_LIFE_10_MIN>;

/// A struct that represents a value seen at specific time which decays
/// exponentially with time. The halflife of the decay is represented
/// a const generic, which ensures that operations involving two
/// decaying values operate with the same decay paremeter.
/// based on https://github.com/twitter/algebird/blob/develop/algebird-core/src/main/scala/com/twitter/algebird/DecayedValue.scala#L31
#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq)]
pub struct DecayedValue<const HL: usize> {
    pub value: f64,
    pub scaled_time: f64,
}

impl<const HL: usize> DecayedValue<HL> {
    const HALF_LIFE: f64 = HL as f64;

    pub fn new(value: f64, time: f64) -> Self {
        let scaled_time = time * 2f64.ln() / Self::HALF_LIFE;
        DecayedValue { value, scaled_time }
    }

    pub fn to_domain(&self) -> domain::counting::DecayedValue {
        domain::counting::DecayedValue {
            value: self.value,
            scaled_time: self.scaled_time,
            half_life: Self::HALF_LIFE,
        }
    }

    pub fn zero() -> Self {
        DecayedValue::new(0f64, f64::MIN)
    }
    pub fn is_zero(&self) -> bool {
        self.value < EPSILON
    }

    pub fn time(&self) -> f64 {
        self.scaled_time / 2f64.ln() * Self::HALF_LIFE
    }

    /// Update DecayedValue to new time.
    /// This operation is done using a 0 value observation
    /// at the specified `time`, if the provided time is before
    /// the current time of the decayed value, this is a no-op.
    pub fn update_as_of(&mut self, time: f64) {
        *self += DecayedValue::new(0f64, time);
    }

    /// Compute the would-be value as of `time`. Note that if `time`
    /// is prior to the time of the decayed value, the returned
    /// value is not "backed" by an observations. I.e. for an exponentially
    /// decaying value with a given value/time, what would be the value
    /// at `time`?
    pub fn value_as_of(&self, time: f64) -> f64 {
        let scaled_as_of_time = time * 2f64.ln() / Self::HALF_LIFE;
        (self.scaled_time - scaled_as_of_time).exp() * self.value
    }

    pub fn average(&self) -> f64 {
        let normalization = Self::HALF_LIFE / 2f64.ln();
        self.value / normalization
    }

    pub fn average_interval(&self, start_time: f64, end_time: f64) -> f64 {
        assert!(start_time < end_time, "start after end");
        let value = self.value_as_of(end_time);
        let time_delta = start_time - end_time;
        let normalization =
            Self::HALF_LIFE * (1f64 - 2f64.powf(time_delta / Self::HALF_LIFE)) / 2f64.ln();
        value / normalization
    }
}

/// plus operation between for two owned decayed values
impl<const HL: usize> AddAssign for DecayedValue<HL> {
    fn add_assign(&mut self, rhs: Self) {
        self.add_assign(&rhs);
    }
}

/// plus operation between for two decayed values
/// the time is set to the more recent of the two,
/// and the updated value is the sum of the more recent
/// value plus the older value decayed for the duration
/// the delta between the times.
impl<'a, const HL: usize> AddAssign<&'a DecayedValue<HL>> for DecayedValue<HL> {
    fn add_assign(&mut self, rhs: &'a Self) {
        if self.scaled_time < rhs.scaled_time {
            //  self is older,
            self.value = rhs.value + (self.scaled_time - rhs.scaled_time).exp() * self.value;
            self.scaled_time = rhs.scaled_time;
        } else {
            //  right is older
            self.value = self.value + (rhs.scaled_time - self.scaled_time).exp() * rhs.value;
        }
    }
}

impl<const HL: usize> PartialOrd for DecayedValue<HL> {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.value.partial_cmp(&other.value_as_of(self.time()))
    }
}

/// The "zero" of the DecayedValue used by e.g. count-min-sketch
impl<const HL: usize> streaming_algorithms::New for DecayedValue<HL> {
    type Config = ();

    fn new(_config: &Self::Config) -> Self {
        DecayedValue::zero()
    }
}

impl<'a, const HL: usize> streaming_algorithms::UnionAssign<&'a DecayedValue<HL>>
    for DecayedValue<HL>
{
    fn union_assign(&mut self, rhs: &'a Self) {
        if *self < *rhs {
            *self = *rhs;
        };
    }
}
impl<const HL: usize> streaming_algorithms::Intersect for DecayedValue<HL> {
    fn intersect<'a>(iter: impl Iterator<Item = &'a Self>) -> Option<Self>
    where
        Self: Sized + 'a,
    {
        let out = iter.min_by(|x, y| x.partial_cmp(y).unwrap());
        out.map(|dv| dv.to_owned())
    }
}

impl<const HL: usize> streaming_algorithms::IntersectPlusUnionIsPlus for DecayedValue<HL> {
    const VAL: bool = false;
}

/// TopK data structure for decaying counts
/// The elements inserted in TopK need to be aggreated already, e.g.
/// an updated DecayedValue for a given T computed using a count-min-sketch.
/// As such, an updated value needs to be strictly larger than the previously inserted value,
/// which is a natural assumption for decaying values.
/// TODO, at first I tried implementing this with a BinaryHeap and struggled, but I don't remember why.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DecayedTopK<T: Eq + Hash + Clone, const HL: usize> {
    k: usize,
    min: Option<(T, DecayedValue<HL>)>,
    counts: HashMap<T, DecayedValue<HL>>,
}

impl<T: Debug + Eq + Hash + Clone, const HL: usize> DecayedTopK<T, HL> {
    pub fn new(k: usize) -> Self {
        DecayedTopK {
            k,
            min: None,
            counts: HashMap::new(),
        }
    }

    /// returns un unsorted iterator of the current counts
    pub fn elements(&self) -> impl Iterator<Item = &T> {
        self.counts.keys()
    }

    /// checks whether an element is in the TopK
    pub fn contains(&self, t: &T) -> bool {
        self.counts.contains_key(t)
    }

    /// checks whether the top k is empty
    pub fn is_empty(&self) -> bool {
        self.counts.is_empty()
    }

    /// Insert an observed item with it's associated decayed value
    /// Returns a bool indicating whether the topk has changed, and
    /// the ejected element and value, if any.
    pub fn insert(&mut self, t: T, dv: DecayedValue<HL>) -> (bool, Option<(T, DecayedValue<HL>)>) {
        let mut ejected = None;
        let mut updated = true;
        if let Some((min_t, min_dv)) = &self.min {
            let mut update_min = false;
            if self.counts.len() < self.k {
                self.counts.insert(t, dv);
                update_min = true;
            } else if let Some(old_dv) = self.counts.get(&t) {
                if dv > *old_dv {
                    // possibly the min changes
                    if *min_t == t {
                        update_min = true;
                    }
                    // if the page is already observed, the new count is
                    // higher than the existing one
                    self.counts.insert(t, dv);
                } else if dv < *old_dv {
                    // tracing::error!(
                    //     "For {t:?}, attempting to insert {dv:?}, but {old_dv:?} is greater."
                    // );
                }
            } else if dv > *min_dv {
                // if observed value is larger than current min, a new key is inserted
                // and the current min discarded
                self.counts.insert(t, dv);
                // current min is removed
                self.counts.remove(min_t);
                ejected = Some((min_t.clone(), *min_dv));
                update_min = true;
            } else {
                // key is not in topk and too small to be inserted
                updated = false;
            }
            if update_min {
                // topk has changed, update current min
                self.update_min();
            }
        } else {
            // topK is empty
            self.min = Some((t.clone(), dv));
            self.counts.insert(t, dv);
        }
        (updated, ejected)
    }

    fn update_min(&mut self) {
        let new_min = self.counts.iter().min_by(|(_, l), (_, r)| {
            //TODO, unwrap....
            l.partial_cmp(r).unwrap()
        });
        self.min = new_min.map(|(new_min_t, v)| (new_min_t.to_owned(), v.to_owned()));
    }
    /// Remove all counts that have decayed to ~zero
    pub fn sanitize(&mut self, as_of_time: f64) {
        let before = self.counts.len();
        self.counts.retain(|_, dv: &mut DecayedValue<HL>| {
            let val = dv.value_as_of(as_of_time);
            val > EPSILON
        });
        let after = self.counts.len();
        if after < before {
            self.update_min();
        }
    }
    /// Returns the top k entries as sorted vector
    pub fn to_domain(&self) -> domain::counting::DecayedTopK<T>
    where
        T: Debug + Serialize + for<'a> Deserialize<'a>,
    {
        let mut topk: Vec<(T, DecayedValue<HL>)> = self
            .counts
            .iter()
            .map(|(k, v)| (k.to_owned(), v.to_owned()))
            .collect();

        topk.sort_unstable_by(|(_, l), (_, r)| l.partial_cmp(r).unwrap().reverse());
        let topk = topk.drain(..).map(|(t, dv)| (t, dv.to_domain())).collect();
        domain::counting::DecayedTopK { topk }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    pub const HALF_LIFE: usize = 100;

    #[test]
    fn simple_decay() {
        let dv: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 100f64);
        assert_eq!(dv.value_as_of(0f64), 20f64);
        assert_eq!(dv.value_as_of(200f64), 5f64);
    }

    #[test]
    fn add_assign() {
        // update older
        let mut dv1: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 0f64);
        let dv2: DecayedValue<HALF_LIFE> = DecayedValue::new(5f64, 100f64);
        dv1 += dv2;
        assert_eq!(dv1.value, 10f64);
        assert_eq!(dv1.time(), 100f64);

        // update newer
        let dv1: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 0f64);
        let mut dv2: DecayedValue<HALF_LIFE> = DecayedValue::new(5f64, 100f64);
        dv2 += dv1;

        assert_eq!(dv2.value, 10f64);
        assert_eq!(dv2.time(), 100f64);
    }

    #[test]
    fn update_as_of() {
        // properly update in the future
        let mut dv: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 0f64);
        dv.update_as_of(100f64);
        assert_eq!(dv.time(), 100f64);
        assert_eq!(dv.value, 5f64);
        assert_eq!(dv.value_as_of(0f64), 10f64);

        // no-op when updating in the past
        let mut dv: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 100f64);
        dv.update_as_of(0f64);
        assert_eq!(dv.time(), 100f64);
        assert_eq!(dv.value, 10f64);
        assert_eq!(dv.value_as_of(100f64), 10f64);
    }

    #[test]
    fn average() {
        let mut dv: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 0f64);
        for i in (0..1000).step_by(100) {
            let i = i as f64;
            dv.update_as_of(i);
        }
        // TODO add assert almost equal
    }

    #[test]
    fn topk() {
        let mut topk: DecayedTopK<&str, 100> = DecayedTopK::new(3);

        // insert elements
        let dv1: DecayedValue<HALF_LIFE> = DecayedValue::new(10f64, 100f64);
        topk.insert("k1", dv1);
        assert_eq!(topk.min.unwrap().0, "k1");
        assert_eq!(topk.counts.len(), 1);

        let dv2: DecayedValue<HALF_LIFE> = DecayedValue::new(4f64, 200f64);
        topk.insert("k2", dv2);
        assert_eq!(topk.min.unwrap().0, "k2");
        assert_eq!(topk.counts.len(), 2);

        let dv3: DecayedValue<HALF_LIFE> = DecayedValue::new(6f64, 200f64);
        topk.insert("k3", dv3);
        assert_eq!(topk.min.unwrap().0, "k2");
        assert_eq!(topk.counts.len(), 3);

        // no change, dv too small
        let dv4: DecayedValue<HALF_LIFE> = DecayedValue::new(1f64, 300f64);
        topk.insert("k4", dv4);
        assert_eq!(topk.min.unwrap().0, "k2");
        assert_eq!(topk.counts.len(), 3);

        // insert new element, update min
        let dv5: DecayedValue<HALF_LIFE> = DecayedValue::new(4f64, 300f64);
        topk.insert("k5", dv5);
        assert_eq!(topk.min.unwrap().0, "k1");
        let mut keys = topk.counts.clone().into_keys().collect::<Vec<&str>>();
        keys.sort_unstable();
        assert_eq!(keys, vec!["k1", "k3", "k5"]);

        // update to existing key, updated min
        let dv1_1: DecayedValue<HALF_LIFE> = DecayedValue::new(5f64, 300f64);
        topk.insert("k1", dv1_1);
        assert_eq!(topk.min.unwrap().0, "k3");
        let mut keys = topk.counts.clone().into_keys().collect::<Vec<&str>>();
        keys.sort_unstable();
        assert_eq!(keys, vec!["k1", "k3", "k5"]);

        // re-insert key, updated min
        let dv2_1: DecayedValue<HALF_LIFE> = DecayedValue::new(3.1f64, 300f64);
        topk.insert("k2", dv2_1);
        assert_eq!(topk.min.unwrap().0, "k2");
        let mut keys = topk.counts.clone().into_keys().collect::<Vec<&str>>();
        keys.sort_unstable();
        assert_eq!(keys, vec!["k1", "k2", "k5"]);
    }
}
