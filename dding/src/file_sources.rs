//! file based sources
//!
use anyhow::{Context, Result};
use apache_avro::from_value;
use apache_avro::Reader;
use parquet::file::reader::SerializedFileReader;
use parquet::record::Row;
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use std::fs::File;

/// struct based on the avro source for wikitext history
/// https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Mediawiki_wikitext_history
#[derive(Debug, Deserialize)]
pub struct WikitextHistory {
    wiki_db: String,
    page_id: i32,
    page_namespace: i32,
    page_title: String,
    page_redirect_title: String,
    page_restrictions: Vec<String>,
    user_id: i32,
    user_text: String,
    revision_id: i32,
    revision_parent_id: i32,
    revision_timestamp: String,
    revision_minor_edit: bool,
    revision_comment: String,
    revision_text_bytes: i32,
    revision_text_sha1: String,
    revision_text: String,
    revision_content_model: String,
    revision_content_format: String,
}

pub fn wikitext_history(file_name: &str) -> Result<impl Iterator<Item = WikitextHistory>> {
    let file = File::open(file_name)?;
    let reader = Reader::new(file)?;

    // schema is not set for wmf data?
    // let schema = reader.reader_schema().unwrap();

    // filter out deserialization error
    let iter = reader.into_iter().flat_map(|res| {
        res.and_then(|v| from_value::<WikitextHistory>(&v))
            .map_err(|err| tracing::warn!("Discarding avro value; error deserializing {:?}. ", err))
    });
    Ok(iter)
}

// TODO the derived json schema is experimental
//     https://schema.wikimedia.org/#!//primary/jsonschema/mediawiki
//     https://gerrit.wikimedia.org/r/plugins/gitiles/schemas/event/primary/+/refs/heads/master
// TODO could do something like https://gitlab.wikimedia.org/repos/data-engineering/mediawiki-event-enrichment/-/tree/main#schema-repositories
// derive structs for JsonSchema specifications
schemafy::schemafy!(root: MediawikiRequest "schemas/mediawiki_api_request.json");
schemafy::schemafy!(root: PageLinkChange "schemas/page_link_change.json");

impl MediawikiRequest {
    /// Attempt to parse a MediawikiRequest from a json byte array, extracted
    /// from a kafka message
    pub fn from_json(bytes: &[u8]) -> Result<MediawikiRequest> {
        serde_json::from_slice::<MediawikiRequest>(bytes)
            .context("Failed to deserialize MediawikiRequest")
    }

    /// Attempt to parse a MediawikiRequest from a parquet row
    /// The struct is derrived using the JsonSchema that is used
    /// for kafka events. These events are also stored on hdfs as
    /// parquet files. This function converts a parquet row into a
    /// json Value, which is then parsed into the struct
    pub fn from_parquet_row(row: Row) -> Result<MediawikiRequest> {
        let mut val = row.to_json_value();
        // insert the $schema key that is required by the JsonSchema
        let object = val.as_object_mut().unwrap();
        object.insert(
            "$schema".to_owned(),
            serde_json::Value::String("https://json-schema.org/draft-07/schema#".to_owned()),
        );
        serde_json::from_value::<MediawikiRequest>(val)
            .context("Failed to deserialize parquet encoded MediawikiRequest")
    }
}

pub fn mediawiki_api_request(file_name: &str) -> Result<impl Iterator<Item = MediawikiRequest>> {
    let iter = SerializedFileReader::try_from(file_name).map(|r| r.into_iter())?;

    // filter out deserialization error
    let reqs_iter = iter.flat_map(|row| {
        MediawikiRequest::from_parquet_row(row).map_err(|err| {
            tracing::warn!("Discarding parquet row; error deserializing {:?}. ", err)
        })
    });

    Ok(reqs_iter)
}

// pub fn parquet(file_name: &str) -> Result<(), DdingError> {
//     let file = File::open(file_name)?;

//     let mut arrow_reader = ParquetFileArrowReader::try_new(file)?;

//     let schema = arrow_reader.get_schema()?;
//     for f in schema.fields {
//         println!("{}",f)
//     }

//     let batch_size = 1;

//     // for now, all columns are read
//     // let mut record_batch_reader = arrow_reader.get_record_reader(batch_size)?;

//     // TODO, if desired/needed we could project the desired columns to read
//     let mask = ProjectionMask::roots(arrow_reader.parquet_schema(), [2,3]);
//     let mut record_batch_reader = arrow_reader
//         .get_record_reader_by_columns(mask, batch_size)?;

//     // let schema = record_batch_reader()?;
//     // for f in schema.fields {
//     //     println!("{}",f)
//     // }

//     for maybe_record_batch in record_batch_reader.take(1) {
//         let record_batch = maybe_record_batch.unwrap();

//         let _ = event_structs::MediawikiRequest::from_record_batch(&record_batch);

//         // let x = record_batch.columns();
//         // println!("{:?}", x);
//         // let y = x.iter().map(|r| {
//         //     println!("{:?}",r);
//         // }).take(1).collect::<Vec<_>>();
//     }
//     Ok(())
// }
