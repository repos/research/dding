use crate::db::ToRedisKey;
use axum::{
    body::{boxed, Body},
    extract::{Extension, Query},
    http::{Method, Response, StatusCode},
    routing::get,
    Json, Router,
};
use domain::api::{Country, Fingerprint, Page, Pageviews, PageviewsSeries, TopK, Wiki};
use redis::cluster::ClusterClient;
use serde::Deserialize;
use std::fmt::Debug;
use std::{collections::HashMap, net::SocketAddr, path::PathBuf, sync::Arc, time::Instant};
use time::OffsetDateTime;
use tokio::fs;
use tower::ServiceExt;
use tower_http::{
    cors::{Any, CorsLayer},
    services::ServeDir,
};

#[derive(Clone)]
struct ApiState {
    db: Arc<ClusterClient>,
    time_zero: Instant,
    event_time_zero: OffsetDateTime,
}

pub async fn start(
    db: Arc<ClusterClient>,
    time_zero: Instant,
    event_time_zero: OffsetDateTime,
    port: u16,
) {
    let cors = CorsLayer::new()
        // allow `GET` and `POST` when accessing the resource
        .allow_methods([Method::GET, Method::POST])
        // allow requests from any origin
        .allow_origin(Any);

    let shared_state = Arc::new(ApiState {
        db,
        time_zero,
        event_time_zero,
    });

    let app: Router = Router::new()
        .route("/wiki", get(topk::<Wiki, Page>))
        .route("/country", get(topk::<Country, Page>))
        .route("/fingerprint", get(topk::<Page, Fingerprint>))
        .route("/pageviews/dv", get(decaying_pageview))
        .route("/pageviews/series", get(pageview_timeseries))
        .layer(Extension(shared_state))
        .layer(cors)
        .fallback_service(get(|req| async move {
            match ServeDir::new("dist").oneshot(req).await {
                Ok(res) => {
                    let status = res.status();
                    match status {
                        StatusCode::NOT_FOUND => {
                            let index_path = PathBuf::from("dist").join("index.html");
                            let index_content = match fs::read_to_string(index_path).await {
                                Err(_) => {
                                    return Response::builder()
                                        .status(StatusCode::NOT_FOUND)
                                        .body(boxed(Body::from("index file not found")))
                                        .unwrap()
                                }
                                Ok(index_content) => index_content,
                            };

                            Response::builder()
                                .status(StatusCode::OK)
                                .body(boxed(Body::from(index_content)))
                                .unwrap()
                        }
                        _ => res.map(boxed),
                    }
                }
                Err(err) => Response::builder()
                    .status(StatusCode::INTERNAL_SERVER_ERROR)
                    .body(boxed(Body::from(format!("error: {err}"))))
                    .expect("error response"),
            }
        }));

    // run our app with hyper
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("api server failed");
}

/// generic handler to a top_k instance from redis, and
/// returned a list evaluated at the current moment
async fn topk<
    K: Debug + ToRedisKey + for<'a> Deserialize<'a>,
    V: Debug + for<'a> Deserialize<'a>,
>(
    Query(key): Query<K>,
    Extension(state): Extension<Arc<ApiState>>,
) -> Result<Json<TopK<V>>, (StatusCode, String)> {
    let namespace = match std::any::type_name::<K>() {
        "domain::api::Wiki" => "topk:page",
        "domain::api::Country" => "topk:page",
        "domain::api::Page" => "topk:fingerprint",
        e => {
            // hackery
            return Err((StatusCode::OK, format!("Error {}", e)));
            // return Err(anyhow::Error::msg(format!("unkown type {e}")))
        }
    };

    let topk = state
        .db
        .get_connection()
        .map_err(anyhow::Error::msg)
        .and_then(|mut con| {
            tracing::info!("topk for key {:?}", &key);
            let current_instant = Instant::now();
            let evaluate_at_timestamp =
                current_instant.duration_since(state.time_zero).as_millis() as f64;
            crate::db::get_topk(&mut con, &key, namespace, evaluate_at_timestamp)
        });

    match topk {
        Ok(topk) => Ok(Json(topk)),
        Err(e) => Err((StatusCode::OK, format!("Error {}", e))),
    }
}

async fn decaying_pageview(
    Query(key): Query<Page>,
    Extension(state): Extension<Arc<ApiState>>,
) -> Result<Json<Pageviews>, (StatusCode, String)> {
    let pageview: Result<Pageviews, anyhow::Error> = state
        .db
        .get_connection()
        .map_err(anyhow::Error::msg)
        .and_then(|mut con| {
            tracing::info!("pageviews for key {:?}", &key);
            let dv = crate::db::get_decayed_pageviews(&mut con, &key)?;
            let current_instant = Instant::now();
            let evaluate_at_timestamp =
                current_instant.duration_since(state.time_zero).as_millis() as f64;
            let value = dv.value_as_of(evaluate_at_timestamp);
            Ok(Pageviews {
                page: key,
                count: value,
            })
        });

    match pageview {
        Ok(pageviews) => Ok(Json(pageviews)),
        Err(e) => Err((StatusCode::OK, format!("Error {}", e))),
    }
}

async fn pageview_timeseries(
    Query(key): Query<Page>,
    Extension(state): Extension<Arc<ApiState>>,
) -> Result<Json<PageviewsSeries>, (StatusCode, String)> {
    let pageview: Result<PageviewsSeries, anyhow::Error> = state
        .db
        .get_connection()
        .map_err(anyhow::Error::msg)
        .and_then(|mut con| {
            tracing::info!("pageviews series for key {:?}", &key);
            let series = crate::db::get_pageviews_series(&mut con, &key)?;
            Ok(PageviewsSeries { page: key, series })
        });

    match pageview {
        Ok(pageviews) => Ok(Json(pageviews)),
        Err(e) => Err((StatusCode::OK, format!("Error {}", e))),
    }
}
