use crate::cumulative::{Keyed, KeyedFields};
use crate::webrequest::WebRequest;
use domain::api::{BaseUrl, Fingerprint, Geography, Url};
use serde::{Deserialize, Serialize};
use std::hash::Hash;
use time::OffsetDateTime;
use timely::dataflow::{operators::Map, Scope, Stream};

#[derive(Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct CacheInfo {
    pub cache_status: String,
    pub url: Url,
    pub fingerprint: Fingerprint,
    pub geography: Geography,
    pub relative_event_time: i128,
}

impl KeyedFields for CacheInfo {
    fn fingerprint(&self) -> &Fingerprint {
        &self.fingerprint
    }

    fn geography(&self) -> &Geography {
        &self.geography
    }

    fn relative_event_time(&self) -> f64 {
        self.relative_event_time as f64
    }

    fn base_url(&self) -> BaseUrl {
        self.url.base()
    }
}

// Use the uri as key
impl Keyed for CacheInfo {
    type Key = Url;
    fn key(&self) -> &Url {
        &self.url
    }
}

pub trait ExtractCacheInfo<S: Scope> {
    /// Extract
    fn extract_cache_info(&self, event_time_zero: OffsetDateTime) -> Stream<S, CacheInfo>;
}

impl<S: Scope> ExtractCacheInfo<S> for Stream<S, WebRequest> {
    fn extract_cache_info(&self, event_time_zero: OffsetDateTime) -> Stream<S, CacheInfo> {
        self.flat_map(move |req| {
            let uri = Url {
                host: req.uri_host.clone(),
                path: req.uri_path.clone(),
                query: req.uri_query.clone(),
            };

            // construct the time of the webreqest
            let since_event_time_zero = req.datetime().ok()? - event_time_zero;
            let relative_event_time = since_event_time_zero.whole_milliseconds();

            Some(CacheInfo {
                cache_status: req.cache_status.clone(),
                url: uri,
                fingerprint: req.fingerprint().clone(),
                geography: req.geography().clone(),
                relative_event_time,
            })
        })
    }
}
