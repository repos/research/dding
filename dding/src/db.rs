//! A key/value store is used to persist data
use crate::{
    decay::{DecayedTopK, DecayedValue},
    trending::{Event, TrendEvent},
};
use anyhow::{Context as _, Result};
use domain::{
    api::{BaseUrl, Country, Fingerprint, Ip, Isp, Page, TopK, Trend, Url, Wiki},
    counting::{DecayedTopK as DecayedTopKDomain, DecayedValue as DecayedValueDomain},
};
use redis::{
    cluster::{ClusterClient, ClusterConnection},
    Commands,
};
use serde::{Deserialize, Serialize};
use std::hash::Hash;
use std::{fmt::Debug, time::Instant};

pub fn write_time_config(
    event_time_zero: time::OffsetDateTime,
    client: &ClusterClient,
) -> Result<()> {
    let mut con = client.get_connection()?;
    let _: () = con.set(
        "time_config:event_time_zero".to_string(),
        serde_json::to_string(&event_time_zero)?,
    )?;
    Ok(())
}
pub fn read_time_config(client: &ClusterClient) -> Result<time::OffsetDateTime> {
    let mut con = client.get_connection()?;
    let serialized: String = con.get("time_config:event_time_zero".to_string())?;
    let event_time_zero = serde_json::from_str(&serialized)?;
    Ok(event_time_zero)
}

/// to write data to redis with the WriteToRedis dataflow operator, implement this trait
pub trait IoData {
    fn insert(&self, namespace: &str, con: &mut ClusterConnection) -> Result<()>;
    fn insert_logged(
        &self,
        namespace: &str,
        con: &mut ClusterConnection,
        time: &u64,
        num_keys: usize,
        worker: usize,
    ) -> Result<()> {
        let start = Instant::now();
        let write_op = self.insert(namespace, con);
        if let Err(e) = &write_op {
            tracing::error!("write failed for {namespace} ({num_keys} keys): {}", e);
        } else {
            tracing::debug!(
                "@time {time}. {namespace}. worker {worker}. wrote {} keys in {:?}",
                num_keys,
                start.elapsed()
            );
        }
        write_op
    }
}

/// DecayedTopK are inserted into redis as json
impl<
        K: ToRedisKey,
        V: Clone + Eq + Hash + Debug + Serialize + for<'a> Deserialize<'a>,
        const HL: usize,
    > IoData for Vec<(K, DecayedTopK<V, HL>)>
{
    fn insert(&self, namespace: &str, con: &mut ClusterConnection) -> Result<()> {
        let mut pipeline = redis::cluster::cluster_pipe();
        // TODO, make this configurable?
        let ttl_seconds = 60 * 10;
        for (key, top_k) in self.iter() {
            // transform into domain representation, e.g. use a field half-life instead of a cost generic
            let top_k = top_k.to_domain();
            pipeline.set_ex(
                key.namespaced(namespace),
                serde_json::to_string(&top_k).expect("json failure"),
                ttl_seconds,
            );
        }
        //TODO error handling? retry?
        pipeline.query(con)?;
        Ok(())
    }
}

/// DecayedValue are inserted into redis as json
impl<K: ToRedisKey, const HL: usize> IoData for Vec<(K, DecayedValue<HL>)> {
    fn insert(&self, namespace: &str, con: &mut ClusterConnection) -> Result<()> {
        let mut pipeline = redis::cluster::cluster_pipe();
        // TODO, make this configurable?
        let ttl_seconds = 3600;
        for (key, dv) in self.iter() {
            // set decaying value
            pipeline.set_ex(
                key.namespaced(namespace),
                serde_json::to_string(&dv.to_domain()).expect("json failure"),
                ttl_seconds,
            );
        }
        //TODO error handling? retry?
        pipeline.query(con)?;
        Ok(())
    }
}

/// Vec<(K, (u64, f64))> considered as a part of timeseries, ie.
/// one datapoint for series associated with each K. The timeseries
/// are stored as redis sorted sets
impl<K: ToRedisKey> IoData for Vec<(K, (u64, f64))> {
    fn insert(&self, namespace: &str, con: &mut ClusterConnection) -> Result<()> {
        let mut pipeline = redis::cluster::cluster_pipe();
        // TODO, make this configurable?
        let ttl_seconds = 3600;
        // TODO, make this configurable? 60 s interval keep 1 hour of data max.
        let series_length: isize = 60;
        // let ((evaluate_at, time), ts_values) = self;
        for (key, (time, value)) in self.iter() {
            let key = key.namespaced(namespace);
            // append to time series
            pipeline.zadd(key.clone(), value, time);
            pipeline.zremrangebyrank(key.clone(), 0, -(series_length - 1));
            pipeline.expire(key, ttl_seconds);
        }
        //TODO error handling? retry?
        pipeline.query(con)?;
        Ok(())
    }
}

/// Trends are stored as hashsets in redis
impl<T: Clone + Eq + Hash + Debug + Serialize + for<'a> Deserialize<'a>> IoData
    for Vec<TrendEvent<T>>
{
    fn insert(&self, namespace: &str, con: &mut ClusterConnection) -> Result<()> {
        let mut pipeline = redis::cluster::cluster_pipe();
        for trend_event in self.iter() {
            let current_key = format!("trending:{namespace}");
            let past_key = format!("trended:{namespace}");
            let element: T = trend_event.element.to_owned();
            let field = serde_json::to_string(&element)?;
            match &trend_event.event {
                Event::Start { start, features } => {
                    let trend = Trend {
                        element,
                        start: *start,
                        end: None,
                        pre_detection: features.values.clone(),
                        post_detection: vec![],
                    };
                    pipeline.hset(current_key, field, serde_json::to_string(&trend)?);
                }
                Event::Ongoing { start, pre, post } => {
                    // TODO do we want to update trends? current impl is inefficient as it writes
                    // the full pre/post detection values.
                    let trend = Trend {
                        element,
                        start: *start,
                        end: None,
                        pre_detection: pre.values.clone(),
                        post_detection: post.values.clone(),
                    };
                    pipeline.hset(current_key, field, serde_json::to_string(&trend)?);
                }
                Event::End {
                    start,
                    end,
                    pre,
                    post,
                } => {
                    pipeline.hdel(current_key, field);
                    let past_trend = Trend {
                        element,
                        start: *start,
                        end: Some(*end),
                        pre_detection: pre.values.clone(),
                        post_detection: post.values.clone(),
                    };
                    pipeline.lpush(past_key, serde_json::to_string(&past_trend)?);
                }
            };
        }
        //TODO error handling? retry?
        pipeline.query(con)?;
        Ok(())
    }
}
/// trait to extract a redis key, which can
/// make use of hashtags to shard certain keys
/// on the same redis host
/// example: for Country, we use the country name
/// as hashtag, ensuring all country topK are on the same
/// redis host (there are only few), which allows for
/// easy scanning of all keys/values
pub trait ToRedisKey {
    fn redis_key(&self) -> String;
    fn namespaced(&self, namespace: &str) -> String {
        format!("{}:{}", self.redis_key(), namespace)
    }
}

impl ToRedisKey for String {
    fn redis_key(&self) -> String {
        self.clone()
    }
}

impl ToRedisKey for &str {
    fn redis_key(&self) -> String {
        self.to_string()
    }
}

impl ToRedisKey for () {
    fn redis_key(&self) -> String {
        "".to_owned()
    }
}

/// country namespace on redis
/// { } redis hashtag to ensure all country keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Country {
    fn redis_key(&self) -> String {
        format!("{{country}}:{}", self.0)
    }
}

/// wiki namespace on redis
/// { } redis hashtag to ensure all projects keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Wiki {
    fn redis_key(&self) -> String {
        format!("{{wiki}}:{}:{}", self.family, self.project)
    }
}

/// isp namespace on redis
/// { } redis hashtag to ensure all keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Isp {
    fn redis_key(&self) -> String {
        format!("{{isp}}:{}", self.0)
    }
}

/// ip namespace on redis
/// { } redis hashtag to ensure all keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Ip {
    fn redis_key(&self) -> String {
        format!("{{ip:{}}}", self.0)
    }
}

/// page namespace on redis
/// { } redis hashtag to ensure all page keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Page {
    fn redis_key(&self) -> String {
        format!("{{page:{}:{}:{}}}", self.family, self.project, self.title)
    }
}

/// page namespace on redis
/// { } redis hashtag to ensure all page keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Fingerprint {
    fn redis_key(&self) -> String {
        format!(
            "{{fingerprint:{}:{}:{}}}",
            self.ip, self.user_agent, self.accept_language
        )
    }
}

/// url namespace on redis
/// { } redis hashtag to ensure all url keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for Url {
    fn redis_key(&self) -> String {
        format!("{{url:{}:{}:{}}}", self.host, self.path, self.query)
    }
}
/// base url namespace on redis
/// { } redis hashtag to ensure all url keys are in the same hash slot
/// in rust curly braces are escaped with curly braces
impl ToRedisKey for BaseUrl {
    fn redis_key(&self) -> String {
        format!("{{baseurl:{}:{}}}", self.host, self.path)
    }
}

/// Retrieve a DecayedTopK instance from redis, and
/// return a TopK evaluated at the specified time
pub fn get_topk<K: ToRedisKey, V: for<'a> Deserialize<'a>>(
    con: &mut ClusterConnection,
    key: &K,
    namespace: &str,
    evaluate_at_timestamp: f64,
) -> Result<TopK<V>> {
    let topk: DecayedTopKDomain<V> = get_decayed_topk(con, key, namespace)?;
    let topk: Vec<(V, f64)> = topk
        .topk
        .into_iter()
        .map(|(k, v)| (k, v.value_as_of(evaluate_at_timestamp)))
        .collect();
    Ok(TopK { topk })
}

pub fn get_decayed_topk<K: ToRedisKey, V: for<'a> Deserialize<'a>>(
    con: &mut ClusterConnection,
    key: &K,
    namespace: &str,
) -> Result<DecayedTopKDomain<V>> {
    let rkey = key.namespaced(namespace);
    let json: String = con.get(rkey)?;
    Ok(serde_json::from_str(&json)?)
}

pub fn get_decayed_pageviews(
    con: &mut ClusterConnection,
    page: &Page,
) -> Result<DecayedValueDomain> {
    let rkey = page.namespaced("pv:dv");
    let json: String = con.get(rkey)?;
    Ok(serde_json::from_str(&json)?)
}

pub fn get_pageviews_series(con: &mut ClusterConnection, page: &Page) -> Result<Vec<(u64, f64)>> {
    let rkey = page.namespaced("pv:series");
    let series: Vec<String> = con.zrange_withscores(rkey, 0, -1)?;

    series
        .chunks(2)
        .map(|chunk| {
            let (t, v) = match chunk {
                [views, time] => (
                    time.parse::<u64>().map_err(anyhow::Error::msg),
                    views.parse::<f64>().map_err(anyhow::Error::msg),
                ),
                e => (
                    Err(anyhow::anyhow!("uneven zrange remaining: {:?}", e)),
                    Ok(0.0), // doesn't matter
                ),
            };
            t.and_then(|tt| v.map(|vv| (tt, vv)))
        })
        .collect()
}

pub fn get_active_trends<T: for<'a> Deserialize<'a>>(
    con: &mut ClusterConnection,
    name: &str,
) -> Result<Vec<Trend<T>>> {
    let rkey = format!("trending:{name}");
    let entries: Vec<String> = con.hgetall(rkey)?;
    entries
        .chunks(2)
        .map(|chunk| {
            match chunk {
                [_, json] => serde_json::from_str(&json).context("Failed to deserialize trend"),
                e => Err(anyhow::anyhow!("uneven entries remaining: {:?}", e)),
            }
            // t.and_then(|tt| v.map(|vv| (tt, vv)))
        })
        .collect()
}

pub fn get_past_trends<T: for<'a> Deserialize<'a>>(
    con: &mut ClusterConnection,
    name: &str,
) -> Result<Vec<Trend<T>>> {
    let rkey = format!("trended:{name}");
    let entries: Vec<String> = con.lrange(rkey, -10, -1)?;
    entries
        .iter()
        .map(|json| serde_json::from_str(&json).context("Failed to deserialize trend"))
        .collect()
}
