/// WMF kafka sources for timely dataflow
use std::pin::Pin;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::task::{Context, Poll};
use std::time::{Duration, Instant};

use futures_util::StreamExt;
use owning_ref::OwningHandle;
use rdkafka::client::ClientContext;
use rdkafka::consumer::{Consumer, ConsumerContext, MessageStream, Rebalance};
use rdkafka::error::KafkaResult;
use rdkafka::util::TokioRuntime;
use rdkafka::{
    config::{ClientConfig, RDKafkaLogLevel},
    consumer::StreamConsumer,
};
use rdkafka::{Message, TopicPartitionList};
use serde::Deserialize;
use timely::dataflow::operators::generic::source;
use timely::dataflow::operators::{CapabilitySet, Map};
use timely::dataflow::{Scope, Stream};
use timely::scheduling::SyncActivator;

use crate::webrequest::WebRequest;

pub struct WmfConsumerContext;

impl ClientContext for WmfConsumerContext {
    fn stats(&self, statistics: rdkafka::Statistics) {
        tracing::info!("Client stats: {:?}", statistics);
    }
}

impl ConsumerContext for WmfConsumerContext {
    fn pre_rebalance(&self, rebalance: &Rebalance) {
        tracing::info!("Pre rebalance {:?}", rebalance);
    }

    fn post_rebalance(&self, rebalance: &Rebalance) {
        tracing::info!("Post rebalance {:?}", rebalance);
    }

    fn commit_callback(&self, _result: KafkaResult<()>, _offsets: &TopicPartitionList) {
        // tracing::info!("Committing offsets: {:?}", offsets);
    }
}

/// creates a generic streaming kafka consumer
pub fn wmf_consumer(group_id: &str) -> StreamConsumer<WmfConsumerContext, TokioRuntime> {
    // pub fn wmf_consumer() -> BaseConsumer<WmfConsumerContext> {
    let brokers = vec![
        "kafka-jumbo1007.eqiad.wmnet",
        "kafka-jumbo1008.eqiad.wmnet",
        "kafka-jumbo1009.eqiad.wmnet",
        "kafka-jumbo1010.eqiad.wmnet",
        "kafka-jumbo1011.eqiad.wmnet",
        "kafka-jumbo1012.eqiad.wmnet",
        "kafka-jumbo1013.eqiad.wmnet",
        "kafka-jumbo1014.eqiad.wmnet",
        "kafka-jumbo1015.eqiad.wmnet",
    ];
    let context = WmfConsumerContext;

    ClientConfig::new()
        .set("group.id", group_id)
        .set("bootstrap.servers", brokers.join(","))
        .set("enable.partition.eof", "false")
        .set("session.timeout.ms", "6000")
        .set("enable.auto.commit", "true")
        // .set("statistics.interval.ms", "5000")
        // .set("auto.offset.reset", "earliest")
        .set("auto.offset.reset", "latest")
        .set_log_level(RDKafkaLogLevel::Debug)
        .create_with_context(context)
        .expect("Consumer creation failed")
}

/// Struct used to own the streaming kafka consumer. Timely dataflow requires a
/// 'static lifetime for a stream and the rdkafka consumer does not support that.
pub struct KafkaStream {
    /// instant when the stream was started.
    start_instant: Instant,
    /// current instant used to derive the timely dataflow time
    current_instant: Instant,
    /// window duration used to increment the timely dataflow time
    window_duration: Duration,
    /// total number of elements consumed for that stream
    pub total_count: u128,
    /// owned streaming consumer using the owning_ref library
    upstream: OwningHandle<
        Box<StreamConsumer<WmfConsumerContext, TokioRuntime>>,
        Box<MessageStream<'static, WmfConsumerContext>>,
    >,
}

impl KafkaStream {
    // TODO generalize
    // let api_requests = "eqiad.api-gateway.request";
    // let mediawiki_requests = "eqiad.mediawiki.api-request";
    // let page_links_change = "eqiad.mediawiki.page-links-change";
    // let webrequest_text = "webrequest_text";
    // let topic = "webrequest_text";
    pub fn new(
        kafka_topic: &str,
        group_id: &str,
        time_zero: Instant,
        window_duration: Duration,
    ) -> Pin<Box<Self>> {
        let consumer = wmf_consumer(group_id);

        consumer.subscribe(&[kafka_topic]).expect("No subscribe?");

        let owned_stream = KafkaStream {
            start_instant: time_zero,
            current_instant: time_zero,
            window_duration,
            total_count: 0u128,
            upstream: OwningHandle::new_with_fn(Box::new(consumer), |c| {
                let cf = unsafe { &*c };
                Box::new(cf.stream())
            }),
        };
        Box::pin(owned_stream)
    }

    pub fn to_stream<T: Clone + for<'a> Deserialize<'a> + 'static, S: Scope<Timestamp = u64>>(
        self: Pin<Box<Self>>,
        scope: &S,
    ) -> Stream<S, T> {
        self.binary_stream(scope)
            .flat_map(|bytes| serde_json::from_slice(&bytes[..]))
    }

    pub fn binary_stream<S: Scope<Timestamp = u64>>(
        mut self: Pin<Box<Self>>,
        scope: &S,
    ) -> Stream<S, Vec<u8>> {
        // self.upstream.poll_next_unpin(cx)
        source(scope, "kafka_source", move |capability, info| {
            let kafka_waker = Arc::new(KafkaWaker::new(
                scope.sync_activator_for(info.address.to_vec()),
            ));

            let mut cap_set = CapabilitySet::from_elem(capability);
            move |output| {
                if kafka_waker.task_ready.load(Ordering::SeqCst) {
                    let waker = futures_util::task::waker_ref(&kafka_waker);
                    let mut cx = Context::from_waker(&waker);
                    kafka_waker.task_ready.store(false, Ordering::SeqCst);

                    while let Poll::Ready(item) = self.upstream.poll_next_unpin(&mut cx) {
                        if self.current_instant.elapsed() > self.window_duration {
                            self.current_instant = Instant::now();
                            let new_time = self
                                .current_instant
                                .duration_since(self.start_instant)
                                .as_millis() as u64;
                            cap_set.downgrade(Some(new_time));
                            tracing::error!("kafka source, new frontier {}", new_time);
                        }

                        match item {
                            Some(Ok(message)) => {
                                let time = self
                                    .current_instant
                                    .duration_since(self.start_instant)
                                    .as_millis() as u64;
                                if let Some(bytes) = message.payload() {
                                    self.total_count += 1;
                                    output.session(&cap_set.delayed(&time)).give(bytes.to_vec());
                                }
                            }
                            Some(Err(error)) => {
                                //TODO when does this happen? how often does this happen?
                                tracing::error!("kafka error {}", error);
                            }
                            None => {
                                //TODO when does this happen? how often does this happen?
                                tracing::error!("kafka stream poll returned a Poll::Ready(None)");
                            }
                        }
                    }
                }
            }
        })
    }
    // }
}

/// async waker inspired by https://github.com/MaterializeInc/materialize/blob/main/src/timely-util/src/builder_async.rs#L129
struct KafkaWaker {
    activator: SyncActivator,
    task_ready: AtomicBool,
}

impl KafkaWaker {
    pub fn new(activator: SyncActivator) -> KafkaWaker {
        KafkaWaker {
            activator: activator,
            task_ready: AtomicBool::new(true),
        }
    }
}

impl futures_util::task::ArcWake for KafkaWaker {
    fn wake_by_ref(arc_self: &Arc<Self>) {
        arc_self.task_ready.store(true, Ordering::SeqCst);
        let _ = arc_self.activator.activate();
    }
}
