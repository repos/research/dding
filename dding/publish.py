"""
Simple script to publish streaming topK pages
"""
#%%
from datetime import datetime
import os
import requests
# projects = ['en', 'fr', 'de', 'es', 'ru', 'ja', 'it', 'zh', 'pl', 'uk', 'pt', 'fa', 'nl', 'he', 'ko']
projects = ['en', 'fr', 'de', 'es', 'ja' ]
pageview_threshold = 50

export_dir = '/home/fab/rust/dding/data/export'
# export_dir = '/srv/published/datasets/streaming_pageviews'

ds = datetime.now()
day = ds.strftime('%Y-%m-%d')
hour = ds.strftime('%HH%M')

for project in projects:
    try:
        response = requests.get(f"http://localhost:3001/top?project={project}&family=wikipedia")
        topk = response.json()['topk']
        topk = sorted(topk,key=lambda x: x[1],reverse=True)

        output_dir = os.path.join(export_dir,project,day)
        os.makedirs(output_dir,exist_ok=True)
        file_name = os.path.join(output_dir, f'{hour}.tsv')

        with open(file_name,'w') as f:
            filtered = [f'{p}\t{v}' for p,v in topk if v>pageview_threshold ]
            f.write('\n'.join(filtered) + '\n')

    except Exception as e:
        print(f'export for {project} failed')
        print(e)


# os.system('published-sync')
