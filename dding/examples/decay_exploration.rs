//! Tool to play around with decaying counts
//! TODO, these should be unit tests
use anyhow::Result;
use dding::decay::DecayedValue;

#[tokio::main]
async fn main() -> Result<()> {
    tracing_subscriber::fmt::init();

    const HALF_LIFE: usize = 1000 * 60 * 10;

    let mut dv: DecayedValue<HALF_LIFE> = DecayedValue::new(100f64, 0f64);
    for i in ((1000 * 60)..(1000 * 60 * 10)).step_by(1000 * 60) {
        let i = i as f64;
        if i == 300000f64 {
            dv += DecayedValue::new(100f64, i);
        }

        dv.update_as_of(i);
        println!("@{} - {:?}", i, dv);
        println!(
            "average {} , {}",
            dv.average() * (1000 * 60) as f64,
            dv.average_interval(i, i - (1000 * 60 * 10) as f64) * (1000 * 60) as f64
        );
    }

    // println!("{:?}",dv);

    // let dv1 = decaying_values::DecayedValue::new(100f64, 5f64, half_life);

    // dv += dv1;

    // println!("{:?}",dv);
    // println!("{}",dv.average(half_life));
    // println!("{}",dv.average_interval(0f64, 10f64, half_life));

    Ok(())
}
