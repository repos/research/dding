use uaparser::{Parser, UserAgentParser};

/// Tool to experiment with user agent parsing
fn main() {
    tracing_subscriber::fmt::init();

    // let ua_parser = UserAgentParser::from_yaml("resources/spider.yaml").unwrap();
    let ua_parser = UserAgentParser::from_yaml("resources/regexes.yaml").unwrap();

    let uas = vec!["-", "Java/1.8.0_265","Python-urllib/3.10", "curl/7.74.0", "Mozilla/5.0 (Linux; Android 10; K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Mobile Safari/537.36"];

    for ua in uas {
        tracing::info!("{}\n {:#?}", ua, ua_parser.parse_device(ua));
        // println!("{:#?}", ua_parser.parse_product(ua));
    }
}
