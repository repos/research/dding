/// cargo run --bin maxminding /usr/share/GeoIP/GeoIP2-City.mmdb 141.211.185.158
use std::net::IpAddr;

use maxminddb::geoip2;

fn main() -> Result<(), String> {
    let mut args = std::env::args().skip(1);
    let reader = maxminddb::Reader::open_readfile(
        args.next()
            .ok_or("First argument must be the path to the IP database")?,
    )
    .unwrap();
    let ip: IpAddr = args
        .next()
        .ok_or("Second argument must be the IP address, like 128.101.101.101")?
        .parse()
        .unwrap();
    let city: geoip2::City = reader.lookup(ip).unwrap();
    println!("{city:#?}");
    let isp: geoip2::Isp = reader.lookup(ip).unwrap();
    println!("{isp:#?}");
    let _isp = isp.isp.unwrap_or("Unknown");
    // let country: geoip2::Country = reader.lookup(ip).unwrap();
    // println!("{country:#?}");
    let names = city.country.unwrap().names.unwrap();
    let name = names.get("en").unwrap();
    println!("{name}");
    let x = city.continent.unwrap().names.unwrap();
    let x = x.get("en").unwrap();
    println!("{x}");

    Ok(())
}
