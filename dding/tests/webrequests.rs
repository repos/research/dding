//! Webrequests integration test

use dding::webrequest::WebRequest;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

fn read_lines(file: &str) -> impl Iterator<Item = String> {
    let path = concat!(env!("CARGO_MANIFEST_DIR"), "/tests/data/");
    let path = Path::new(path).join(file);
    let file = File::open(path.clone()).unwrap_or_else(|_| panic!("can't open file {:?}", path));
    BufReader::new(file)
        .lines()
        .map(|line| line.expect("can't read line"))
}

#[test]
fn parse_webrequests() {
    let reqs: Vec<WebRequest> = read_lines("webrequest_test.json")
        .map(|line| WebRequest::from_json(&line).expect("can't parse webrequest json"))
        .collect();

    for req in &reqs {
        assert!(req.datetime().is_ok(), "can't parse datetime");
    }

    for req in &reqs {
        assert!(
            req.normalized_host().is_some(),
            "can't parse normalized host"
        );
    }
}
