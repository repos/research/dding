import logging
import redis
import skein
import sys
import time
import util

logging.basicConfig(level=logging.INFO)

redis_port = 3009
binary_name = "ddos_detection"


def deploy_on_yarn(
    workers: int,
    kafka_group: str,
    binary_path: str,
    redis_nodes: str | None = None,
):
    spec = skein.ApplicationSpec.from_yaml(
        f"""
name: dding-ddos-dection
queue: default

services:
    ddos_detection:
        env:
            workers: "{workers}"
            kafka_group: "{kafka_group}"
            redis_nodes: "{redis_nodes}"
        instances: 0
        files:
            ddos_detection: {binary_path}
        resources:
            vcores: 10
            memory: 30000 MiB
        script: |
            RUST_BACKTRACE=full ./ddos_detection --workers $workers --group-id $kafka_group --redis-nodes $redis_nodes || sleep 600000
    """
    )
    if not redis_nodes:
        # redis yarn deploy
        redis_spec = redis.redis_spec(port=redis_port)
        spec.services.update(redis_spec.services)

    with skein.Client() as client:
        app_id = client.submit(spec)
        app = client.connect(app_id)

        if not redis_nodes:
            redis_nodes = redis.start_redis_cluster(app, 6, redis_port)

        logging.info("deploying ddos_detection")
        app.add_container(service="ddos_detection", env={"redis_nodes": redis_nodes})
        done = False
        while not done:
            app = client.connect(app_id)
            containers = app.get_containers(["ddos_detection"])
            if containers:
                address = containers[0].yarn_node_http_address
                address = address.split(":")[0]
                if address:
                    logging.info(
                        f"ddos_detection yarn application:  https://yarn.wikimedia.org/proxy/{app_id}"
                    )
                    if "https://gitlab.wikimedia.org" in binary_path:
                        # deployed from CI, update config
                        redis_nodes_config = util.generate_config_path(
                            "ddos_detection_redis_nodes.env"
                        )
                        util.write_config(redis_nodes_config, redis_nodes)
                        logging.info(f"redis conf written to {redis_nodes_config}")
                    else:
                        logging.info("redis conf not written")
                    logging.info("to stop yarn application:")
                    logging.info(f"yarn application -kill {app_id}")
                    logging.info("to start cli:")
                    logging.info(
                        f"cargo run --release --bin ddos_cli -- -r {redis_nodes}"
                    )
                    done = True
            else:
                time.sleep(0.5)


if __name__ == "__main__":
    use_ci_build = len(sys.argv) == 2 and sys.argv[1] == "ci"
    binary_path = (
        util.gitlab_binary_url(binary_name)
        if use_ci_build
        else f"target/release/{binary_name}"
    )
    deploy_on_yarn(
        workers=6,
        kafka_group="ddos_detection",
        binary_path=binary_path,
        redis_nodes=redis.redis_nodes(),
    )
