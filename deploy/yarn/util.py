import fsspec
import os
import subprocess

BASE_DIR = "/wmf/data/research/binaries/dding/"


def generate_config_path(config_name: str) -> str:
    return BASE_DIR + config_name


def configure_fsspec() -> None:
    os.environ["CLASSPATH"] = (
        subprocess.check_output(["hadoop", "classpath", "--glob"])
        .decode("utf-8")
        .strip()
    )
    os.environ["JAVA_HOME"] = "/usr/lib/jvm/java-1.8.0-openjdk-amd64"
    os.environ["HADOOP_HOME"] = "/usr/lib/hadoop"


def write_config(hdfs_path: str, config: str):
    configure_fsspec()
    fs = fsspec.open("hdfs://analytics-hadoop/").fs
    with fs.open(hdfs_path, "w") as file:
        file.write(config)


def read_config(hdfs_path: str) -> str:
    configure_fsspec()
    fs = fsspec.open("hdfs://analytics-hadoop/").fs
    with fs.open(hdfs_path, "r") as file:
        return file.read()


def gitlab_binary_url(binary_name: str) -> str:
    return f"https://gitlab.wikimedia.org/api/v4/projects/870/packages/generic/{binary_name}/0.1.0/{binary_name}"
