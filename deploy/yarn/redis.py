import logging
import os
import skein
import tempfile
import time

logging.basicConfig(level=logging.INFO)


def redis_nodes() -> list[str] | None:
    nodes = os.environ.get("redis_nodes")
    if nodes:
        logging.info("using `redis_nodes` from env")
        logging.info(nodes)
        return nodes
    else:
        return None


def redis_spec(port=3002) -> skein.ApplicationSpec:
    # redis config file. disable snapshotting and logging
    redis_conf = tempfile.NamedTemporaryFile("w", encoding="UTF-8", delete=False)
    redis_conf.write(
        f"""
port {port}
cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 5000
protected-mode no
save 36000 1
stop-writes-on-bgsave-error yes
repl-diskless-sync yes
appendonly no
"""
    )
    redis_conf.flush()

    return skein.ApplicationSpec.from_yaml(
        f"""
name: redis-yarn
queue: default

services:
    redis-server:
        files:
            redis-server: hdfs://analytics-hadoop/wmf/data/research/binaries/redis/redis-server
            redis.conf: {redis_conf.name}
        instances: 0
        resources:
            vcores: 1
            memory: 10000 MiB
        script: |
            ./redis-server ./redis.conf || sleep 3600
    init-cluster:
        env:
            redis_nodes: ''
            replicas: '1'
        files:
            redis-cli: hdfs://analytics-hadoop/wmf/data/research/binaries/redis/redis-cli
        instances: 0
        resources:
            vcores: 1
            memory: 5000 MiB
        script: |
            ./redis-cli --cluster-yes --cluster create $redis_nodes  --cluster-replicas $replicas || sleep 3600
    """
    )


def start_redis_cluster(app, n_servers=6, port=3002) -> str:
    logging.info(f"starting {n_servers} redis servers")
    for i in range(n_servers):
        app.add_container(service="redis-server")
    logging.info("waiting for servers")
    redis_nodes = None
    time.sleep(5)
    while not redis_nodes:
        containers = app.get_containers(["redis-server"])
        if containers:
            address = [c.yarn_node_http_address.split(":")[0] for c in containers]
            if len(address) == n_servers and "" not in address:
                logging.info("redis-servers are running")
                redis_nodes = " ".join([f"{a}:{port}" for a in address])
        else:
            time.sleep(0.5)

    logging.info("initializing cluster")
    init = app.add_container(
        service="init-cluster", env={"redis_nodes": redis_nodes, "replicas": "1"}
    )
    time.sleep(5)
    logging.info("waiting for initializing")
    while all([c.state != "RUNNING" for c in app.get_containers(["init-cluster"])]):
        if init.state == "REQUESTED":
            # TODO why is the first one getting stuck in REQUESTED at times?
            init = app.add_container(
                service="init-cluster",
                env={"redis_nodes": redis_nodes, "replicas": "1"},
            )
        time.sleep(5)
    return redis_nodes


if __name__ == "__main__":
    with skein.Client() as client:
        app_id = client.submit(redis_spec())
        app = client.connect(app_id)
        logging.info(
            f"redis yarn application:  https://yarn.wikimedia.org/proxy/{app_id}"
        )
        redis_nodes = start_redis_cluster(app, 6)
        logging.info(f'export redis_nodes="{redis_nodes}"')

        logging.info("to stop yarn application:")
        logging.info(f"yarn application -kill {app_id}")
