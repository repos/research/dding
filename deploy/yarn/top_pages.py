import util
import logging
import redis
import skein
import sys
import time

logging.basicConfig(level=logging.INFO)

redis_port = 3010
binary_name = "top_pages"


def deploy_on_yarn(workers, kafka_group, binary_path: str, redis_nodes=None, port=3001):
    spec = skein.ApplicationSpec.from_yaml(
        f"""
name: dding-top-pages
queue: default

services:
    top_pages:
        env:
            workers: "{workers}"
            kafka_group: "{kafka_group}"
            redis_nodes: "{redis_nodes}"
            port: "{port}"
        instances: 0
        files:
            top_pages: {binary_path}
        resources:
            vcores: 10
            memory: 30000 MiB
        script: |
            RUST_BACKTRACE=full ./top_pages --workers $workers --group-id $kafka_group --redis-nodes $redis_nodes --port $port || sleep 600000
    """
    )
    if not redis_nodes:
        # redis yarn deploy
        redis_spec = redis.redis_spec(port=redis_port)
        spec.services.update(redis_spec.services)

    with skein.Client() as client:
        app_id = client.submit(spec)
        app = client.connect(app_id)

        if not redis_nodes:
            redis_nodes = redis.start_redis_cluster(app, 6, redis_port)

        logging.info("deploying top_pages")
        app.add_container(service="top_pages", env={"redis_nodes": redis_nodes})
        done = False
        while not done:
            app = client.connect(app_id)
            containers = app.get_containers(["top_pages"])
            if containers:
                address = containers[0].yarn_node_http_address
                address = address.split(":")[0]
                if address:
                    logging.info(
                        f"top_pages yarn application:  https://yarn.wikimedia.org/proxy/{app_id}"
                    )
                    logging.info(f"top_pages is running on host: {address}")
                    if "https://gitlab.wikimedia.org" in binary_path:
                        # deployed from CI, update config
                        redis_nodes_config = util.generate_config_path(
                            "top_pages_redis_nodes.env"
                        )
                        util.write_config(redis_nodes_config, redis_nodes)
                        logging.info(f"redis conf written to {redis_nodes_config}")
                    else:
                        logging.info("redis conf not written")
                    logging.info("to stop yarn application:")
                    logging.info(f"yarn application -kill {app_id}")
                    logging.info("to start cli:")
                    logging.info(
                        f"cargo run --release --bin pageviews_cli -- --redis-nodes {redis_nodes}"
                    )
                    done = True
            else:
                time.sleep(0.5)


if __name__ == "__main__":
    use_ci_build = len(sys.argv) == 2 and sys.argv[1] == "ci"
    binary_path = (
        util.gitlab_binary_url(binary_name)
        if use_ci_build
        else f"target/release/{binary_name}"
    )
    deploy_on_yarn(
        workers=6,
        kafka_group="top_pages",
        binary_path=binary_path,
        redis_nodes=redis.redis_nodes(),
        port=3003,
    )
