#!/usr/bin/env bash

set -e

# NOTE: this script assumes a env variable `redis_nodes`,
# e.g. by running `python deploy/yarn/redis.py` and the returned `export redis_nodes=xxx` command
# alternatively, you can define `redis_nodes` below
# redis_nodes="an-worker1089.eqiad.wmnet:3002"
if [[ -z "$redis_nodes" ]]; then
    echo "redis_nodes not defined" 1>&2
    exit 1
fi

for node in $redis_nodes; do
    echo "$node:"
    redis-cli -h ${node%:*} -p ${node#*:} INFO memory | grep used_memory_human
    redis-cli -h ${node%:*} -p ${node#*:} INFO stats | grep instantaneous_ops_per_sec
done
