#!/usr/bin/env bash

set -e

# NOTE: this script assumes a env variable `redis_nodes`,
# e.g. by running `python deploy/yarn/redis.py` and the returned `export redis_nodes=xxx` command
# alternatively, you can define `redis_nodes` below
# redis_nodes="an-worker1089.eqiad.wmnet:3002"
if [[ -z "$redis_nodes" ]]; then
    echo "redis_nodes not defined" 1>&2
    exit 1
fi

workers=9
kafka_group="ddos_detection_local"

cargo build --release --bin ddos_detection
# no docker...
mkdir -p dist
cp target/release/ddos_detection dist
cp -r resources/ dist
cp -r ddrt/dist/ dist
pushd dist
~/kafka/usr/bin/kafka-consumer-groups --bootstrap-server kafka-jumbo1010:9092 --group $kafka_group --reset-offsets --to-latest --topic webrequest_text --execute


RUST_LOG="debug"
# RUST_LOG="ddos_detection=debug,dding=debug,dding::db=info"
# RUST_BACKTRACE="full"
RUST_LOG="trending_detection=debug,topk=debug,decayed=debug" ./ddos_detection --workers $workers --group-id $kafka_group --redis-nodes $redis_nodes
