#!/usr/bin/env bash

set -e

# NOTE: this script assumes a env variable `redis_nodes`,
# e.g. by running `python deploy/yarn/redis.py` and the returned `export redis_nodes=xxx` command
# alternatively, you can define `redis_nodes` below
# redis_nodes="an-worker1089.eqiad.wmnet:3002"
if [[ -z "$redis_nodes" ]]; then
    echo "redis_nodes not defined" 1>&2
    exit 1
fi

workers=6
kafka_group="top_pages_local"
port=3001

cargo build --release --bin top_pages
# no docker...
mkdir -p dist
cp target/release/top_pages dist
cp -r resources/ dist
cp -r ddrt/dist/ dist
pushd dist
~/kafka/usr/bin/kafka-consumer-groups --bootstrap-server kafka-jumbo1010:9092 --group $kafka_group --reset-offsets --to-latest --topic webrequest_text --execute

# RUST_LOG="info,dding::db=debug" local
RUST_LOG="trending_detection=debug,topk=debug,partial_decayed=debug,decayed=debug"
RUST_BACKTRACE="full"
RUST_LOG="trending_detection=debug,topk=debug,decayed=debug" ./top_pages --workers $workers --group-id $kafka_group --redis-nodes $redis_nodes --port $port
